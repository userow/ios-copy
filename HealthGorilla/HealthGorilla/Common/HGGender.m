//
//  HGGender.m
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGGender.h"

@implementation HGGender

+ (NSArray *)allGenders
{
    return @[ @"Male", @"Female"];
}

+ (NSArray *)allGendersLocalized
{
    return @[ @"Male".localized, @"Female".localized];
}

+ (NSString *)defaultGender
{
    return @"Male";
}

+ (bool)isMale:(NSString *)gender
{
    return [gender.lowercaseString isEqualToString:@"Male".lowercaseString];
}

+ (bool)isFemale:(NSString *)gender
{
    return [gender.lowercaseString isEqualToString:@"Female".lowercaseString];
}

+ (NSString *)male
{
    return @"Male";
}

+ (NSString *)female
{
    return @"Female";
}

@end
