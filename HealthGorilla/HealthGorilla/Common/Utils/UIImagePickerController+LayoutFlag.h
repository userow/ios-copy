//
//  UIImagePickerController+LayoutFlag.h
//  Health Gorilla
//
//  Created by Alex Rodset on 28/09/2018.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImagePickerController (LayoutFlag)

@property (readwrite) BOOL addFrameLayout;

- (void)setAddFrameLayout:(BOOL)addFrameLayout;

@end

NS_ASSUME_NONNULL_END
