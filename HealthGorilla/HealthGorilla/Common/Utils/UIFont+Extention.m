//
//  UIFont+Extention.m
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "UIFont+Extention.h"

@implementation UIFont (Extention)

+ (UIFont *)mediumSystemFontOfSize:(CGFloat)fontSize
{
    return [UIFont systemFontOfSize:fontSize weight:UIFontWeightMedium];
}


+ (UIFont *)lightSystemFontOfSize:(CGFloat)fontSize
{
    return [UIFont systemFontOfSize:fontSize weight:UIFontWeightLight];
}


+ (UIFont *)thinSystemFontOfSize:(CGFloat)fontSize
{
    return [UIFont systemFontOfSize:fontSize weight:UIFontWeightThin];
}


+ (UIFont *)courierFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"Courier" size:fontSize];
}

@end
