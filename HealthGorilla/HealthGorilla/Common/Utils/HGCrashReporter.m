//
//  HGCrashReporter.m
//  HG-Project
//
//  Created by not Pavel on 8/8/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "HGCrashReporter.h"
#import "NSBundle+HG.h"


#if USE_PLCRASH

#import <CrashReporter/CrashReporter.h>
#import <MessageUI/MessageUI.h>
#import <DLSFTPConnection.h>
#import <DLSFTPUploadRequest.h>

@interface HGCrashReporter () <MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) UIViewController *parentController;
@property (nonatomic, strong) DLSFTPConnection *uploadConnection;
@property (nonatomic, strong) PLCrashReporter *crashReporter;

@end

#endif


@implementation HGCrashReporter

SYNTHESIZE_SINGLETON(HGCrashReporter);

#if USE_PLCRASH

+ (void)enableCrashReporter
{
    PLCrashReporterConfig *config = [[PLCrashReporterConfig alloc]
                                     initWithSignalHandlerType:PLCrashReporterSignalHandlerTypeBSD
                                     symbolicationStrategy:PLCrashReporterSymbolicationStrategyAll];
    PLCrashReporter *crashReporter = [[PLCrashReporter alloc] initWithConfiguration:config];
    
    NSError *error = nil;
    if (![crashReporter enableCrashReporterAndReturnError:&error]) {
        NSLog(@"Warning: Could not enable crash reporter: %@", error);
    }
    
    [self shared].crashReporter = crashReporter;
}


+ (bool)hasPendingCrashReport
{
    return [[self shared].crashReporter hasPendingCrashReport];
}


+ (void)promptToSendPendingCrashReportIn:(UIViewController *)controller
{
    PSPDFAlertView *prompt = [HGAlertView alertViewWithTitle:@"Crash Detected".localized
                                                  andMessage:@"We are sorry, the app crashed previously.\nWould you like to send us report so we can fix the problem?".localized];
    [prompt addButtonWithTitle:@"No".localized block:^(NSInteger buttonIndex) {
        [[HGCrashReporter shared] purgePendingCrashReport];
    }];
    [prompt setCancelButtonWithTitle:@"Yes".localized block:^(NSInteger buttonIndex) {
        [[HGCrashReporter shared] handleCrashReportIn:controller];
    }];
    [prompt show];
}


- (void)handleCrashReportIn:(UIViewController *)controller
{
    // Try loading the crash report
    NSError *error = nil;
    NSData *crashData = [_crashReporter loadPendingCrashReportDataAndReturnError: &error];
    if (nil == crashData) {
        NSLog(@"Could not load crash report: %@", error);
        [self purgePendingCrashReport];
        return;
    }
    
    // We could send the report from here, but we'll just print out some debugging info instead
    PLCrashReport *report = [[PLCrashReport alloc] initWithData:crashData error: &error];
    if (nil == report) {
        NSLog(@"Could not parse crash report: %@", error);
        [self purgePendingCrashReport];
        return;
    }
  
    NSString *text = [PLCrashReportTextFormatter stringValueForCrashReport:report
                                                            withTextFormat:PLCrashReportTextFormatiOS];
    self.parentController = controller;
    
    NSDictionary *crashDesc = @ {
        @"Device" : [HGDevice modelName],
        @"System" : [HGDevice systemVersion],
        @"Build" : [NSString stringWithFormat:@"%@ (%@)", [NSBundle shortVersion], [NSBundle version]],
        @"Server" : HGPortalURL,
    };
    
    [self submitCrashWithText:text crashDesc:crashDesc];
}


- (void)submitCrashWithText:(NSString *)crashText crashDesc:(NSDictionary *)crashDesc
{
    NSError *error = nil;
    
    NSString *directory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *remoteFtpFolder = @"uploads";

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
    
    NSString *filename = [NSString stringWithFormat:@"[%@]_%@",
                          [HGUser username], [dateFormatter stringFromDate:[NSDate date]]];
    
    // make a file with crash data
    NSString *crashFilename = [filename stringByAppendingString:@".txt"];
    NSString *crashLocalPath = [directory stringByAppendingPathComponent:crashFilename];
    NSString *crashRemotePath = [remoteFtpFolder stringByAppendingPathComponent:crashFilename];
    [crashText writeToFile:crashLocalPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    
    // make a file with crash description
    NSString *descFilename = [filename stringByAppendingString:@".plist"];
    NSString *descLocalPath = [directory stringByAppendingPathComponent:descFilename];
    NSString *descRemotePath = [remoteFtpFolder stringByAppendingPathComponent:descFilename];
    [crashDesc writeToFile:descLocalPath atomically:YES];
    
    NSString *privateKeyPath = [[NSBundle mainBundle] pathForResource:@"uploader" ofType:@"pem"];
    DLSFTPConnection *uploadConnection = [[DLSFTPConnection alloc] initWithHostname:@"dumps.healthgorilla.com"
                                                                               port:22
                                                                           username:@"uploader"
                                                                            keypath:privateKeyPath
                                                                         passphrase:@""];
    void (^doneBlock)() = ^{
        [self disconnect];
        [self purgePendingCrashReport];
        [SVProgressHUD dismiss];
    };
    
    DLSFTPUploadRequest *crashUploadRequest = [self ftpRequestWithRemotePath:crashRemotePath localPath:crashLocalPath completion:nil];
    DLSFTPUploadRequest *descUploadRequest = [self ftpRequestWithRemotePath:descRemotePath localPath:descLocalPath completion:^{
        doneBlock();
    }];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD show];
    
    [uploadConnection connectWithSuccessBlock:^{
        [uploadConnection submitRequest:crashUploadRequest];
        [uploadConnection submitRequest:descUploadRequest];
    } failureBlock:^(NSError *error) {
        NSLog(@"%@", error);
        doneBlock();
    }];
    
    self.uploadConnection = uploadConnection;
}


- (DLSFTPUploadRequest *)ftpRequestWithRemotePath:(NSString *)remotePath localPath:(NSString *)localPath completion:(void (^)())completion
{
    void (^doneBlock)() = ^{
        
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:localPath error:&error];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if (completion) {
                completion();
            }
        }];
    };
    
    DLSFTPUploadRequest *uploadRequest = [[DLSFTPUploadRequest alloc] initWithRemotePath:remotePath localPath:localPath successBlock:^(DLSFTPFile *file, NSDate *startTime, NSDate *finishTime) {
        
        doneBlock();
        
    } failureBlock:^(NSError *error) {
        
        NSLog(@"%@", error);
        
    } progressBlock:^(unsigned long long bytesReceived, unsigned long long bytesTotal) {
        // nop
    }];
    
    return uploadRequest;
}


- (void)purgePendingCrashReport
{
    [_crashReporter purgePendingCrashReport];
}


- (void)disconnect
{
    [self.uploadConnection disconnect];
    self.uploadConnection = nil;
}


- (void)submitEmailWithData:(NSData *)crashData
{
    // methos is UNUSED
    
    NSString *emailTitle = [NSString stringWithFormat:@"%@ Crash Report %@".localized, [HGAppConfig appNameWhitelabeled], [NSDate date]];
    NSString *messageBody = @"";
    NSArray *toRecipents = @[ kCrashReportEmailRecipient ];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Add attachment
    [mc addAttachmentData:crashData mimeType:@"application/octet-stream" fileName:[NSString stringWithFormat:@"%@.plcrash", [NSDate date]]];
    
    // Present mail view controller on screen
    [self.parentController presentViewController:mc animated:YES completion:NULL];
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self purgePendingCrashReport];
    
    // Close the Mail Interface
    [self.parentController dismissViewControllerAnimated:YES completion:nil];
}

#else

+ (void)enableCrashReporter {
    // nop
}
+ (bool)hasPendingCrashReport {
    return NO;
}
+ (void)promptToSendPendingCrashReportIn:(UIViewController *)controller {
    // nop
}

#endif

@end
