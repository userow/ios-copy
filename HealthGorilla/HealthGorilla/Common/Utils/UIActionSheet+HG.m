//
//  UIActionSheet+HG.m
//  HG-Project
//
//  Created by not Paul on 14.08.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "UIActionSheet+HG.h"
#import <objc/runtime.h>


@interface UIActionSheet () <UIActionSheetDelegate>

@property (copy) UIActionSheetBlock cancelBlock;
@property (copy) UIActionSheetBlock destructiveBlock;
@property (copy) UIActionSheetButtonBlock clickBlock;

@end


@implementation UIActionSheet (HG)

+ (UIActionSheet *)presentOnView:(UIView *)view
                       withTitle:(NSString *)title
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock
{
    return [self presentOnView:view
                        sender:nil
                     withTitle:title
                  otherButtons:otherButtons
                      onCancel:cancelBlock
               onClickedButton:clickBlock];
}


+ (UIActionSheet *)presentOnView:(UIView *)view
                          sender:(id)sender
                       withTitle:(NSString *)title
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock
{
    return [self presentOnView:view
                        sender:sender
                     withTitle:title
                  cancelButton:@"Cancel".localized
             destructiveButton:nil
                  otherButtons:otherButtons
                      onCancel:cancelBlock
                 onDestructive:nil
               onClickedButton:clickBlock];
}



+ (UIActionSheet *)presentOnView:(UIView *)view
                       withTitle:(NSString *)title
                    cancelButton:(NSString *)cancelButton
               destructiveButton:(NSString *)destructiveButton
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                   onDestructive:(UIActionSheetBlock)destructiveBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock
{
    return [self presentOnView:view
                        sender:nil
                     withTitle:title
                  cancelButton:cancelButton
             destructiveButton:destructiveButton
                  otherButtons:otherButtons
                      onCancel:cancelBlock
                 onDestructive:destructiveBlock
               onClickedButton:clickBlock];
}


+ (UIActionSheet *)presentOnView:(UIView *)view
                          sender:(id)sender
                       withTitle:(NSString *)title
                    cancelButton:(NSString *)cancelButton
               destructiveButton:(NSString *)destructiveButton
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                   onDestructive:(UIActionSheetBlock)destructiveBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:title
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:destructiveButton
                                              otherButtonTitles:nil];
    
    for (NSString *other in otherButtons) {
        [sheet addButtonWithTitle:other];
    }
    
    if (cancelButton) {
        NSInteger cancelButtonIndex = [sheet addButtonWithTitle:cancelButton];
        [sheet setCancelButtonIndex:cancelButtonIndex];
    }

    sheet.delegate = sheet;
    sheet.cancelBlock = cancelBlock;
    sheet.destructiveBlock = destructiveBlock;
    sheet.clickBlock = clickBlock;
    
    [[NSNotificationCenter defaultCenter] addObserver:sheet selector:@selector(applicationWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];

    if (sender && [HGDevice isPad]) {
        if ([sender isKindOfClass:[UIBarButtonItem class]]) {
            [sheet showFromBarButtonItem:sender animated:YES];
        } else if ([sender isKindOfClass:[UIView class]]) {
            [sheet showFromRect:[sender bounds] inView:sender animated:YES];
        }
    } else {
        [sheet showInView:view];
    }

    return sheet;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)applicationWillResignActive
{
    // FIX: dismiss any action sheet on recieving 'app will resign active' notification
    // see IOS-127 'Delete Draft' warning gets misplaced after minimizing-restoring app
    [self dismissWithClickedButtonIndex:999 animated:NO];
}


- (UIActionSheetBlock)cancelBlock {
    return objc_getAssociatedObject(self, @selector(cancelBlock));
}


- (void)setCancelBlock:(UIActionSheetBlock)cancelBlock {
    objc_setAssociatedObject(self, @selector(cancelBlock), cancelBlock, OBJC_ASSOCIATION_COPY);
}


- (UIActionSheetBlock)destructiveBlock {
    return objc_getAssociatedObject(self, @selector(destructiveBlock));
}


- (void)setDestructiveBlock:(UIActionSheetBlock)destructiveBlock {
    objc_setAssociatedObject(self, @selector(destructiveBlock), destructiveBlock, OBJC_ASSOCIATION_COPY);
}


- (UIActionSheetButtonBlock)clickBlock {
    return objc_getAssociatedObject(self, @selector(clickBlock));
}


- (void)setClickBlock:(UIActionSheetButtonBlock)clickBlock {
    objc_setAssociatedObject(self, @selector(clickBlock), clickBlock, OBJC_ASSOCIATION_COPY);
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Blocks execute while UIActionSheet is living.
    // On iPad it prevents presentation of next modal VC because current in presenting.
    // WORKAROUND: delay the execution of blocks on run loop
    if(actionSheet.destructiveButtonIndex == buttonIndex) {
        if (actionSheet.destructiveBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                actionSheet.destructiveBlock(actionSheet);
            }];
        }
    }
    else if (actionSheet.cancelButtonIndex == buttonIndex) {
        if (actionSheet.cancelBlock) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                actionSheet.cancelBlock(actionSheet);
            }];
        }
    }
    else if (actionSheet.clickBlock) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            actionSheet.clickBlock(actionSheet, buttonIndex);
        }];
    }
}


- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    if (actionSheet.cancelBlock) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            actionSheet.cancelBlock(actionSheet);
        }];
    }
}

@end
