//
//  UIImageView+HG.h
//  HG-Project
//
//  Created by not Pavel on 7/6/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface UIImageView (HG)

- (void)circleForColor:(UIColor *)color shift:(CGFloat)shift size:(CGFloat)size;
- (void)hg_setUnread;
- (void)hg_setFlagged;
- (void)hg_setUnreadAndFlagged;
- (void)hg_setEmptyPasscode:(UIColor *)color second:(UIColor *)second;
- (void)hg_setFilledPasscode:(UIColor *)color;
- (void)hg_setElectronic;
- (void)hg_setHGUser;

@end
