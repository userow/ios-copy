//
//  UICKeyChainStore+HG.h
//  GenericKeyChainTry
//
//  Created by Mykola on 9/21/14.
//  Copyright (c) 2014 Mykola. All rights reserved.
//

#import "UICKeyChainStore.h"

@interface UICKeyChainStore (HG)

/**
 * @method registerDefault:
 * Sets default values for application launch. If there was no previous settings - it would set.
 * @note It doesn't erase previous user settings.
 */
- (void)registerDefaults:(NSDictionary *)registrationDictionary;

/**
 * Getters
 */
- (NSDate *)dateForKey:(NSString *)defaultName;
- (NSArray *)arrayForKey:(NSString *)defaultName;
- (NSDictionary *)dictionaryForKey:(NSString *)defaultName;
- (NSInteger)integerForKey:(NSString *)defaultName;
- (float)floatForKey:(NSString *)defaultName;
- (double)doubleForKey:(NSString *)defaultName;
- (BOOL)boolForKey:(NSString *)defaultName;

/**
 * Setters. Note that after setting values you should call [self synchronize];.
 */
- (void)setDate:(NSDate *)value forKey:(NSString *)defaultName;
- (void)setArray:(NSArray *)value forKey:(NSString *)defaultName;
- (void)setDictionary:(NSDictionary *)value forKey:(NSString *)defaultName;
- (void)setInteger:(NSInteger)value forKey:(NSString *)defaultName;
- (void)setFloat:(float)value forKey:(NSString *)defaultName;
- (void)setDouble:(double)value forKey:(NSString *)defaultName;
- (void)setBool:(BOOL)value forKey:(NSString *)defaultName;


@end
