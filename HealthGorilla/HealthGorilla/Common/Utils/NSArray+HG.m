//
//  NSArray+HG.m
//  HG-Project
//
//  Created by not Pavel on 1/8/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "NSArray+HG.h"

@implementation NSArray (HG)

- (BOOL)containsString:(NSString *)key
{
    for (NSObject *object in self) {
        if ([object isKindOfClass:[NSString class]]) {
            if ([(NSString *)object isEqualToString:key]) {
                return YES;
            }
        }
    }
    
    return NO;
}

@end
