//
//  UIWindow+HG.h
//  HG-Project
//
//  Created by not Pavel on 8/29/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface UIWindow (HG)

- (UIViewController *) visibleViewController;

@end
