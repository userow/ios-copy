//
//  NSString+HG.h
//  HG-Project
//
//  Created by not Pavel on 6/12/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface NSString (HG)

+ (BOOL)isEmpty:(NSString *)source;
+ (NSString *)stripDoubleSpaceFrom:(NSString *)source;
+ (NSString *)superscriptFromInteger:(NSInteger)value;
+ (NSString *)formattedUSFax:(NSString *)fax;
- (NSString *)rawPhone;

- (bool)isValidNPI;
- (bool)isValidEmail;
- (bool)isValidPassword;
- (bool)isStrongPassword;
- (bool)isValidPhone;
- (bool)isValidPhoneWithDigitsCount:(NSInteger)count;

- (NSString *)stringByTrimmingLeadingWhitespace;
- (NSString *)stringByTrimmingTrailingTabs;
- (NSString *)stringByTrimmingWhiteSpaces;
- (NSString *)stringByReplacingHtmlTags;
- (NSString *)stringByReplacingCRWithBRTags;

- (BOOL)hasDigits;
- (BOOL)isHtml;
- (BOOL)isHtmlOrFormated;
- (NSUInteger)numberOfMatches:(NSString *)substring;
- (BOOL)containsDigitsOnly;
- (BOOL)isFloatNumber;
- (BOOL)hasValue;
- (BOOL)isMultilineOrHtml;
- (NSString *)urlQueryPercentEncoded;
- (NSString *)urlPathPercentEncoded;

+ (NSString *)messageDelimiterWithTitle:(NSString *)title font:(UIFont *)font;


/**
 Language used for localizing the code

 @return language code (en / es)
 */
+ (NSString *)usingLanguage;

/**
 Localization of unique key

 @return Localized string
 */
- (NSString *)localized;

@end
