//
//  UIView+HG.m
//  HG-Project
//
//  Created by not Pavel on 7/1/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "UIView+HG.h"
#import <objc/runtime.h>

@implementation UIView (HG)

- (void)setUserInfo:(id)userInfo
{
    objc_setAssociatedObject(self, @selector(userInfo), userInfo, OBJC_ASSOCIATION_RETAIN_NONATOMIC ) ;
}

- (id)userInfo
{
    return objc_getAssociatedObject(self, @selector(userInfo)) ;
}

@end
