//
//  HGMultiImagePicker.m
//  HG-Project
//
//  Created by not Paul on 06/06/2017.
//  Copyright © 2017 HG-Inc. All rights reserved.
//

#import "HGMultiImagePicker.h"


@interface HGMultiImagePicker () <ImagePickerHandler>

@property (nonatomic, copy) HGMultiImagePickerCompletion completion;
@property (nonatomic, strong) HGMultiImagePicker *selfRetain;
@property (nonatomic, strong) ImagePickerWrapper *wrapper;

@end


@implementation HGMultiImagePicker

+ (void)pickWithPresenter:(UIViewController *)presenter
             configurator:(HGMultiImagePickerConfigurator)configurator
               completion:(HGMultiImagePickerCompletion)completion
{
    ImagePickerConfiguration *configuration = [[ImagePickerConfiguration alloc] init];
    if (configurator) {
        configurator(configuration);
    }
    
    ImagePickerWrapper *wrapper = [[ImagePickerWrapper alloc] init];
    
    HGMultiImagePicker *handler = [[HGMultiImagePicker alloc] init];
    handler.completion = completion;
    handler.selfRetain = handler; // retain controoler itself
    handler.wrapper = wrapper;
    
    UIViewController *c = [wrapper createPickerWithHandler:handler configuration:configuration];
    [presenter presentViewController:c animated:YES completion:nil];
}


#pragma mark - ImagePickerHandler

- (void)onDoneWithImages:(NSArray<UIImage *> * _Nullable)images
{
    self.completion(images);
    self.selfRetain = nil; // release self
}

- (void)onCancel
{
    self.selfRetain = nil; // release self
}


@end
