//
//  UIViewController+Logging.m
//  FCForms
//
//  Created by Pavel Wasilenko on 15/11/16.
//  Copyright © 2016 RCG Digital. All rights reserved.
//

#import "UIViewController+Logging.h"

#import <objc/runtime.h>

@implementation UIViewController (Logging)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        NSArray *originalSelectors = @[
//                                       @"viewWillAppear:",
//                                       @"viewDidAppear:",
//                                       @"viewWillDisppear:",
//                                       @"viewDidDisappear:",
                                       @"viewDidLoad",
                                       @"prepareForSegue:sender:",
                                       ];
        NSArray *swizzledSelectors = @[
//                                       @"xxx_viewWillAppear:",
//                                       @"xxx_viewDidAppear:",
//                                       @"xxx_viewWillDisppear:",
//                                       @"xxx_viewDidDisappear:",
                                       @"xxx_viewDidLoad",
                                       @"xxx_prepareForSegue:sender:",
                                       ];
        
        
        for (int i = 0; i < originalSelectors.count; i++) {
            SEL originalSelector = NSSelectorFromString(originalSelectors[i]);
            SEL swizzledSelector = NSSelectorFromString(swizzledSelectors[i]);
            
            Method originalMethod = class_getInstanceMethod(class, originalSelector);
            Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
            
            // When swizzling a class method, use the following:
            // Class class = object_getClass((id)self);
            // ...
            // Method originalMethod = class_getClassMethod(class, originalSelector);
            // Method swizzledMethod = class_getClassMethod(class, swizzledSelector);
            
            BOOL didAddMethod =
            class_addMethod(class,
                            originalSelector,
                            method_getImplementation(swizzledMethod),
                            method_getTypeEncoding(swizzledMethod));
            
            if (didAddMethod) {
                class_replaceMethod(class,
                                    swizzledSelector,
                                    method_getImplementation(originalMethod),
                                    method_getTypeEncoding(originalMethod));
            } else {
                method_exchangeImplementations(originalMethod, swizzledMethod);
            }
        }
    });
}

#pragma mark - Method Swizzling

- (void)xxx_viewWillAppear:(BOOL)animated {
//    NSLog(@"%@", [[NSString stringWithFormat:[@"\n%s [Line %d] :\n" stringByAppendingString:fmt], __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__] stringByAppendingString:@"\n"])
    
    NSLog(@"\n%s [Line %d] :\n viewWillAppear: - START - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
    [self xxx_viewWillAppear:animated];
    NSLog(@"\n%s [Line %d] :\n viewWillAppear: - END - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
}

- (void)xxx_viewDidAppear:(BOOL)animated {
    NSLog(@"\n%s [Line %d] :\n viewDidAppear: - START - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
    [self xxx_viewDidAppear:animated];
    NSLog(@"\n%s [Line %d] :\n viewDidAppear: - END - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
}

- (void)xxx_viewWillDisappear:(BOOL)animated {
    NSLog(@"\n%s [Line %d] :\n viewWillDisappear: - START - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
    [self xxx_viewWillDisappear:animated];
    NSLog(@"\n%s [Line %d] :\n viewWillDisappear: - END - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
}

- (void)xxx_viewDidDisappear:(BOOL)animated {
    NSLog(@"\n%s [Line %d] :\n viewWillDisappear: - START - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
    [self xxx_viewDidDisappear:animated];
    NSLog(@"\n%s [Line %d] :\n viewWillDisappear: - END - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
}

- (void)xxx_viewDidLoad {
    NSLog(@"\n%s [Line %d] :\n viewDidLoad: - START - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
    [self xxx_viewDidLoad];
    NSLog(@"\n%s [Line %d] :\n viewDidLoad: - END - %@\n", __PRETTY_FUNCTION__, __LINE__, self);
}

-(void)xxx_prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"\n%s [Line %d] :\n prepareForSegue: %@ sender: %@ : - START - %@\n", __PRETTY_FUNCTION__, __LINE__, segue, sender, self);
    [self xxx_prepareForSegue:segue sender:sender];
    NSLog(@"\n%s [Line %d] :\n prepareForSegue: %@ sender: %@ : - END - %@\n", __PRETTY_FUNCTION__, __LINE__, segue, sender, self);
}
@end
