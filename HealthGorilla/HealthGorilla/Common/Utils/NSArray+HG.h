//
//  NSArray+HG.h
//  HG-Project
//
//  Created by not Pavel on 1/8/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

@interface NSArray (HG)

- (BOOL)containsString:(NSString *)key;

@end
