//
//  HGImageUploader.h
//  HG-Project
//
//  Created by not Paul on 28.03.16.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

typedef void (^HGImageUploaderCompletion)(HGExternalAttachment *attachment);

@interface HGImageUploader : NSObject

+ (void)uploadImage:(UIImage *)image completion:(HGImageUploaderCompletion)completion;

+ (void)uploadImage:(UIImage *)image
             isBack:(NSNumber *)isBack
             userId:(NSString *)userId
         completion:(HGImageUploaderCompletion)completion;

@end
