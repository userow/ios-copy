//
//  HGImagePicker.h
//  HG-Project
//
//  Created by not Paul on 18.12.15.
//  Copyright © 2015 HG-Inc. All rights reserved.
//

typedef void (^HGImagePickerConfigurator)(UIImagePickerController *picker);
typedef void (^HGImagePickerCompletion)(UIImage *image);

@interface HGImagePicker : NSObject

+ (void)pickWithPresenter:(UIViewController *)presenter
             configurator:(HGImagePickerConfigurator)configurator
               completion:(HGImagePickerCompletion)completion;

+ (bool)isCameraSourceAvailable;
+ (bool)isPhotoLibrarySourceAvailable;

@end
