//
//  HGInstabug.m
//  HG-Project
//
//  Created by not Paul on 30.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGInstabug.h"

#if USE_INSTABUG
#import <Instabug/Instabug.h>
#endif // USE_INSTABUG

@implementation HGInstabug

#if USE_INSTABUG

static bool s_initialized = NO;

+ (void)start
{
    [Instabug startWithToken:kInstabugToken captureSource:IBGCaptureSourceUIKit invocationEvent:IBGInvocationEventScreenshot];
    
    [Instabug setDefaultInvocationMode:IBGInvocationModeBugReporter];
    [Instabug setWillShowEmailField:NO];
    [Instabug setCommentIsRequired:YES];
    [Instabug setOkButtonText:@"OK".localized];
    [Instabug setWillShowTutorialAlert:NO];
    [Instabug setWillShowStartAlert:NO];
    
    // orange color scheme
    [Instabug setButtonsFontColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    [Instabug setButtonsColor:[UIColor colorWithRed:0.74 green:0.21 blue:0.027 alpha:1]];
    [Instabug setHeaderFontColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    [Instabug setHeaderColor:[UIColor colorWithRed:1 green:0.45 blue:0 alpha:1]];
    [Instabug setTextFontColor:[UIColor colorWithRed:0.32 green:0.32 blue:0.32 alpha:1]];
    [Instabug setTextBackgroundColor:[UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1]];
    
    s_initialized = YES;
}


+ (void)reportIssue:(NSString *)title info:(NSString *)info
{
//    NSLog(@"%s: %@, %@", __FUNCTION__, title, info);

    if (!s_initialized) {
        return;
    }
    
    NSString *directory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *path = [directory stringByAppendingPathComponent:@"report.txt"];
    
    NSError *error = nil;
    BOOL succeed = [info writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (succeed){
        [Instabug attachFileAtLocation:path];
        [Instabug reportBugWithComment:title email:@"" screenshot:nil];
    }
}

#else // USE_INSTABUG

+ (void)start {
    // nop
}
+ (void)reportIssue:(NSString *)title info:(NSString *)info {
    // nop
}

#endif // USE_INSTABUG

@end
