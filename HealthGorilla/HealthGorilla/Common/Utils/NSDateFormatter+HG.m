//
//  NSDateFormatter+HG.m
//  HG-Project
//
//  Created by not Pavel on 6/18/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "NSDateFormatter+HG.h"
#import "HGDateFormatterFactory.h"

@implementation NSDateFormatter (HG)

+ (instancetype)formatterByDate:(NSDate *)date
{
    if ([date isToday]) {
        return [HGDateFormatterFactory shortTime];
    } else if ([date isThisWeek]) {
        return [HGDateFormatterFactory weekdayTime];
    }
    return [HGDateFormatterFactory shortDate];
}

@end
