//
//  UIPickerView+NSDate.h
//  HG-Project
//
//  Created by Sergey Gorokhovich on 5/28/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGDate.h"

@interface UIPickerView (NSDate)

- (void)setHGDate:(HGDate *)date;
- (void)selectDefault;

@property (nonatomic, readonly) NSDate *currentDateValue;
@property (nonatomic, readonly) HGDate *currentHGDateValue;

@property (nonatomic, readonly) NSString *selectedTimePickerValue;
@property (nonatomic, readonly) NSUInteger timePickerValuesCount;

- (NSString *)timePickerValueForRow:(NSInteger)row;

@end
