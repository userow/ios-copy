//
//  UILabel+HG.m
//  HG-Project
//
//  Created by not Pavel on 6/19/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "UILabel+HG.h"

@implementation UILabel (HG)

+ (CGSize)sizeForTextHtml:(NSString *)text font:(UIFont *)font frame:(CGRect)frame
{
    UILabel *message = [[UILabel alloc] initWithFrame:frame];
    message.font = font;
    message.numberOfLines = 0;
    message.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    attributes[NSFontAttributeName] = font;
//    NSMutableAttributedString *bodyAttributedText = [[NSMutableAttributedString alloc] initWithString:text];
//    NSMutableParagraphStyle *bodyParagraphStyle = [[NSMutableParagraphStyle alloc] init];
//    [bodyParagraphStyle setLineSpacing:2.0f];
//    [bodyAttributedText addAttribute:NSParagraphStyleAttributeName
//                               value:bodyParagraphStyle
//                               range:NSMakeRange(0, text.length)];
//    wishMessage.attributedText = bodyAttributedText;
    
    NSAttributedString *bodyAttributedText = [[NSAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:&attributes error:nil];
    message.attributedText = bodyAttributedText;
    [message sizeToFit];
    
    if (text) {
        return message.frame.size;
    } else {
        return CGSizeMake(0, 0);
    }
}

+ (CGFloat)fontSizeToFitText:(NSString *)text startSize:(CGFloat)size width:(CGFloat)width font:(NSString *)font
{
    CGFloat __height = [UILabel sizeForText:text font:[UIFont fontWithName:font size:size] frame:CGRectMake(.0f, .0f, 1200.0f, size + 2)].height;
    CGFloat fontSize = size;
    while (fontSize > 0.0)
    {
        CGRect rect = [text boundingRectWithSize:CGSizeMake(10000.0f, 10000.0f)
                                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                      attributes:@{NSFontAttributeName:[UIFont fontWithName:font size:fontSize]}
                                         context:nil];
        if (rect.size.height < __height && rect.size.width <= (width + 8)) {
            break;
        }
        
        fontSize -= 1.0;
    }
    
    return fontSize > 1 ? fontSize : 12.0f;
}

+ (CGFloat)fontSizeToFitHtmlText:(NSString *)text startSize:(CGFloat)size width:(CGFloat)width font:(NSString *)font
{
    CGFloat __height = [UILabel sizeForTextHtml:text font:[UIFont fontWithName:font size:size] frame:CGRectMake(.0f, .0f, 1200.0f, size + 1)].height;
    CGFloat fontSize = size;
    while (fontSize > 0.0)
    {
        CGRect rect = [text boundingRectWithSize:CGSizeMake(320.f, 10000.0f)
                                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                      attributes:@{NSFontAttributeName:[UIFont fontWithName:font size:fontSize], NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                         context:nil];
        if (rect.size.height < __height && rect.size.width <= width) {
            break;
        }
        
        fontSize -= 1.0;
    }
    
    return fontSize > 1 ? fontSize : 12.0f;
}

+ (CGFloat)fontSizeToFitText:(NSString *)text startSize:(CGFloat)size width:(CGFloat)width
{
    CGFloat __height = [UILabel sizeForText:text font:[UIFont systemFontOfSize:size] frame:CGRectMake(.0f, .0f, 1200.0f, size + 1)].height;
    CGFloat fontSize = size;
    while (fontSize > 0.0)
    {
        CGRect rect = [text boundingRectWithSize:CGSizeMake(width, 10000.0f)
                                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                      attributes:@{NSFontAttributeName:[UIFont italicSystemFontOfSize:fontSize]}
                                         context:nil];
        if (rect.size.height < __height) {
            break;
        }
        
        fontSize -= 1.0;
    }
    
    return fontSize > 1 ? fontSize : 12.0f;
}

+ (CGSize)sizeForText:(NSString *)text font:(UIFont *)font frame:(CGRect)frame
{
    UILabel *message = [[UILabel alloc] initWithFrame:frame];
    message.numberOfLines = 0;
    message.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSMutableAttributedString *bodyAttributedText = [[NSMutableAttributedString alloc] initWithString:text
                                                                                           attributes:@{NSFontAttributeName:font}];
    message.attributedText = bodyAttributedText;
    [message sizeToFit];
    
    if (text) {
        return message.frame.size;
    } else {
        return CGSizeMake(0, 0);
    }
}


+ (CGSize)sizeForText:(NSString *)text font:(UIFont *)font maxWidth:(CGFloat)maxWidth
{
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, 4000)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@ {
                                      NSFontAttributeName : font,
                                  } context:nil].size;
    return CGSizeMake(ceil(size.width), ceil(size.height));
}


+ (CGSize)sizeForAttributedText:(NSAttributedString *)text maxWidth:(CGFloat)maxWidth
{
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, 4000)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                     context:nil].size;
    return CGSizeMake(ceil(size.width), ceil(size.height));
}


+ (CGFloat)layoutAttributedLabel:(UILabel *)label x:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth
{
    CGSize size = [UILabel sizeForAttributedText:label.attributedText maxWidth:maxWidth];
    label.frame = CGRectMake(x, y, ceil(size.width), ceil(size.height));
    
    return CGRectGetHeight(label.bounds);
}


+ (CGFloat)layoutLabel:(UILabel *)label x:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth
{
    CGSize size = [UILabel sizeForText:label.text font:label.font maxWidth:maxWidth];
    label.frame = CGRectMake(x, y, ceil(size.width), ceil(size.height));
    
    return CGRectGetHeight(label.bounds);
}


+ (CGFloat)layoutLabel:(UILabel *)label rightx:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth
{
    CGSize size = [UILabel sizeForText:label.text font:label.font maxWidth:maxWidth];
    label.frame = CGRectMake(ceil(x-size.width), y, ceil(size.width), ceil(size.height));
    
    return CGRectGetHeight(label.bounds);
}


+ (CGFloat)layoutLabel:(UILabel *)label centerx:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth
{
    CGSize size = [UILabel sizeForText:label.text font:label.font maxWidth:maxWidth];
    label.frame = CGRectMake(ceil(x-size.width/2), y, ceil(size.width), ceil(size.height));
    
    return CGRectGetHeight(label.bounds);
}

@end
