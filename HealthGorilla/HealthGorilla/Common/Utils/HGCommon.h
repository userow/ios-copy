//
//  HGCommon.h
//  HG-Project
//
//  Created by not Pavel on 7/12/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#ifndef HealthGorilla_HGCommon_h
#define HealthGorilla_HGCommon_h

#define CGRectSetPos( r, x, y ) CGRectMake( x, y, r.size.width, r.size.height )
#define CGRectSetPosX( r, x ) CGRectMake( x, r.origin.y, r.size.width, r.size.height )
#define CGRectSetPosY( r, y ) CGRectMake( r.origin.x, y, r.size.width, r.size.height )

#define TOTAL_SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define TOTAL_SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_6_OR_P (IS_IPHONE_6P || IS_IPHONE_6)

static NSUInteger HGErrorSubmitOrder = 101010;

#endif
