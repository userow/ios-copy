//
//  UIImagePickerController+LayoutFlag.m
//  Health Gorilla
//
//  Created by Alex Rodset on 28/09/2018.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import <objc/runtime.h>
#import "UIImagePickerController+LayoutFlag.h"

static char const * const ObjectTagKey = "ObjectTag";

@implementation UIImagePickerController (LayoutFlag)

- (void)setAddFrameLayout:(BOOL)addFrameLayoutFlag
{
    NSNumber *number = [NSNumber numberWithBool: addFrameLayoutFlag];
    objc_setAssociatedObject(self, ObjectTagKey, number , OBJC_ASSOCIATION_RETAIN);
}

- (BOOL) addFrameLayout
{
    NSNumber *number = objc_getAssociatedObject(self, ObjectTagKey);
    return [number boolValue];
}


@end
