//
//  NSData+HGImageContentType.h
//  HG-Project
//
//  Created by not Paul on 26/02/2017.
//  Copyright © 2017 HG-Inc. All rights reserved.
//

@interface NSData (HGImageContentType)

+ (NSString *)hg_contentTypeForImageData:(NSData *)data;

@end
