//
//  NSDateFormatter+HG.h
//  HG-Project
//
//  Created by not Pavel on 6/18/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface NSDateFormatter (HG)

+ (instancetype)formatterByDate:(NSDate *)date;

@end
