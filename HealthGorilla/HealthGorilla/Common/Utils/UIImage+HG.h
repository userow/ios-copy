//
//  UIImage+HG.h
//  HG-Project
//
//  Created by not Pavel on 5/30/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface UIImage (HG)

- (NSString *)hg_dataURLString;

+ (UIImage *)hg_imageFromDataURLString:(NSString *)string;
+ (UIImage *)hg_imageRectWithSolidColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)hg_imageEllipseWithSolidColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)hg_imageEllipseWithSolidColor:(UIColor *)color dotColor:(UIColor *)dotColor dotRaduis:(CGFloat)dotRaduis size:(CGSize)size;

- (void)hg_dumpWithFileName:(NSString *)fileName;

@end
