//
//  UIActionSheet+HG.h
//  HG-Project
//
//  Created by not Paul on 14.08.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

typedef void (^UIActionSheetBlock)(UIActionSheet * actionSheet);
typedef void (^UIActionSheetButtonBlock)(UIActionSheet *actionSheet, NSUInteger index);

@interface UIActionSheet (HG)

+ (UIActionSheet *)presentOnView:(UIView *)view
                       withTitle:(NSString *)title
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock;

+ (UIActionSheet *)presentOnView:(UIView *)view
                          sender:(id)sender
                       withTitle:(NSString *)title
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock;

+ (UIActionSheet *)presentOnView:(UIView *)view
                       withTitle:(NSString *)title
                    cancelButton:(NSString *)cancelButton
               destructiveButton:(NSString *)destructiveButton
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                   onDestructive:(UIActionSheetBlock)destructiveBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock;

+ (UIActionSheet *)presentOnView:(UIView *)view
                          sender:(id)sender
                       withTitle:(NSString *)title
                    cancelButton:(NSString *)cancelButton
               destructiveButton:(NSString *)destructiveButton
                    otherButtons:(NSArray *)otherButtons
                        onCancel:(UIActionSheetBlock)cancelBlock
                   onDestructive:(UIActionSheetBlock)destructiveBlock
                 onClickedButton:(UIActionSheetButtonBlock)clickBlock;

@end
