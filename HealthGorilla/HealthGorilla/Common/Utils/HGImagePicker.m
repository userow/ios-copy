//
//  HGImagePicker.m
//  HG-Project
//
//  Created by not Paul on 18.12.15.
//  Copyright © 2015 HG-Inc. All rights reserved.
//

#import "HGImagePicker.h"
#import "UIImagePickerController+LayoutFlag.h"


@interface HGImagePicker () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, copy) HGImagePickerCompletion completion;
@property (nonatomic, strong) HGImagePicker *selfRetain;

@end


@implementation HGImagePicker

+ (void)pickWithPresenter:(UIViewController *)presenter
             configurator:(HGImagePickerConfigurator)configurator
               completion:(HGImagePickerCompletion)completion
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    if (configurator) {
        configurator(picker);
    }
    
    if (picker.addFrameLayout){


        picker.cameraOverlayView = [self overlayView];
       
    }
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"_UIImagePickerControllerUserDidCaptureItem" object:nil queue:nil usingBlock:^(NSNotification *note) {
        picker.cameraOverlayView = nil;
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"_UIImagePickerControllerUserDidRejectItem" object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        if (picker.addFrameLayout) {
            picker.cameraOverlayView = [self overlayView];
        }
    }];
    
    HGImagePicker *delegate = [[HGImagePicker alloc] init];
    delegate.completion = completion;
    delegate.selfRetain = delegate; // retain controoler itself

    picker.delegate = delegate;

    [presenter presentViewController:picker animated:YES completion:nil];
}

+(UIView*)overlayView{
    float sideMargin = 15.0;
    float topMargin = 60.0;
    float bottomMargin = 121.0;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(sideMargin, topMargin, [UIScreen mainScreen].bounds.size.width - 2 * sideMargin, [UIScreen mainScreen].bounds.size.height - topMargin - bottomMargin)];
    view.backgroundColor = [UIColor clearColor];
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 4;
    view.layer.opacity = 0.7;
    
    UILabel *textLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, sideMargin * 3, view.bounds.size.height)];
    [view addSubview:textLbl];
    textLbl.backgroundColor = [UIColor clearColor];
    textLbl.textColor = [UIColor whiteColor];
    textLbl.font = [UIFont boldSystemFontOfSize:17];
    textLbl.text = @"Please use landscape mode".localized;
    CGRect frame = textLbl.frame;
    [textLbl setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
    textLbl.frame = frame;
    textLbl.textAlignment = NSTextAlignmentCenter;
    
    return view;
}

+ (bool)isCameraSourceAvailable
{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}


+ (bool)isPhotoLibrarySourceAvailable
{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    HGImagePickerCompletion completion = self.completion;
    [picker dismissViewControllerAnimated:YES completion:^{
        if (completion) {
            UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
            if (!image) {
                image = [info valueForKey:UIImagePickerControllerOriginalImage];
            }
            completion(image);
        }
    }];
    self.selfRetain = nil; // release self
}

-(void)removeAllObservers{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.selfRetain = nil; // release self
}


@end
