//
//  UIViewController+Logging.h
//  FCForms
//
//  Created by Pavel Wasilenko on 15/11/16.
//  Copyright © 2016 RCG Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Logging)

+ (void)load;

@end
