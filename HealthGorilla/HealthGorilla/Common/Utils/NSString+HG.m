//
//  NSString+HG.m
//  HG-Project
//
//  Created by not Pavel on 6/12/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "NSString+HG.h"

@implementation NSString (HG)

+ (BOOL)isEmpty:(NSString *)source
{
    return !source || source.length == 0;
}

+ (NSString *)stripDoubleSpaceFrom:(NSString *)source
{
    if ([self isEmpty:source]) {
        return source;
    }
    NSString *result = [source copy];
    while ([result rangeOfString:@"  "].location != NSNotFound) {
        result = [result stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    return result;
}

+ (NSString *)superscriptFromInteger:(NSInteger)value
{
    NSArray *array = @[@"⁰", @"¹", @"²", @"³", @"⁴", @"⁵", @"⁶", @"⁷", @"⁸", @"⁹"];
    if (value >= 0 && value <= 9) {
        NSString *myString = [NSString stringWithFormat:@"%@", array[value]];
        return myString;
    } else {
        NSString *base = [@(value) stringValue];
        NSMutableString *newString = [[NSMutableString alloc] init];
        for (NSInteger b = 0; b < base.length; b++) {
            NSInteger temp = [[base substringWithRange:NSMakeRange(b, 1)] integerValue];
            [newString appendString:array[temp]];
        }
        
        NSString *returnString = [NSString stringWithString:newString];
        return returnString;
    }
}


+ (NSString *)formattedUSFax:(NSString *)fax
{
    NSString *rawFax = fax.rawPhone;
    if (rawFax) {
        return [NSString stringWithFormat:@"(%@) %@-%@",
                [rawFax substringWithRange:NSMakeRange(0, 3)],
                [rawFax substringWithRange:NSMakeRange(3, 3)],
                [rawFax substringWithRange:NSMakeRange(6, 4)]];
    }
    return nil;
}


- (NSString *)rawPhone
{
    // clean up all except digits
    NSString *number = [self stringByTrimmingWhiteSpaces];
    number = [number stringByReplacingOccurrencesOfString:@"+" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"." withString:@""];
    // FIX: remove conditional formatting left from system copy-paste
    number = [number stringByReplacingOccurrencesOfString:@"\U0000202d" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"\U0000202c" withString:@""];
    
    if (number.hasValue && number.containsDigitsOnly) {
        if ([number hasPrefix:@"1"]) {
            if (number.length == 11) {
                return [number substringFromIndex:1]; // drop US prefix
            }
            return nil;
        }
        if (number.length == 10) {
            return number;
        }
    }
    return nil;
}


- (bool)isValidPhone
{
    return nil != [self rawPhone];
}


- (bool)isValidPhoneWithDigitsCount:(NSInteger)count
{
    NSString *phone = [self rawPhone];
    if (phone) {
        return phone.length == count;
    }
    return NO;
}


- (bool)isValidNPI
{
    const NSInteger kLength = 10;
    if (self.length != kLength) {
        return NO;
    }
    
    unichar digits[kLength];
    for (NSInteger index = 0; index < kLength; ++index) {
        unichar ch = [self characterAtIndex:index];
        if (!isdigit(ch)) {
            return NO;
        }
        digits[index] = ch-'0';
    }
    
    // Calculate the sum
    NSInteger sum = 24; // Add constant 24, to account for the 80840 prefix
    for (NSInteger i = 0; i < kLength - 1; i++) {
        if ((i + 1) % 2 != 0) {
            
            NSString *str = @(digits[i] * 2).stringValue;
            for (NSInteger index = 0; index < str.length; ++index) {
                unichar ch = [str characterAtIndex:index];
                sum += ch-'0';
            }
        } else {
            sum += digits[i];
        }
    }
    // Next higher number ending in zero
    NSInteger nextHireNumber = sum;
    while (nextHireNumber % 10 != 0) {
        nextHireNumber++;
    }
    // Get the check digit
    NSInteger checkDigit = nextHireNumber - sum;
    NSInteger originalCheckDigit = digits[kLength - 1]; // the last digit
    // End
    return originalCheckDigit == checkDigit;
}


- (bool)isValidEmail
{
    if (0 == self.length) {
        return NO;
    }
    bool stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}


- (bool)isValidPassword
{
    // upper & lowercase letters, at least one digit
    NSCharacterSet *allowedChars = [NSCharacterSet alphanumericCharacterSet];
    for (NSInteger i = 0; i < self.length; ++i) {
        const unichar symbol = [self characterAtIndex:i];
        if (![allowedChars characterIsMember:symbol]) {
            return NO;
        }
    }
    return YES;
}


- (bool)isStrongPassword
{
    // upper & lowercase letters, at least one digit, at least 6 symbols
    if (self.length < 8) {
        return NO;
    }
    NSCharacterSet *digits = [NSCharacterSet decimalDigitCharacterSet];
    NSInteger digitsCounter = 0;
    
    NSCharacterSet *uppercases = [NSCharacterSet uppercaseLetterCharacterSet];
    NSInteger uppercaseCounter = 0;
    
    NSCharacterSet *lowercases = [NSCharacterSet lowercaseLetterCharacterSet];
    NSInteger lowercaseCounter = 0;
    
    for (NSInteger i = 0; i < self.length; ++i) {
        const unichar symbol = [self characterAtIndex:i];
        if ([digits characterIsMember:symbol]) {
            ++digitsCounter;
        }
        else if ([uppercases characterIsMember:symbol]) {
            ++uppercaseCounter;
        }
        else if ([lowercases characterIsMember:symbol]) {
            ++lowercaseCounter;
        }
    }
    return (digitsCounter > 0) && (lowercaseCounter > 0) && (uppercaseCounter > 0);
}


- (NSString *)stringByTrimmingLeadingWhitespace
{
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    for (NSInteger i = 0; i < self.length; ++i) {
        if (![whitespaces characterIsMember:[self characterAtIndex:i]]) {
            return [self substringFromIndex:i];
        }
    }
    return @"";
}


- (NSString *)stringByTrimmingTrailingTabs
{
    NSString *text = [self stringByReplacingOccurrencesOfString:@"\t\t\t\t\t\t\t\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\t\t\t\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\t\t\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\t\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\t\n" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"\t\n" withString:@"\n"];
    
    return text;
}

- (NSString *)stringByTrimmingWhiteSpaces
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


- (NSString *)stringByReplacingHtmlTags
{
    NSString *text = [self stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"</br>" withString:@"\n"];
    text = [text stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    return text;
}


- (NSString *)stringByReplacingCRWithBRTags
{
    NSString *text = [self stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    return text;
}


- (BOOL)hasDigits
{
    if (self.length >= 7 && [self rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound) {
        return YES;
    }
    return NO;
}

- (BOOL)isHtml
{
    static NSArray *tags = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tags = @[@"<b>", @"<i>", @"</div>", @"<p>", @"<br>", @"</br>"];
    });
    
    for (NSString *tag in tags) {
        if ([self rangeOfString:tag].location != NSNotFound) {
            return YES;
        }
    }

    return NO;
}


- (BOOL)isHtmlOrFormated
{
    return [self isHtml]
    || [self rangeOfString:@"\t"].location != NSNotFound
    || [self rangeOfString:@"\n"].location != NSNotFound;
}


- (NSUInteger)numberOfMatches:(NSString *)substring
{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:substring
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    return [regex numberOfMatchesInString:self options:0 range:NSMakeRange(0, self.length)];
}


- (BOOL)containsDigitsOnly
{
    if (0 == self.length) {
        return NO;
    }
    for (NSInteger index = 0; index < self.length; ++index) {
        unichar ch = [self characterAtIndex:index];
        if (!isdigit(ch)) {
            return NO;
        }
    }
    return YES;
}


- (BOOL)isFloatNumber
{
    NSScanner *sc = [NSScanner scannerWithString:self];
    // We can pass NULL because we don't actually need the value to test
    // for if the string is numeric. This is allowable.
    if ( [sc scanFloat:NULL] )
    {
        // Ensure nothing left in scanner so that "42foo" is not accepted.
        // ("42" would be consumed by scanFloat above leaving "foo".)
        return [sc isAtEnd];
    }
    // Couldn't even scan a float :(
    return NO;
}


- (BOOL)hasValue
{
    return self.length > 0;
}


- (BOOL)isMultilineOrHtml
{
    return [self isHtml] || [self numberOfMatches:@"\n"] > 0;
}


+ (NSString *)messageDelimiterWithTitle:(NSString *)title font:(UIFont *)font
{
    NSString *spacedTitle = [NSString stringWithFormat:@" %@ ", title];
    CGFloat titleWidth = [spacedTitle boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                                   options:0
                                                attributes:@ {
                                                    NSFontAttributeName : font,
                                                } context:nil].size.width;
    
    NSString *delimiterSymbol = @"-";
    CGFloat delimiterWidth = [delimiterSymbol boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                                           options:0
                                                        attributes:@ {
                                                            NSFontAttributeName : font,
                                                        } context:nil].size.width;
    
    const CGFloat screenWidth = TOTAL_SCREEN_WIDTH;
    const CGFloat delimiterWidthHalf = (screenWidth - titleWidth) * 0.5;
    const NSInteger count = floor(delimiterWidthHalf / delimiterWidth);
    
    unichar delimiterChar = [delimiterSymbol characterAtIndex:0];
    NSString *delimiterHalf = [self stringWithRepeatingCharacter:delimiterChar times:count];
    
    return [[delimiterHalf stringByAppendingString:spacedTitle]
            stringByAppendingString:delimiterHalf];
}


+ (NSString *)stringWithRepeatingCharacter:(unichar)character times:(NSInteger)times
{
    char repeatString[times + 1];
    memset(repeatString, character, times);
    
    // Set terminating null
    repeatString[times] = '\0';
    
    return [NSString stringWithCString:repeatString encoding:NSUTF8StringEncoding];
}


- (NSString *)urlQueryPercentEncoded
{
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}


- (NSString *)urlPathPercentEncoded
{
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
}

#pragma mark - Localization

+ (NSString *)usingLanguage {
//    //1 - taking from Localizable.strings
//    NSString *language = NSLocalizedString(@"language-of-strings-file", @"");
//
//    //2 - in case of bad localization
//    if (!language.hasValue || [language isEqualToString:@"language-of-strings-file"]) {
//        language = [[[NSBundle mainBundle] preferredLocalizations] firstObject];
//
//        if (![language isEqualToString:@"en"] && ![language isEqualToString:@"es"]) {
//            if ([HGAppConfig isAngelesClinical]) {
//                language = @"es";
//            }
//            if ([HGAppConfig isHealthGorilla]) {
//                language = @"en";
//            }
//        }
//
//    }

    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] firstObject];
    
    return language;
}

- (NSString *)localized;
{
    NSString *language = [NSString usingLanguage];
    
    NSBundle *bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]];
//  NSLog(@"%@: %@", lang, NSLocalizedStringFromTableInBundle(@"Language", @"Localizable", bundle, nil));
  
    NSString *localized = NSLocalizedStringFromTableInBundle(self, @"Localizable", bundle, self);
  
    if (!localized.hasValue) {
        localized = [NSString stringWithFormat:@"[%@] %@", language, self];
    }

    if ([HGAppConfig isQAbuild]) {
        NSLog(@"[%@] '%@' : '%@'",  language, self, localized);
    }
    
    return localized;

//    return NSLocalizedString(self, self);
}

@end
