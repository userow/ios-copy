//
//  UILabel+HG.h
//  HG-Project
//
//  Created by not Pavel on 6/19/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface UILabel (HG)

+ (CGSize)sizeForTextHtml:(NSString *)text font:(UIFont *)font frame:(CGRect)frame;
+ (CGSize)sizeForText:(NSString *)text font:(UIFont *)font frame:(CGRect)frame;
+ (CGFloat)fontSizeToFitText:(NSString *)text startSize:(CGFloat)size width:(CGFloat)width;
+ (CGFloat)fontSizeToFitText:(NSString *)text startSize:(CGFloat)size width:(CGFloat)width font:(NSString *)font;
+ (CGFloat)fontSizeToFitHtmlText:(NSString *)text startSize:(CGFloat)size width:(CGFloat)width font:(NSString *)font;

+ (CGSize)sizeForText:(NSString *)text font:(UIFont *)font maxWidth:(CGFloat)maxWidth;
+ (CGSize)sizeForAttributedText:(NSAttributedString *)text maxWidth:(CGFloat)maxWidth;
+ (CGFloat)layoutAttributedLabel:(UILabel *)label x:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth;
+ (CGFloat)layoutLabel:(UILabel *)label x:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth;
+ (CGFloat)layoutLabel:(UILabel *)label rightx:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth;
+ (CGFloat)layoutLabel:(UILabel *)label centerx:(CGFloat)x y:(CGFloat)y maxWidth:(CGFloat)maxWidth;

@end
