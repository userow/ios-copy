//
//  HGWeakTimer.h
//  HG-Project
//
//  Created by not Paul on 04.10.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

typedef void (^HGWeakTimerCompletion)(NSTimer *timer);

@interface HGWeakTimer : NSObject

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval target:(id)target selector:(SEL)selector repeats:(BOOL)repeats;
+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats completion:(HGWeakTimerCompletion)completion;

@end
