//
//  DateFormatterFactory.m
//  HG-Project
//
//  Created by not Paul on 09.07.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGDateFormatterFactory.h"


@interface HGDateFormatterFactory ()

@property (nonatomic, strong) NSCache *cache;

@end


@implementation HGDateFormatterFactory

SYNTHESIZE_SINGLETON(HGDateFormatterFactory);

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;

    _cache = [[NSCache alloc] init];
    
	return self;
}


- (NSDateFormatter *)dateFormatterForKey:(NSString *)key usePosix:(bool)usePosix formatter:(void (^)(NSDateFormatter *))formatter
{
	NSDateFormatter *df = [_cache objectForKey:key];
	if (nil == df) {
		df = [[NSDateFormatter alloc] init];
		
        // set it to true to eliminate local user settings
		if (usePosix) {
			NSLocale *posixLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
			[df setLocale:posixLocale];
		}
        formatter(df);
		[_cache setObject:df forKey:key];
	}
	return df;
}


- (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format usePosix:(bool)usePosix
{
    NSString *key = format;
    return [self dateFormatterForKey:key usePosix:usePosix formatter:^(NSDateFormatter *df) {
       [df setDateFormat:format];
    }];
}


- (NSDateFormatter *)dateFormatterWithDateStyle:(NSDateFormatterStyle)dateStyle timeStyle:(NSDateFormatterStyle)timeStyle usePosix:(bool)usePosix
{
    NSString *key = [NSString stringWithFormat:@"%ld_%ld", (long)dateStyle, (long)timeStyle];
    return [self dateFormatterForKey:key usePosix:usePosix formatter:^(NSDateFormatter *df) {
        df.dateStyle = dateStyle;
        df.timeStyle = timeStyle;
    }];
}

- (NSDateFormatter *)dateFormatterWithDateStyleLocalized:(NSDateFormatterStyle)dateStyle timeStyle:(NSDateFormatterStyle)timeStyle usePosix:(bool)usePosix
{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *key = [NSString stringWithFormat:@"%ld_%ld", (long)dateStyle, (long)timeStyle];
    return [self dateFormatterForKey:key usePosix:usePosix formatter:^(NSDateFormatter *df) {
        df.dateStyle = dateStyle;
        df.timeStyle = timeStyle;
        df.locale = locale;
    }];
}



#pragma mark - Formatters

+ (NSDateFormatter *)prettyDateTime
{
    // Dec 31, 2015 at 8:27 PM
    return [[self shared] dateFormatterWithDateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterShortStyle usePosix:YES];
}


+ (NSDateFormatter *)prettyDate
{
    // Dec 31, 2015
    return [[self shared] dateFormatterWithDateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle usePosix:YES];
}

+ (NSDateFormatter *)prettyDateLocalized
{
    // Dec 31, 2015
    return [[self shared] dateFormatterWithDateStyleLocalized:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle usePosix:YES];
}


+ (NSDateFormatter *)aoeDate
{
    // 2015-01-31
    return [[self shared] dateFormatterWithFormat:@"yyyy-MM-dd" usePosix:YES];
}


+ (NSDateFormatter *)shortDateTime
{
    // 01/31/15 9:32 PM
    return [[self shared] dateFormatterWithFormat:@"MM/dd/yy h:mm a" usePosix:YES];
}


+ (NSDateFormatter *)fullDateTime
{
    // 01/31/2015 9:32 PM
    return [[self shared] dateFormatterWithFormat:@"MM/dd/yyyy h:mm a" usePosix:YES];
}


+ (NSDateFormatter *)shortDate
{
    // 01/31/15
    return [[self shared] dateFormatterWithFormat:@"MM/dd/yy" usePosix:YES];
}


+ (NSDateFormatter *)shortDateYYYY
{
    // 01/31/2015
    return [[self shared] dateFormatterWithFormat:@"MM/dd/yyyy" usePosix:YES];
}


+ (NSDateFormatter *)shortTime
{
    // 9:32 PM
    return [[self shared] dateFormatterWithFormat:@"h:mm a" usePosix:YES];
}


+ (NSDateFormatter *)fullTime
{
    // 09:32 PM
    return [[self shared] dateFormatterWithFormat:@"hh:mm a" usePosix:YES];
}


+ (NSDateFormatter *)weekdayTime
{
    // Wed 9:32 PM
    return [[self shared] dateFormatterWithFormat:@"EEE h:mm a" usePosix:YES];
}


+ (NSDateFormatter *)weekdayDate
{
    // Wed 01/31
    return [[self shared] dateFormatterWithFormat:@"EEE MM/dd" usePosix:YES];
}


+ (NSDateFormatter *)iso
{
    // 2015-01-31T23:23:00.000MSK
    return [[self shared] dateFormatterWithFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ" usePosix:YES];
}


+ (NSDateFormatter *)yyyyMMddHHmmss
{
    // 19701231120000
    return [[self shared ] dateFormatterWithFormat:@"yyyyMMddHHmmss" usePosix:YES];
}


+ (NSDateFormatter *)yyyyMMdd
{
    // 19701231
    return [[self shared ] dateFormatterWithFormat:@"yyyyMMdd" usePosix:YES];
}

@end
