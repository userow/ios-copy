// UIImage+Resize.h
// Created by Trevor Harmon on 8/5/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.

// Extends the UIImage class to support resizing/cropping

@interface UIImage (Resize)

+ (UIImage *)stretchableImageNamed:(NSString *)imgName;

- (UIImage *)croppedImage:(CGRect)bounds;

- (UIImage *)retinaImage;

- (UIImage *)reduceFillInSize:(CGFloat)size
         interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)reduceFitToSize:(CGFloat)size
        interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;
@end
