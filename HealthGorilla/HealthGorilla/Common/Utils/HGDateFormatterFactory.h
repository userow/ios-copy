//
//  DateFormatterFactory.h
//  HG-Project
//
//  Created by not Paul on 09.07.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

@interface HGDateFormatterFactory : NSObject

DECLARE_SINGLETON(HGDateFormatterFactory);

+ (NSDateFormatter *)prettyDateTime;
+ (NSDateFormatter *)prettyDate;
+ (NSDateFormatter *)aoeDate;
+ (NSDateFormatter *)shortDateTime;
+ (NSDateFormatter *)fullDateTime;
+ (NSDateFormatter *)shortDate;
+ (NSDateFormatter *)shortDateYYYY;
+ (NSDateFormatter *)shortTime;
+ (NSDateFormatter *)fullTime;
+ (NSDateFormatter *)weekdayTime;
+ (NSDateFormatter *)weekdayDate;
+ (NSDateFormatter *)iso;
+ (NSDateFormatter *)yyyyMMddHHmmss;
+ (NSDateFormatter *)yyyyMMdd;
+ (NSDateFormatter *)prettyDateLocalized;

@end
