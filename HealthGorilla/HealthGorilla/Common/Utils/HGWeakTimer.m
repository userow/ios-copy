//
//  HGWeakTimer.m
//  HG-Project
//
//  Created by not Paul on 04.10.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGWeakTimer.h"


@interface HGWeakTimer ()
{
    __weak id _target;
    SEL _sel;
}

@property (nonatomic, copy) HGWeakTimerCompletion completion;

@end


@implementation HGWeakTimer

- (instancetype)initWithTarget:(id)target selector:(SEL)sel
{
	if (self = [super init]) {
		_target = target;
		_sel = sel;
	}
	return self;
}

- (instancetype)initWithCompletion:(HGWeakTimerCompletion)completion
{
    if (self = [super init]) {
        self.completion = completion;
    }
    return self;
}

- (void)timerDidFire:(NSTimer *)timer
{
	if (_target) {
		NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:_target selector:_sel object:nil];
		[op.invocation invokeWithTarget:_target];
	}
    else if (_completion) {
        _completion(timer);
    }
	else {
		[timer invalidate];
	}
}

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval target:(id)target selector:(SEL)selector repeats:(BOOL)repeats
{
	HGWeakTimer *proxy = [[self alloc] initWithTarget:target selector:selector];
	return [NSTimer scheduledTimerWithTimeInterval:interval target:proxy selector:@selector(timerDidFire:) userInfo:nil repeats:repeats];
}

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats completion:(HGWeakTimerCompletion)completion
{
    HGWeakTimer *proxy = [[self alloc] initWithCompletion:completion];
    return [NSTimer scheduledTimerWithTimeInterval:interval target:proxy selector:@selector(timerDidFire:) userInfo:nil repeats:repeats];
}

@end
