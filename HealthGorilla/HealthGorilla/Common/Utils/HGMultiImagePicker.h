//
//  HGMultiImagePicker.h
//  HG-Project
//
//  Created by not Paul on 06/06/2017.
//  Copyright © 2017 HG-Inc. All rights reserved.
//

#import "Health_Gorilla-Swift.h"

typedef void (^HGMultiImagePickerConfigurator)(ImagePickerConfiguration *configuration);
typedef void (^HGMultiImagePickerCompletion)(NSArray<UIImage *> *images);

@interface HGMultiImagePicker : NSObject

+ (void)pickWithPresenter:(UIViewController *)presenter
             configurator:(HGMultiImagePickerConfigurator)configurator
               completion:(HGMultiImagePickerCompletion)completion;

@end
