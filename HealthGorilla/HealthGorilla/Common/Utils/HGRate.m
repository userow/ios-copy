//
//  HGRate.m
//  HG-Project
//
//  Created by Mykola on 11/17/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "HGRate.h"
#import "HGUser.h"

@import StoreKit;


@implementation HGRate

+ (void)askForApplicationRate
{
    if (![self shouldDisplayAlert]) {
        return;
    }

    if ([self canDisplayNativeAlert]) {
        [self displayNativeAlert];
    } else {
        [HGRate displayAlert];
    }
    
    [HGUser incrementRateAppCounter];
    [HGUser setRateAppLastTime:[NSDate date]];
}


+ (BOOL)canDisplayNativeAlert
{
    // 10.3+ supported
    return nil != [SKStoreReviewController class];
}


+ (void)displayNativeAlert
{
    [SKStoreReviewController requestReview];
}


+ (BOOL)shouldDisplayAlert
{
    if ([HGUser didRateAppDone]) {
        return NO;
    }

    NSDate *lastAppearanceDate = [HGUser rateAppLastTime];
    if (!lastAppearanceDate) {
        [HGUser setRateAppLastTime:[NSDate date]];
        return NO;
    }
    
    NSInteger appearanceCounter = [HGUser rateAppCounter];
    
    const NSTimeInterval secondsPerHour = 60 * 60;
    const NSTimeInterval secondsPerDay = 24 * secondsPerHour;
    
    NSTimeInterval daysDelay = 1;
    if (appearanceCounter > 0) {
        daysDelay = 5;
    }
    
    const NSTimeInterval delayBetweenAppearances = daysDelay * secondsPerDay;
    
    NSTimeInterval timeFromLastAppearance = [[NSDate date] timeIntervalSinceDate:lastAppearanceDate];
    if (timeFromLastAppearance < delayBetweenAppearances) {
        return NO;
    }
    return YES;
}


+ (void)openRatePage
{
    NSURL *itunesAppURL = [NSURL URLWithString:[self linkForRate]];
    if ([[UIApplication sharedApplication] canOpenURL:itunesAppURL]) {
        [[UIApplication sharedApplication] openURL:itunesAppURL];
        [HGUser setRateAppDone];
    }
}


+ (NSString *)linkForRate
{
    //done: //LOC: change for AC / IC
    NSString *stringFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";
    
    return [NSString stringWithFormat:stringFormat, [HGAppConfig HGAppStoreID]];
}


+ (void)displayAlert
{
    NSString *title = @"Rate us!".localized;
    NSString *message = [NSString stringWithFormat:@"If you enjoy using %@ app, please take a moment to rate it. Thanks for your support!".localized, [HGAppConfig appNameWhitelabeled]];
    
    PSPDFAlertView *likeAlert = [HGAlertView alertViewWithTitle:title andMessage:message];
    [likeAlert addButtonWithTitle:@"Later".localized block:^(NSInteger buttonIndex) {
        //[HGMixpanel report:HGAlertUserChoseYES];
        // nop
    }];
    [likeAlert setCancelButtonWithTitle:@"Rate it now".localized block:^(NSInteger buttonIndex) {
        //[HGMixpanel report:HGRatesAlertUserChoseRate];
        [self openRatePage];
    }];
    [likeAlert show];
}

@end
