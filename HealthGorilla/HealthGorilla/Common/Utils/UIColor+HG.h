//
//  UIColor+HG.h
//  HG-Project
//
//  Created by not Pavel on 6/2/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#define UIRGB(R,G,B) [UIColor colorWithRed:(R) green:(G) blue:(B)]
#define UIRGBA(R,G,B,A) [UIColor colorWithRed:(R)/255. green:(G)/255. blue:(B)/255. alpha:A]

@interface UIColor (HG)

+ (UIColor *)colorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue;
+ (void)addLinearGradientToView:(UIView *)theView withColor:(UIColor *)theColor transparentToOpaque:(BOOL)transparentToOpaque;

+ (UIColor *)contactsInstantGreen;
+ (UIColor *)contactsCaptionGrey;
+ (UIColor *)tableViewSectionHeaderTextGrey;
+ (UIColor *)tableViewSectionHeaderTextDarkGrey;
+ (UIColor *)tableViewBackgroundLightGrey;
+ (UIColor *)tableViewBackgroundLight;
+ (UIColor *)tableViewSectionHeaderBackgroundLightGrey;
+ (UIColor *)hgBlue;
+ (UIColor *)hgBlue:(CGFloat)alpha;

@end
