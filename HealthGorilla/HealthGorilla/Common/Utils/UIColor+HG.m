//
//  UIColor+HG.m
//  HG-Project
//
//  Created by not Pavel on 6/2/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "UIColor+HG.h"

@implementation UIColor (HG)

+ (UIColor *)colorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue
{
    return [UIColor colorWithRed:((CGFloat)red)/255.0f green:((CGFloat)green)/255.0f blue:((CGFloat)blue)/255.0f alpha:1.0f];
}

+ (void)addLinearGradientToView:(UIView *)theView withColor:(UIColor *)theColor transparentToOpaque:(BOOL)transparentToOpaque
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    //the gradient layer must be positioned at the origin of the view
    CGRect gradientFrame = theView.frame;
    gradientFrame.origin.x = 0;
    gradientFrame.origin.y = 0;
    gradient.frame = gradientFrame;
    
    //build the colors array for the gradient
    NSArray *colors = @[(id)[theColor CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.9f] CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.4f] CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
                       (id)[[UIColor clearColor] CGColor]];
    
    //reverse the color array if needed
    if(transparentToOpaque)
    {
        colors = [[colors reverseObjectEnumerator] allObjects];
    }
    
    //apply the colors and the gradient to the view
    gradient.colors = colors;
    
    [theView.layer insertSublayer:gradient atIndex:0];
}

+ (UIColor *)contactsInstantGreen
{
    return [UIColor colorWithRed:150 green:200 blue:90];
}

+ (UIColor *)contactsCaptionGrey
{
    return [UIColor colorWithRed:115 green:115 blue:115];
}

+ (UIColor *)tableViewSectionHeaderTextGrey
{
    return [UIColor colorWithRed:131.0f green:130.0f blue:130.0f];
}

+ (UIColor *)tableViewSectionHeaderTextDarkGrey
{
    return [UIColor colorWithRed:112.0f green:112.0f blue:111.0f];
}

+ (UIColor *)tableViewBackgroundLightGrey
{
    return [UIColor colorWithRed:235.0f green:235.0f blue:241.0f];
}

+ (UIColor *)tableViewBackgroundLight
{
    return [UIColor colorWithRed:224.0f green:224.0f blue:224.0f];
}

+ (UIColor *)tableViewSectionHeaderBackgroundLightGrey
{
    return [UIColor colorWithRed:244.0f green:244.0f blue:244.0f];
}

+ (UIColor *)hgBlue:(CGFloat)alpha
{
    return [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0f alpha:alpha];
}

+ (UIColor *)hgBlue
{
    return [UIColor hgBlue:1.0];
}

@end
