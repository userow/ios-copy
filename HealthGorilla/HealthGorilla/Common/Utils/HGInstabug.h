//
//  HGInstabug.h
//  HG-Project
//
//  Created by not Paul on 30.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

@interface HGInstabug : NSObject

+ (void)start;
+ (void)reportIssue:(NSString *)title info:(NSString *)info;

@end
