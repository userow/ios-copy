//
//  HGMixpanel.h
//  HG-Project
//
//  Created by not Pavel on 8/8/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#define DEF_STR(name) __unsafe_unretained NSString * const name

extern const struct HGMixpanelEvent {
    
    // Login
    DEF_STR(loginPWD);
    DEF_STR(loginPIN);
    DEF_STR(resetPIN);
    
    // MD Tabs
    DEF_STR(tabInbox);
    DEF_STR(tabPatients);
    DEF_STR(tabSettings);
    
    // MD Inbox
    DEF_STR(inboxResults);
    DEF_STR(inboxDocuments);
    DEF_STR(inboxAlerts);
    DEF_STR(inboxMessages);
    DEF_STR(inboxFaxes);
    DEF_STR(inboxReferrals);
    DEF_STR(inboxOrders);
    DEF_STR(inboxEncounters);
    
    // MD Inbox - Plus button
    DEF_STR(inboxPlusButton);
    DEF_STR(inboxPlusOrder);
    DEF_STR(inboxPlusMessage);
    DEF_STR(inboxPlusReferral);
    DEF_STR(inboxPlusPatient);
    DEF_STR(inboxPlusEncounter);
    
    // Lists
    DEF_STR(listFilter);
    DEF_STR(listItemResult);
    DEF_STR(listItemDocument);
    DEF_STR(listItemAlert);
    DEF_STR(listItemMessage);
    DEF_STR(listItemFax);
    DEF_STR(listItemReferral);
    DEF_STR(listItemOrder);
    DEF_STR(listItemEncounter);
    
    // Filter
    DEF_STR(filterShowAll);
    DEF_STR(filterFolder);
    DEF_STR(filterPriority);
    DEF_STR(filterDoctors);
    DEF_STR(filterTypes);
    DEF_STR(filterFaxes);
    DEF_STR(filterLocations);
    
    // Interaction Details
    DEF_STR(patientPhone);
    DEF_STR(patientChart);
    DEF_STR(actionReply);
    DEF_STR(actionForward);
    DEF_STR(actionSign);
    DEF_STR(actionFlag);
    DEF_STR(actionUnread);
    DEF_STR(actionArchive);
    DEF_STR(actionRestore);
    DEF_STR(actionComplete);
    DEF_STR(actionReopen);
    DEF_STR(actionAddAction);
    DEF_STR(actionEdit);
    DEF_STR(attachmentInteraction);
    DEF_STR(attachmentBinary);
    DEF_STR(detailsResults);
    
    // MD Patients
    DEF_STR(patientsSearch);
    
    // MD Patient's Chart
    DEF_STR(patientChartDemographics);
    DEF_STR(patientChartInsurances);
    DEF_STR(patientChartResults);
    DEF_STR(patientChartDocuments);
    DEF_STR(patientChartMessages);
    DEF_STR(patientChartReferrals);
    DEF_STR(patientChartOrders);
    DEF_STR(patientChartDiagnoses);
    DEF_STR(patientChartMedications);
    DEF_STR(patientChartAllergies);
    DEF_STR(patientChartImmunizations);
    DEF_STR(patientChartVitalSigns);
    DEF_STR(patientChartProcedures);
    DEF_STR(patientChartFamilyHistory);
    DEF_STR(patientChartSocialHistory);
    DEF_STR(patientChartEncounters);
    DEF_STR(patientChartPlanOfCare);
    DEF_STR(patientChartMedEquipment);
    DEF_STR(patientChartFuncStatus);
    
    // MD Patient's Chart - Plus Button
    DEF_STR(patientChartPlusButton);
    DEF_STR(patientChartPlusOrder);
    DEF_STR(patientChartPlusReferral);
    DEF_STR(patientChartPlusMessage);
    
    // MD Settings
    DEF_STR(settingsProfile);
    DEF_STR(settingsPasscode);
    DEF_STR(settingsSupport);
    DEF_STR(settingsSignOut);
    
    // Settings - Passcode
    DEF_STR(passcodeOn);
    DEF_STR(passcodeOff);
    DEF_STR(passcodeChange);

} HGMP;


@interface HGMixpanel : NSObject

+ (void)start;
+ (void)report:(NSString *)event;
+ (void)reportTapOnBinaryAttachmentWithContentType:(NSString *)contentType;
+ (void)setUserIdentity:(NSString *)identity;

@end
