//
//  NSBundle+HG.h
//  HG-Project
//
//  Created by not Paul on 30.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

@interface NSBundle (HG)

+ (NSString *)shortVersion;
+ (NSString *)version;
+ (NSString *)displayName;
+ (NSString *)URLScheme;

@end
