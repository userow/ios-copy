//
//  UIImageView+HG.m
//  HG-Project
//
//  Created by not Pavel on 7/6/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "UIImageView+HG.h"

@implementation UIImageView (HG)

- (void)circleForColor:(UIColor *)color shift:(CGFloat)shift size:(CGFloat)size
{
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    circleLayer.rasterizationScale = [[UIScreen mainScreen] scale];
    
    // Give the layer the same bounds as your image view
    [circleLayer setBounds:CGRectMake(0.0f, 0.0f, [self bounds].size.width,
                                      [self bounds].size.height)];
    // Position the circle anywhere you like, but this will center it
    // In the parent layer, which will be your image view's root layer
    [circleLayer setPosition:CGPointMake([self bounds].size.width/2.0f,
                                         [self bounds].size.height/2.0f)];
    // Create a circle path.
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:
                          CGRectMake(shift, shift, size, size)];
    // Set the path on the layer
    [circleLayer setPath:[path CGPath]];
    // Set the stroke color
    [circleLayer setStrokeColor:[color CGColor]];
    [circleLayer setFillColor:[color CGColor]];
    // Set the stroke line width
    [circleLayer setLineWidth:2.0f];
    
    [self.layer addSublayer:circleLayer];
}


- (void)hg_setUnread
{
    [self removeAllSublayers];
    
    UIColor *blue = UIRGB(0, 122, 255);
    [self circleForColor:blue shift:0 size:[self bounds].size.width];
}


- (void)hg_setFlagged
{
    [self removeAllSublayers];
    
    UIColor *red = UIRGB(255, 59, 48);
    [self circleForColor:red shift:0 size:[self bounds].size.width];
}


- (void)hg_setUnreadAndFlagged
{
    [self removeAllSublayers];
    
    UIColor *red = UIRGB(255, 59, 48);
    [self circleForColor:red shift:0 size:[self bounds].size.width];
    
    [self circleForColor:[UIColor whiteColor] shift:1.5 size:7];
    
    UIColor *blue = UIRGB(0, 122, 255);
    [self circleForColor:blue shift:2.5 size:5];
}


- (void)hg_setElectronic
{
    [self removeAllSublayers];
    
    UIColor *green = UIRGB(58, 170, 53);
    [self circleForColor:green shift:0 size:[self bounds].size.width];
    
    UILabel *eLabel = [[UILabel alloc] initWithFrame:self.bounds];
    eLabel.text = @"E";
    eLabel.textColor = [UIColor whiteColor];
    eLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:eLabel];
}


- (void)hg_setHGUser
{
    [self removeAllSublayers];
    
    UIColor *color = UIRGB(12, 136, 196);
    [self circleForColor:color shift:0 size:[self bounds].size.width];
    
    UILabel *eLabel = [[UILabel alloc] initWithFrame:self.bounds];
    eLabel.text = @"HG";
    eLabel.textColor = [UIColor whiteColor];
    eLabel.textAlignment = NSTextAlignmentCenter;
    eLabel.font = [UIFont systemFontOfSize:10];
    
    [self addSubview:eLabel];
}


- (void)hg_setEmptyPasscode:(UIColor *)color second:(UIColor *)second
{
    [self removeAllSublayers];
    [self circleForColor:color shift:0 size:[self bounds].size.width];
    [self circleForColor:second shift:1 size:[self bounds].size.width - 2];
}


- (void)hg_setFilledPasscode:(UIColor *)color
{
    [self removeAllSublayers];
    [self circleForColor:color shift:0 size:[self bounds].size.width];
}


- (void)removeAllSublayers
{
    while (self.layer.sublayers.count > 0) {
        [self.layer.sublayers.lastObject removeFromSuperlayer];
    }
}

@end
