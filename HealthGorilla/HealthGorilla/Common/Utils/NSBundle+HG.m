//
//  NSBundle+HG.m
//  HG-Project
//
//  Created by not Paul on 30.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "NSBundle+HG.h"

@implementation NSBundle (HG)

+ (NSString *)shortVersion
{
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
}

+ (NSString *)version
{
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
}

+ (NSString *)displayName
{
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"];
}

+ (NSString *)URLScheme
{
    NSArray *types = [[NSBundle mainBundle] infoDictionary][@"CFBundleURLTypes"];
    NSDictionary *type = types.firstObject; // caution: expected single URL Type
    NSArray *schemes = type[@"CFBundleURLSchemes"];
    return schemes.firstObject; // caution: expected single URL scheme in selected URL Type
}

@end
