//
//  HGMixpanel.m
//  HG-Project
//
//  Created by not Pavel on 8/8/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "HGMixpanel.h"
#import <Mixpanel/Mixpanel.h>


const struct HGMixpanelEvent HGMP = {
    
    // Login
    .loginPWD = @"Mobile_Login",
    .loginPIN = @"Mobile_PIN_Entered",
    .resetPIN = @"Mobile_PIN_Screen_Reset",
    
    // MD Tabs
    .tabInbox = @"Mobile_Inbox_Tab",
    .tabPatients = @"Mobile_PT_Tab",
    .tabSettings = @"Mobile_Settings_Tab",
    
    // MD Inbox
    .inboxResults = @"Mobile_Home_Results_Category",
    .inboxDocuments = @"Mobile_Home_Documents_Category",
    .inboxAlerts = @"Mobile_Home_Alerts_Category",
    .inboxMessages = @"Mobile_Home_Messages_Category",
    .inboxFaxes = @"Mobile_Home_Faxes_Category",
    .inboxReferrals = @"Mobile_Home_Referrals_Category",
    .inboxOrders = @"Mobile_Home_Orders_Category",
    .inboxEncounters = @"Mobile_Home_Encounters_Category",
    
    // MD Inbox - Plus button
    .inboxPlusButton = @"Mobile_Home_Plus_Button",
    .inboxPlusOrder = @"Mobile_Home_Plus_Button_Order",
    .inboxPlusMessage = @"Mobile_Home_Plus_Button_Message",
    .inboxPlusReferral = @"Mobile_Home_Plus_Button_Referral",
    .inboxPlusPatient = @"Mobile_Home_Plus_Button_Patient",
    .inboxPlusEncounter = @"Mobile_Home_Plus_Button_Encounter",
    
    // Lists
    .listFilter = @"Mobile_List_Filter_Button",
    .listItemResult = @"Mobile_List_Item_Result",
    .listItemDocument = @"Mobile_List_Item_Document",
    .listItemAlert = @"Mobile_List_Item_Alert",
    .listItemMessage = @"Mobile_List_Item_Message",
    .listItemFax = @"Mobile_List_Item_Fax",
    .listItemReferral = @"Mobile_List_Item_Referral",
    .listItemOrder = @"Mobile_List_Item_Order",
    .listItemEncounter = @"Mobile_List_Item_Encounter",
    
    // Filter
    .filterShowAll = @"Mobile_List_Filter_ShowAll",
    .filterFolder = @"Mobile_List_Filter_Status",
    .filterPriority = @"Mobile_List_Filter_Priority",
    .filterDoctors = @"Mobile_List_Filter_Doctors",
    .filterTypes = @"Mobile_List_Filter_Types",
    .filterFaxes = @"Mobile_List_Filter_Faxes",
    .filterLocations = @"Mobile_List_Filter_Locations",
    
    // Interaction Details
    .patientPhone = @"Mobile_Details_PT_Phone",
    .patientChart = @"Mobile_Details_PT_Chart",
    .actionReply = @"Mobile_Details_Action_Button_Reply",
    .actionForward = @"Mobile_Details_Action_Button_Forward",
    .actionSign = @"Mobile_Details_Action_Button_Sign",
    .actionFlag = @"Mobile_Details_Action_Button_Flag",
    .actionUnread = @"Mobile_Details_Action_Button_Unread",
    .actionArchive = @"Mobile_Details_Action_Button_Archive",
    .actionRestore = @"Mobile_Details_Action_Button_Restore",
    .actionComplete = @"Mobile_Details_Action_Button_Complete",
    .actionReopen = @"Mobile_Details_Action_Button_Reopen",
    .actionAddAction = @"Mobile_Details_Action_Button_AddAction",
    .actionEdit = @"Mobile_Details_Action_Button_Edit",
    .attachmentInteraction = @"Mobile_Details_Attachment_PT_Chart",
    .attachmentBinary = @"Mobile_Details_Attachment_", // + truncated MIME type
    .detailsResults = @"Mobile_Details_Results",
    
    // MD Patients
    .patientsSearch = @"Mobile_PT_Search",
    
    // MD Patient's Chart
    .patientChartDemographics = @"Mobile_PT_Demographics",
    .patientChartInsurances = @"Mobile_PT_Insurances",
    .patientChartResults = @"Mobile_PT_Results_Category",
    .patientChartDocuments = @"Mobile_PT_Documents_Category",
    .patientChartMessages = @"Mobile_PT_Messages_Category",
    .patientChartReferrals = @"Mobile_PT_Referrals_Category",
    .patientChartOrders = @"Mobile_PT_Orders_Category",
    .patientChartDiagnoses = @"Mobile_PT_Diagnoses_Category",
    .patientChartMedications = @"Mobile_PT_Medications_Category",
    .patientChartAllergies = @"Mobile_PT_Allergies_Category",
    .patientChartImmunizations = @"Mobile_PT_Immunizations_Category",
    .patientChartVitalSigns = @"Mobile_PT_VitalSigns_Category",
    .patientChartProcedures = @"Mobile_PT_Procedures_Category",
    .patientChartFamilyHistory = @"Mobile_PT_FamilyHistory_Category",
    .patientChartSocialHistory = @"Mobile_PT_SocialHistory_Category",
    .patientChartEncounters = @"Mobile_PT_Encounters_Category",
    .patientChartPlanOfCare = @"Mobile_PT_PlanOfCare_Category",
    .patientChartMedEquipment = @"Mobile_PT_MedEquipment_Category",
    .patientChartFuncStatus = @"Mobile_PT_FuncStatus_Category",
    
    // MD Patient's Chart - Plus Button
    .patientChartPlusButton = @"Mobile_PT_Plus_Button",
    .patientChartPlusOrder = @"Mobile_PT_Plus_Button_Order",
    .patientChartPlusReferral = @"Mobile_PT_Plus_Button_Referral",
    .patientChartPlusMessage = @"Mobile_PT_Plus_Button_Message",
    
    // MD Settings
    .settingsProfile = @"Mobile_Settings_Profile",
    .settingsPasscode = @"Mobile_Settings_Passcode",
    .settingsSupport = @"Mobile_Settings_Help",
    .settingsSignOut = @"Mobile_Settings_Sign_Out",
    
    // Settings - Passcode
    .passcodeOn = @"Mobile_Settings_Passcode_On",
    .passcodeOff = @"Mobile_Settings_Passcode_Off",
    .passcodeChange = @"Mobile_Settings_Passcode_Change_Passcode",
};


static bool s_initialized = NO;

@implementation HGMixpanel

+ (void)start
{
    [Mixpanel sharedInstanceWithToken:kMixpanelToken];
    s_initialized = YES;
}


+ (void)setUserIdentity:(NSString *)identity
{
    if (!s_initialized) {
        return;
    }
    [[Mixpanel sharedInstance] identify:identity];
}


+ (void)report:(NSString *)event
{
    if (!s_initialized) {
        return;
    }
    if (!event) {
        return;
    }
#if defined(HG_QA)
    NSLog(@"Mixpanel: %@", event);
#endif
    [[Mixpanel sharedInstance] track:event];
}


+ (void)reportTapOnBinaryAttachmentWithContentType:(NSString *)contentType
{
    if (!s_initialized) {
        return;
    }
    if (!contentType) {
        return;
    }
    NSString *mime = [contentType componentsSeparatedByString:@"/"].lastObject;
    if (!mime) {
        return;
    }
    NSString *event = [HGMP.attachmentBinary stringByAppendingString:mime];
    [self report:event];
}

@end
