//
//  ImagePickerWrapper.swift
//  HG-Project
//
//  Created by not Paul on 04/06/2017.
//  Copyright © 2017 HG-Inc. All rights reserved.
//

import Foundation
import ImagePicker


final class ImagePickerConfiguration: NSObject {
    
    fileprivate var configuration = Configuration()
    
    @objc var imageLimit = 0
    @objc var allowMultiplePhotoSelection: Bool {
        get {
            return configuration.allowMultiplePhotoSelection
        }
        set {
            configuration.allowMultiplePhotoSelection = newValue
        }
    }
}


@objc protocol ImagePickerHandler {
    
    func onDone(images: [UIImage]?)
    func onCancel()
}


final class ImagePickerWrapper: NSObject {

    fileprivate weak var handler: ImagePickerHandler!

    @objc(createPickerWithHandler:configuration:)
    func createPicker(handler: ImagePickerHandler, configuration: ImagePickerConfiguration) -> UIViewController {
        
        self.handler = handler

        let controller = ImagePickerController(configuration: configuration.configuration)
        controller.imageLimit = configuration.imageLimit
        controller.delegate = self
        
        return controller
    }
}


extension ImagePickerWrapper: ImagePickerDelegate {
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true) {
            self.handler.onDone(images: images)
        }
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true) {
            self.handler.onDone(images: images)
        }
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        self.handler.onCancel()
    }
}
