//
//  UIImage+HG.m
//  HG-Project
//
//  Created by Pavel Vasilenko on 18-08-14.
//  Copyright (c) 2018 HG-Inc. All rights reserved.
//

#import <NSData_Base64/NSData+Base64.h>

@implementation UIImage(Base64)

+ (UIImage *)imageFromBase64:(NSString *)base64;
{
    UIImage *img = nil;
    
    if (base64.hasValue) {
        NSData *data = [NSData dataFromBase64String:base64];
        img = [UIImage imageWithData:data];
    }
    
    return img;
}

- (NSString *)base64StringFromImage;
{
    NSData *imageData = UIImageJPEGRepresentation(self, 1.0);
    NSString * base64String = [imageData base64EncodedString];
    return base64String;
}

@end
