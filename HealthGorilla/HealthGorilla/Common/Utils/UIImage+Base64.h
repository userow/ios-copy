//
//  UIImage+HG.h
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-14.
//  Copyright (c) 2018 HG-Inc. All rights reserved.
//

@interface UIImage (Base64)

+ (UIImage *)imageFromBase64:(NSString *)base64;

- (NSString *)base64StringFromImage;

@end
