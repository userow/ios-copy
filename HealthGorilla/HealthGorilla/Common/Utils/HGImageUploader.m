//
//  HGImageUploader.m
//  HG-Project
//
//  Created by not Paul on 28.03.16.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGImageUploader.h"
#import "UIImage+Resize.h"


@implementation HGImageUploader

+ (void)uploadImage:(UIImage *)image
         completion:(HGImageUploaderCompletion)completion;
{
    [HGImageUploader uploadImage:image
                          isBack:nil
                          userId:nil
                      completion:completion];
}

+ (void)uploadImage:(UIImage *)image
             isBack:(NSNumber *)isBack
             userId:(NSString *)userId
         completion:(HGImageUploaderCompletion)completion
{
    // reduce & compress image
    const CGFloat reducer = 0.5; // % of width
    const CGFloat reducedSize = image.size.width * reducer;
    UIImage *photo = [image reduceFitToSize:reducedSize interpolationQuality:kCGInterpolationDefault];
    
    const CGFloat compressionQuality = 0.8; // JPEG compression
    NSData *imageData = UIImageJPEGRepresentation(photo, compressionQuality);
    
    // upload photo and attach its ID to message
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD show];
    
    HGIdologyImageType type = HGIdologyImageTypeUnknown;
    
    if (isBack) {
        if (isBack.boolValue) {
            type = HGIdologyImageTypeBack;
        } else {
            type = HGIdologyImageTypeFront;
        }
    }
    
    [[HGApiClient shared] uploadImageWithData:imageData
                                         name:@"photo"
                                         type:type
                                pendingUserId:userId
                                  resultBlock:^(HGExternalAttachment *attachment, NSError *error)
    {
        
        [SVProgressHUD dismiss];
        
        if (error) {
            NSString *message = error.localizedDescription ?: @"Failed to upload an image".localized;
            PSPDFAlertView *alert = [HGAlertView alertViewWithTitle:@"Upload".localized andMessage:message];
            [alert setCancelButtonWithTitle:@"OK".localized block:nil];
            [alert show];
        }
        
        completion(attachment);
    }];
}

@end
