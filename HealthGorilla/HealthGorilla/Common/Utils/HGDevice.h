//
//  HGDevice.h
//  HG-Project
//
//  Created by not Pavel on 10.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

@interface HGDevice : NSObject

+ (NSString *)modelName;
+ (bool)isSimulator;
+ (bool)isPhone;
+ (bool)isPad;
+ (NSString *)systemVersion;
+ (float)systemVersionFloat;
+ (NSInteger)majorSystemVersion;
+ (bool)canMakePhoneCalls;

@end
