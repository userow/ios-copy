//
//  HGCrashReporter.h
//  HG-Project
//
//  Created by not Pavel on 8/8/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface HGCrashReporter : NSObject

DECLARE_SINGLETON(HGCrashReporter);

+ (void)enableCrashReporter;
+ (bool)hasPendingCrashReport;
+ (void)promptToSendPendingCrashReportIn:(UIViewController *)controller;

@end
