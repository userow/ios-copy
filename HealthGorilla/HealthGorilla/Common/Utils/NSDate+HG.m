//
//  NSDate+HG.m
//  HG-Project
//
//  Created by not Pavel on 6/18/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "NSDate+HG.h"

@implementation NSDate (HG)
/*
- (NSInteger)yearsSinceNow
{
    NSInteger todayYear = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                          fromDate:[NSDate date]].year;
    NSInteger targetYear = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                             fromDate:self].year;
    NSInteger targetDay =  [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                    fromDate:self].day;
    NSInteger todayDay =  [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                           fromDate:[NSDate date]].day;
    
    NSInteger targetMonth =  [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                           fromDate:self].month;
    NSInteger todayMonth =  [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                          fromDate:[NSDate date]].month;

    NSUInteger years = todayYear - targetYear;
    
    if (targetMonth > todayMonth) {
        return years - 1;
    }
    
    if (targetMonth == todayMonth) {
        if (targetDay > todayDay) {
            return years - 1;
        }
    }
    
    return years;
}
*/
/*
+ (NSDate *)convertGMTtoLocal:(NSString *)gmtDateStr
{
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSTimeZone *local = [NSTimeZone systemTimeZone];
    
    [formatter setTimeZone:local];
    
    NSDate *localDate = [formatter dateFromString:gmtDateStr]; // get the date
    
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    
    NSTimeInterval localTimeInterval = [localDate timeIntervalSinceReferenceDate] + timeZoneOffset;
    
    NSDate *localCurrentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:localTimeInterval];
    return localCurrentDate;
}
*/
@end
