//
//  HGDevice.h
//  HG-Project
//
//  Created by not Pavel on 10.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGDevice.h"
#import <sys/utsname.h>

@import CoreTelephony;


@implementation HGDevice

+ (NSString *)modelId
{
	struct utsname systemInfo;
	uname(&systemInfo);
	
	return @(systemInfo.machine);
}


+ (NSString *)modelName
{
	NSString *platform = [self modelId];
    
	if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
	if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
	if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
	if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
	if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4 (Verizon)";
	if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
	if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
	if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
	if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
	if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (Global)";
	if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
	if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (Global)";
	if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
	if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    
	if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
	if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
	if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
	if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
	if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPod7,1"])      return @"iPod Touch 6G";
    
	if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    
	if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
	if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
	if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
	if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    
	if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini 1 (WiFi)";
	if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini 1 (GSM)";
	if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini 1 (GSM+CDMA)";
    
	if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
	if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
	if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    
	if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
	if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
	if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    
	if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
	if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (GSM)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air (GSM+CDMA)";
    
	if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2";
	if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2";
    
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3";
    
    if ([platform isEqualToString:@"iPad5,1"])      return @"iPad Mini 4";
    if ([platform isEqualToString:@"iPad5,2"])      return @"iPad Mini 4";
    
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2";
    
    if ([platform isEqualToString:@"iPad6,3"])      return @"iPad Pro (9.7 inch)";
    if ([platform isEqualToString:@"iPad6,4"])      return @"iPad Pro (9.7 inch)";
    if ([platform isEqualToString:@"iPad6,7"])      return @"iPad Pro (12.9 inch)";
    if ([platform isEqualToString:@"iPad6,8"])      return @"iPad Pro (12.9 inch)";
    
    if ([platform isEqualToString:@"iPad6,11"])     return @"iPad (5th generation)";
    if ([platform isEqualToString:@"iPad6,12"])     return @"iPad (5th generation)";
    
    if ([platform isEqualToString:@"iPad7,1"])      return @"iPad Pro (12.9-inch, 2nd generation)";
    if ([platform isEqualToString:@"iPad7,2"])      return @"iPad Pro (12.9-inch, 2nd generation)";
    if ([platform isEqualToString:@"iPad7,3"])      return @"iPad Pro (10.5-inch)";
    if ([platform isEqualToString:@"iPad7,4"])      return @"iPad Pro (10.5-inch)";
    
	if ([platform isEqualToString:@"i386"])         return @"Simulator";
	if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
	return platform;
}


+ (bool)isSimulator
{
	return [[self modelName].lowercaseString containsString:@"simulator"];
}


+ (bool)isPhone
{
    return UIUserInterfaceIdiomPhone == [[UIDevice currentDevice] userInterfaceIdiom];
}

+ (bool)isPad
{
    return UIUserInterfaceIdiomPad == [[UIDevice currentDevice] userInterfaceIdiom];
}


+ (NSString *)systemVersion
{
    return [UIDevice currentDevice].systemVersion;
}


+ (float)systemVersionFloat
{
    return [[self systemVersion] floatValue];
}


+ (NSInteger)majorSystemVersion
{
    NSArray *components = [[self systemVersion] componentsSeparatedByString:@"."];
    NSString *major = components[0];
    return major.integerValue;
}


+ (bool)canMakePhoneCalls
{
    if ([self isSimulator]) {
        return NO; // avoid warning -canOpenURL: failed for URL: "tel://"
    }
    
    if ([self isPad]) {
        return NO; // disable phone call on iPad, see IOS-167
    }
    
    // check 'tel:' URL
    if (NO == [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
        return NO; // device is not capable for making calls
    }
    
    // on iOS 8.0+ iPad can open tel: URL in case of Continuity feature, so we need to check a SIM
    CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
    NSString *mnc = netInfo.subscriberCellularProvider.mobileNetworkCode;
    return (mnc.length > 0);
}

@end
