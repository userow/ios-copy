//
//  UIPickerView+NSDate.m
//  HG-Project
//
//  Created by Sergey Gorokhovich on 5/28/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "UIPickerView+NSDate.h"

@implementation UIPickerView (NSDate)

+ (NSArray *)timePickerValues
{
    static NSArray *values = nil;
    if (!values) {
    values = @[@"",
             @"12:00 AM",
             @"12:30 AM",
             @"01:00 AM",
             @"01:30 AM",
             @"02:00 AM",
             @"02:30 AM",
             @"03:00 AM",
             @"03:30 AM",
             @"04:00 AM",
             @"04:30 AM",
             @"05:00 AM",
             @"05:30 AM",
             @"06:00 AM",
             @"06:30 AM",
             @"07:00 AM",
             @"07:30 AM",
             @"08:00 AM",
             @"08:30 AM",
             @"09:00 AM",
             @"09:30 AM",
             @"10:00 AM",
             @"10:30 AM",
             @"11:00 AM",
             @"11:30 AM",
             @"12:00 PM",
             @"12:30 PM",
             @"01:00 PM",
             @"01:30 PM",
             @"02:00 PM",
             @"02:30 PM",
             @"03:00 PM",
             @"03:30 PM",
             @"04:00 PM",
             @"04:30 PM",
             @"05:00 PM",
             @"05:30 PM",
             @"06:00 PM",
             @"06:30 PM",
             @"07:00 PM",
             @"07:30 PM",
             @"08:00 PM",
             @"08:30 PM",
             @"09:00 PM",
             @"09:30 PM",
             @"10:00 PM",
             @"10:30 PM",
             @"11:00 PM",
             @"11:30 PM"];
    }
    return values;
}

+ (NSArray *)timePickerNumberValues
{
    static NSArray *values = nil;
    if (!values) {
        values = @[@[],
             @[ @0, @0 ],
             @[ @0, @30 ],
             @[ @1, @0 ],
             @[ @1, @30 ],
             @[ @2, @0 ],
             @[ @2, @30 ],
             @[ @3, @0 ],
             @[ @3, @30 ],
             @[ @4, @0 ],
             @[ @4, @30 ],
             @[ @5, @0 ],
             @[ @5, @30 ],
             @[ @6, @0 ],
             @[ @6, @30 ],
             @[ @7, @0 ],
             @[ @7, @30 ],
             @[ @8, @0 ],
             @[ @8, @30 ],
             @[ @9, @0 ],
             @[ @9, @30 ],
             @[ @10, @0 ],
             @[ @10, @30 ],
             @[ @11, @0 ],
             @[ @11, @30 ],
             @[ @12, @0 ],
             @[ @12, @30 ],
             @[ @13, @0 ],
             @[ @13, @30 ],
             @[ @14, @0 ],
             @[ @14, @30 ],
             @[ @15, @0 ],
             @[ @15, @30 ],
             @[ @16, @0 ],
             @[ @16, @30 ],
             @[ @17, @0 ],
             @[ @17, @30 ],
             @[ @18, @0 ],
             @[ @18, @30 ],
             @[ @19, @0 ],
             @[ @19, @30 ],
             @[ @20, @0 ],
             @[ @20, @30 ],
             @[ @21, @0 ],
             @[ @21, @30 ],
             @[ @22, @0 ],
             @[ @22, @30 ],
             @[ @23, @0 ],
             @[ @23, @30 ]
             ];
    }
    return values;
}


- (NSString *)selectedTimePickerValue
{
    NSInteger row = [self selectedRowInComponent:0];
    return [self timePickerValueForRow:row];
}


- (NSUInteger)timePickerValuesCount
{
    return [self.class timePickerValues].count;
}


- (NSString *)timePickerValueForRow:(NSInteger)row
{
    return [self.class timePickerValues][row];
}


- (NSDate *)currentDateValue
{
    NSString *currentDateString = [self.class timePickerValues][[self selectedRowInComponent:0]];

    NSDateFormatter *df = [HGDateFormatterFactory fullTime];
    return ![currentDateString isEqualToString:@""] ? [df dateFromString:currentDateString] : nil;
}


- (HGDate *)currentHGDateValue
{
    HGDate *date = [[HGDate alloc] init];
    NSArray *values = [self.class timePickerNumberValues][[self selectedRowInComponent:0]];
    if (values.count > 0) {
        date.hour = values[0];
        date.minute = values[1];
        return date;
    }
    
    return nil;
}


- (void)selectDefault
{
    [self selectRow:0 inComponent:0 animated:YES];
}


- (void)setHGDate:(HGDate *)date
{
    NSInteger row = 0;
    NSArray *values = [self.class timePickerNumberValues];
    for (NSArray *pair in values) {
        if (pair.count > 0 &&
            [pair[0] integerValue] == date.hour.integerValue &&
            [pair[1] integerValue] == date.minute.integerValue) {
            break;
        }
        row++;
    }
    [self selectRow:row inComponent:0 animated:YES];
}


#pragma mark - Round Date

- (NSDate *)changeTimeValue:(NSDate *)dateValue
{
    const NSCalendarUnit unitFlags = (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal);

    NSDateComponents *comps = [[NSCalendar currentCalendar] components:unitFlags fromDate:dateValue];
    
    if (comps.minute < 30 && comps.minute > 0) {
        NSInteger value = 30 - comps.minute;
        
        if (value < 15) {
            [comps setMinute:30];
        } else {
            [comps setMinute:0];
        }
    } else if (comps.minute > 30) {
        NSInteger value = 60 - comps.minute;
        
        if (value < 15) {
            [comps setHour:comps.hour + 1];
            [comps setMinute:0];
        } else {
            [comps setMinute:30];
        }
    }
    
    // Zero out the seconds.
    [comps setSecond:0];
    // Construct a new date.
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

@end
