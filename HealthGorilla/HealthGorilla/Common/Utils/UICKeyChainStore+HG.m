//
//  UICKeyChainStore+HG.m
//  GenericKeyChainTry
//
//  Created by Mykola on 9/21/14.
//  Copyright (c) 2014 Mykola. All rights reserved.
//

#import "UICKeyChainStore+HG.h"

@implementation UICKeyChainStore (HG)

- (void)registerDefaults:(NSDictionary *)registrationDictionary
{
    for(id key in registrationDictionary)
    {
        id object = [registrationDictionary valueForKey:key];
        
        if ([object isKindOfClass:[NSDate class]])
        {
            if (![self dateForKey:key])
            {
                [self setDate:object forKey:key];
            }
        } else if ([object isKindOfClass:[NSArray class]])
        {
            if (![self arrayForKey:key])
            {
                [self setArray:object forKey:key];
            }
        } else if ([object isKindOfClass:[NSDictionary class]])
        {
            if (![self dictionaryForKey:key])
            {
                [self setDictionary:object forKey:key];
            }
        } else if ([object isKindOfClass:[NSData class]])
        {
            if (![self dataForKey:key])
            {
                [self setData:object forKey:key];
            }
        } else if ([object isKindOfClass:[NSString class]])
        {
            if (![self stringForKey:key])
            {
                [self setString:object forKey:key];
            }
        } else {
            if (![self dataForKey:key])
            {
                NSData *data = [NSData dataWithBytes: &object length: sizeof(object)];
                [self setData:data forKey:key];
            }
        }
    }
}

#pragma mark - getters

- (NSDate *)dateForKey:(NSString *)defaultName
{
    NSData * data = [self dataForKey:defaultName];
    NSDate * date = nil;
    if (data)
    {
        date = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return date;
}

- (NSArray *)arrayForKey:(NSString *)defaultName
{
    NSData * data = [self dataForKey:defaultName];
    NSArray *array = nil;
    if (data)
    {
        array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return array;
}

- (NSDictionary *)dictionaryForKey:(NSString *)defaultName
{
    NSData * data = [self dataForKey:defaultName];
    NSDictionary *dictionary = nil;
    if (data)
    {
        dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return dictionary;
}

- (NSInteger)integerForKey:(NSString *)defaultName
{
    NSData * data = [self dataForKey:defaultName];
    NSNumber *number;
    [data getBytes:&number length:sizeof(number)];
    NSInteger value = [number integerValue];
    return value;
}

- (float)floatForKey:(NSString *)defaultName
{
    NSData * data = [self dataForKey:defaultName];
    NSNumber *number;
    [data getBytes:&number length:sizeof(number)];
    float value = [number floatValue];
    return value;
}
- (double)doubleForKey:(NSString *)defaultName
{
    NSData * data = [self dataForKey:defaultName];
    NSNumber *number;
    [data getBytes:&number length:sizeof(number)];
    double value = [number doubleValue];
    return value;
}

- (BOOL)boolForKey:(NSString *)defaultName
{
    return [[self stringForKey:defaultName] boolValue];
}

#pragma mark - setters

- (void)setDate:(NSDate *)value forKey:(NSString *)defaultName
{
    NSData *data = nil;
    if (value)
    {
        data = [NSKeyedArchiver archivedDataWithRootObject:value];
    }
    [self setData:data forKey:defaultName];
}

- (void)setArray:(NSArray *)value forKey:(NSString *)defaultName
{
    NSData *data = nil;
    if (value)
    {
        data = [NSKeyedArchiver archivedDataWithRootObject:value];
    }
    [self setData:data forKey:defaultName];
}

- (void) setDictionary:(NSDictionary *)value forKey:(NSString *)defaultName
{
    NSData *data = nil;
    if (value)
    {
        data = [NSKeyedArchiver archivedDataWithRootObject:value];
    }
    [self setData:data forKey:defaultName];
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)defaultName
{
    NSNumber *number = @(value);
    NSData *data = [NSData dataWithBytes: &number length: sizeof(number)];
    [self setData:data forKey:defaultName];
}

- (void)setFloat:(float)value forKey:(NSString *)defaultName
{
    NSNumber *number = @(value);
    NSData *data = [NSData dataWithBytes: &number length: sizeof(number)];
    [self setData:data forKey:defaultName];
}

- (void)setDouble:(double)value forKey:(NSString *)defaultName
{
    NSNumber *number = @(value);
    NSData *data = [NSData dataWithBytes: &number length: sizeof(number)];
    [self setData:data forKey:defaultName];
}

- (void)setBool:(BOOL)value forKey:(NSString *)defaultName
{
    NSString *string = [NSString stringWithFormat:@"%d",value];
    [self setString:string forKey:defaultName];
}

@end
