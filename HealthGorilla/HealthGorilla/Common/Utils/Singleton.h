//
//  Singleton.h
//  HG-Project
//
//  Created by not Pavel on 10.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#define SYNTHESIZE_SINGLETON(classname) \
 \
+ (classname *)shared \
{ \
	static dispatch_once_t once; \
	static classname *instance; \
	dispatch_once(&once, ^{ \
		instance = [classname new]; \
	}); \
	return instance; \
}


#define DECLARE_SINGLETON(classname) \
 \
+ (classname *)shared;
