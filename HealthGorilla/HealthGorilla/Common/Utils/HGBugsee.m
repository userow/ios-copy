//
//  HGBugsee.m
//  HG-Project
//
//  Created by not Paul on 19.02.16.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGBugsee.h"


#if USE_BUGSEE
#import <Bugsee/Bugsee.h>
#endif // USE_BUGSEE


@implementation HGBugsee

#if USE_BUGSEE

+ (void)start
{
    NSDictionary *options = @ {
        BugseeShakeToReportKey      : BugseeTrue,
        BugseeScreenshotToReportKey : BugseeTrue,
        BugseeCrashReportKey        : BugseeTrue,
        BugseeMonitorNetworkKey : BugseeTrue,
    };
    [Bugsee launchWithToken:[HGAppConfig kBugseeToken] andOptions:options];
}

#else // USE_BUGSEE

+ (void)start {
    // nop
}

#endif // USE_BUGSEE

@end
