//
//  UIImage+HG.m
//  HG-Project
//
//  Created by not Pavel on 5/30/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "UIImage+HG.h"

@implementation UIImage(HG)

/*
- (UIImage *)scaleToSize:(CGSize)size
{
    // Scalling selected image to targeted size
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, size.width, size.height, 8, 0, colorSpace, kCGBitmapAlphaInfoMask &kCGImageAlphaPremultipliedLast);
    CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
    
    if (self.imageOrientation == UIImageOrientationRight) {
        CGContextRotateCTM(context, -M_PI_2);
        CGContextTranslateCTM(context, -size.height, 0.0f);
        CGContextDrawImage(context, CGRectMake(0, 0, size.height, size.width), self.CGImage);
    } else {
        CGContextDrawImage(context, CGRectMake(0, 0, size.width, size.height), self.CGImage);
    }
    
    CGImageRef scaledImage = CGBitmapContextCreateImage(context);
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    UIImage *image = [UIImage imageWithCGImage: scaledImage];
    CGImageRelease(scaledImage);
    
    return image;
}

- (UIImage *)scaleProportionalToSize:(CGSize)size
{
    if (self.size.width > self.size.height) {
        size = CGSizeMake((self.size.width / self.size.height) * size.height, size.height);
    } else {
        size = CGSizeMake(size.width, (self.size.height / self.size.width) * size.width);
    }
    
    return [self scaleToSize:size];
}
*/

+ (UIImage *)hg_imageFromDataURLString:(NSString *)string
{
    // image data starts with "data:image/png;base64,"
    NSRange range = [string rangeOfString:@";base64,"];
    
    NSData *data = nil;
    if (range.location != NSNotFound) {
        NSString *imageString = [string substringFromIndex:NSMaxRange(range)];
        data = [[NSData alloc] initWithBase64EncodedString:imageString options:NSDataBase64DecodingIgnoreUnknownCharacters];
    } else {
        data = [[NSData alloc] initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
    }
    return [UIImage imageWithData:data];
}


- (NSString *)hg_dataURLString
{
    NSString *prefix = @"data:image/png;base64,";
    NSData *imageData = UIImagePNGRepresentation(self);
    
    return [prefix stringByAppendingString:[imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]];
}


+ (UIImage *)hg_imageRectWithSolidColor:(UIColor *)color size:(CGSize)size
{
    BOOL opaque = (1 == CGColorGetAlpha(color.CGColor));
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    UIGraphicsBeginImageContextWithOptions(size, opaque, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect fillRect = CGRectMake(0, 0, size.width, size.height);
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, fillRect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


+ (UIImage *)hg_imageEllipseWithSolidColor:(UIColor *)color size:(CGSize)size
{
    BOOL opaque = NO;
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    UIGraphicsBeginImageContextWithOptions(size, opaque, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect fillRect = CGRectMake(0, 0, size.width, size.height);
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillEllipseInRect(context, fillRect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


+ (UIImage *)hg_imageEllipseWithSolidColor:(UIColor *)color dotColor:(UIColor *)dotColor dotRaduis:(CGFloat)dotRaduis size:(CGSize)size
{
    BOOL opaque = NO;
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    UIGraphicsBeginImageContextWithOptions(size, opaque, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect fillRect = CGRectMake(0, 0, size.width, size.height);
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillEllipseInRect(context, fillRect);
    
    const CGFloat cx = size.width/2;
    const CGFloat cy = size.height/2;
    
    CGRect dotRect = CGRectMake(cx-dotRaduis, cy-dotRaduis, dotRaduis*2, dotRaduis*2);
    CGContextSetFillColorWithColor(context, dotColor.CGColor);
    CGContextFillEllipseInRect(context, dotRect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


- (void)hg_dumpWithFileName:(NSString *)fileName
{
    NSData *data = UIImagePNGRepresentation(self);
    NSString *directory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *path = [directory stringByAppendingPathComponent:fileName];
    [data writeToFile:path atomically:YES];
}

@end
