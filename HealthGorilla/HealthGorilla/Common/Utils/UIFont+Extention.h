//
//  UIFont+Extention.h
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@interface UIFont (Extention)

+ (UIFont *)mediumSystemFontOfSize:(CGFloat)fontSize;
+ (UIFont *)lightSystemFontOfSize:(CGFloat)fontSize;
+ (UIFont *)thinSystemFontOfSize:(CGFloat)fontSize;

+ (UIFont *)courierFontOfSize:(CGFloat)fontSize;

@end
