//
//  HGIdologyDocType.h
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

@interface HGIdologyDocType : NSObject

+ (NSArray *)allDocTypesIds;
+ (NSArray *)allDocTypesDescriptions;
+ (NSString *)defaultDocType;

+ (bool)isDriverLicense:(NSString *)driverLicense;
+ (bool)isIdCard:(NSString *)idCard;
+ (bool)isPassport:(NSString *)passport;

+ (NSString *)driverLicense;
+ (NSString *)idCard;
+ (NSString *)passport;

@end
