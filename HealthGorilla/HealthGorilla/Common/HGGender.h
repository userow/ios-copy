//
//  HGGender.h
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@interface HGGender : NSObject

+ (NSArray *)allGenders;
+ (NSArray *)allGendersLocalized;
+ (NSString *)defaultGender;
+ (bool)isMale:(NSString *)gender;
+ (bool)isFemale:(NSString *)gender;
+ (NSString *)male;
+ (NSString *)female;

@end
