//
//  HGCustomSegmentedControl.swift
//  Health Gorilla
//
//  Created by Pavel Wasilenko on 18-09-02.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

import UIKit

class HGCustomSegmentedControl: UISegmentedControl {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func draw(_ rect: CGRect) {
        
        for segment in subviews {
            for subview in segment.subviews {
                if let segmentLabel = subview as? UILabel {
                    segmentLabel.numberOfLines = 0
                    segmentLabel.adjustsFontSizeToFitWidth = true
                    segmentLabel.minimumScaleFactor = 0.5
                    segmentLabel.translatesAutoresizingMaskIntoConstraints = false
                    segmentLabel.leadingAnchor.constraint(equalTo: segment.leadingAnchor).isActive = true
                    segmentLabel.trailingAnchor.constraint(equalTo: segment.trailingAnchor).isActive = true
                    segmentLabel.topAnchor.constraint(equalTo: segment.topAnchor).isActive = true
                    segmentLabel.bottomAnchor.constraint(equalTo: segment.bottomAnchor).isActive = true
                }
            }
        }
    }
    
    override func didMoveToSuperview() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.backgroundColor = .white
//        self.tintColor = UIColor.orange
        
        let font = UIFont(name: "Helvetica", size: 12.0)
        self.setTitleTextAttributes([NSAttributedStringKey.font: font!], for: .normal)
        
        let fontBold = UIFont(name: "Helvetica-Bold", size: 12.0)
        self.setTitleTextAttributes([NSAttributedStringKey.font: fontBold!], for: .selected)
    }
}
