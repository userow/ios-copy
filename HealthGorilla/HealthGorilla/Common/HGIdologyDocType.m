//
//  HGIdologyDocType.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGIdologyDocType.h"

@implementation HGIdologyDocType

+ (NSArray *)allDocTypesIds
{
    //driverLicense - Driver's License, idCard - ID Card, passport - Passport]
    return @[ @"driverLicense", @"idCard", @"passport"];
}

+ (NSArray *)allDocTypesDescriptions
{
    return @[ @"Driver's License".localized, @"ID Card".localized, @"Passport".localized ];
}

+ (NSString *)defaultDocType
{
    return @"driverLicense";
}



+ (bool)isDriverLicense:(NSString *)word;
{
    return [word.lowercaseString isEqualToString:@"driverLicense".lowercaseString] ||
    [word.lowercaseString isEqualToString:@"Driver's License".localized.lowercaseString];
}

+ (bool)isIdCard:(NSString *)word;
{
    return [word.lowercaseString isEqualToString:@"idCard".lowercaseString] ||
    [word.lowercaseString isEqualToString:@"ID Card".localized.lowercaseString];
}

+ (bool)isPassport:(NSString *)word;
{
    return [word.lowercaseString isEqualToString:@"passport".lowercaseString] ||
    [word.lowercaseString isEqualToString:@"Passport".localized.lowercaseString];
}


+ (NSString *)driverLicense;
{
    return @"driverLicense";
}

+ (NSString *)idCard;
{
    return @"idCard";
}

+ (NSString *)passport;
{
    return @"passport";
}

@end
