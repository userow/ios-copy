//
//  HGFileDownloader.m
//  HG-Project
//
//  Created by not Pavel on 6/24/14.
//  Refactored by not Pavel on July 10, 2015
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "HGFileDownloader.h"
#import "HGAttachment.h"
#import "HGImageCache.h"


@interface HGFileDownloader ()

@end


@implementation HGFileDownloader

- (instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    self = [super initWithBaseURL:url sessionConfiguration:configuration];
    if (!self) return nil;
    
    AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = nil; // accept everything
    self.responseSerializer = responseSerializer;
    
    return self;
}


- (NSURLSessionDataTask *)downloadWithUrl:(NSString *)url
                                 progress:(void (^)(NSProgress *))progress
                              resultBlock:(void (^)(NSURLSessionDataTask *task, id responseObject, NSError *error))resultBlock
{
    return [self GET:url parameters:nil progress:progress success:^(NSURLSessionDataTask *task, id responseObject) {
        
        resultBlock(task, responseObject, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        resultBlock(task, nil, error);
    }];
}


- (NSURLSessionDataTask *)downloadAttachment:(HGAttachment *)attachment
                                 resultBlock:(void (^)(NSData *data, NSString *textEncodingName, NSString *mimeType, NSError *error))resultBlock
{
    NSString *urlFormat = [self.baseURL.absoluteString stringByAppendingString:@"/download?fileId=%@&fileName=%@&type=%@"];
    NSString *url = [NSString stringWithFormat:urlFormat,
                     attachment.objectId,
                     attachment.name.urlQueryPercentEncoded,
                     attachment.contentType];
    
    return [self downloadWithUrl:url progress:nil resultBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        
        NSData *data = responseObject;
        NSString *textEncodingName = task.response.textEncodingName;
        NSString *mimeType = task.response.MIMEType;
        
        resultBlock(data, textEncodingName, mimeType, error);
    }];
}


- (NSURLSessionDataTask *)downloadPreview:(HGAttachment *)attachment
                              resultBlock:(void (^)(UIImage *icon, NSError *error))resultBlock
{
    NSString *key = attachment.objectId;
    
    HGImageCache *imageCache = [HGImageCache attachments];
    UIImage *cachedImage = [imageCache imageForKey:key];
    if (cachedImage) {
        resultBlock(cachedImage, nil);
        return nil;
    }
    
    NSString *urlFormat = [self.baseURL.absoluteString stringByAppendingString:@"/download?fileId=%@&fileName=%@&type=%@"];
    NSString *url = [NSString stringWithFormat:urlFormat,
                     attachment.objectId,
                     attachment.name.urlQueryPercentEncoded,
                     attachment.contentType];
    
    return [self downloadWithUrl:url progress:nil resultBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        
        if (!error) {
            NSData *data = responseObject;
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                [imageCache storeImage:image forKey:key];
            }
            resultBlock(image, nil);
        } else {
            resultBlock(nil, error);
        }
    }];
}


- (NSURLSessionDataTask *)downloadFacilityIcon:(NSString *)facilityId
                                   resultBlock:(void (^)(UIImage *icon, NSError *error))resultBlock
{
    NSString *key = facilityId;
    
    HGImageCache *imageCache = [HGImageCache facilityIcons];
    UIImage *cachedImage = [imageCache imageForKey:key];
    if (cachedImage) {
        resultBlock(cachedImage, nil);
        return nil;
    }
    
    NSString *url = [self.baseURL.absoluteString stringByAppendingString:@"/icon?type=facility&id=%@"];
    url = [NSString stringWithFormat:url, facilityId];
    
    return [self downloadWithUrl:url progress:nil resultBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        
        if (!error) {
            NSData *data = responseObject;
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                [imageCache storeImage:image forKey:key];
            }
            resultBlock(image, nil);
        } else {
            resultBlock(nil, error);
        }
    }];
}


- (NSURLSessionDataTask *)downloadPhotoWithRelativeUrl:(NSString *)relativeUrl
                                           resultBlock:(void (^)(UIImage *image, NSError *error))resultBlock
{
    NSString *key = relativeUrl;
    NSString *url = [self.baseURL.absoluteString stringByAppendingPathComponent:relativeUrl];
    
    return [self downloadPhotoWithUrl:url key:key resultBlock:resultBlock];
}


- (NSURLSessionDataTask *)downloadPhotoWithAbsoluteUrl:(NSString *)absoluteUrl
                                           resultBlock:(void (^)(UIImage *image, NSError *error))resultBlock
{
    NSString *key = absoluteUrl;
    NSString *url = absoluteUrl;
    return [self downloadPhotoWithUrl:url key:key resultBlock:resultBlock];
}


- (NSURLSessionDataTask *)downloadPhotoWithUrl:(NSString *)url
                                           key:(NSString *)key
                                           resultBlock:(void (^)(UIImage *image, NSError *error))resultBlock
{
    HGImageCache *imageCache = [HGImageCache photos];
    UIImage *cachedImage = [imageCache imageForKey:key];
    if (cachedImage) {
        resultBlock(cachedImage, nil);
        return nil;
    }
    
    return [self downloadWithUrl:url progress:nil resultBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        
        if (!error) {
            NSData *data = responseObject;
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                [imageCache storeImage:image forKey:key];
            }
            resultBlock(image, nil);
        } else {
            resultBlock(nil, error);
        }
    }];
}


- (NSURLSessionDataTask *)downloadFileWithRelativeUrl:(NSString *)relativeUrl
                                          resultBlock:(void (^)(NSData *responseData, NSError *error))resultBlock
{
    NSString *url = [self.baseURL.absoluteString stringByAppendingPathComponent:relativeUrl];
    
    return [self downloadWithUrl:url progress:nil resultBlock:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
        
        if (!error) {
            resultBlock(responseObject, nil);
        } else {
            resultBlock(nil, error);
        }
    }];
}

@end
