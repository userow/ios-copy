//
//  HGUserSession.m
//  HG-Project
//
//  Created by not Paul on 19.08.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGUserSession.h"
#import "HGUserStorage.h"


static const NSInteger HGDefaultSessionTimeout = 300;
static const NSInteger HGUnknownSessionTimeout = -1;



@interface HGUserSession ()

DECLARE_SINGLETON(HGUserSession)

@property (nonatomic) BOOL loggedIn;
@property (nonatomic) NSInteger timeoutSec;

@end


@implementation HGUserSession

SYNTHESIZE_SINGLETON(HGUserSession);

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _loggedIn = NO;
    _timeoutSec = HGUnknownSessionTimeout;
    
    return self;
}


- (NSInteger)timeoutSec
{
    switch (_timeoutSec) {
        case 0:
        case HGUnknownSessionTimeout:
            return HGDefaultSessionTimeout;
        default:
            return _timeoutSec;
    }
}


+ (void)setLoggedInWithTimeout:(NSInteger)timeout
{
    [self shared].loggedIn = YES;
    [self shared].timeoutSec = timeout;
}


+ (void)setLoggedOut
{
    [self shared].loggedIn = NO;
    [self shared].timeoutSec = HGUnknownSessionTimeout;
}


+ (bool)isLoggedIn
{
    return [self shared].loggedIn;
}


+ (NSInteger)timeoutSec
{
    return [self shared].timeoutSec;
}


+ (bool)isExpired
{
    NSDate *date = [HGUserStorage lastSessionTime];
    if (date) {
        NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:date];
        return (interval > [self shared].timeoutSec);
    }
    return NO;
}


+ (void)saveLastSessionTime
{
    [HGUserStorage saveLastSessionTime];
}

@end
