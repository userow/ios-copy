//
//  HGApiClient.h
//  HG-Project
//
//  Created by not Paul on 10.07.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//
#import "HGInteractionID.h"
#import "HGSendDocumentResponse.h"
#import "HGIdologyImageFieldItem.h"

extern NSString * const HGErrorDomain;

@class HGAttachment;
@class HGExternalAttachment;
@class HGRecipient;
@class HGCareContact;
@class HGPatient;
@class HGRecipientInfo;

typedef void (^HGApiClientCompletion)(NSDictionary *response, NSError *error);
typedef void (^HGApiArrayCompletion)(NSArray *response, NSError *error);


@interface HGApiClient : NSObject

DECLARE_SINGLETON(HGApiClient);

+ (NSURLResponse *)recentResponse;

#pragma mark - low-level POST request

- (NSURLSessionDataTask *)POST:(NSString *)method
                    parameters:(id)parameters
                   resultBlock:(HGApiClientCompletion)resultBlock;

#pragma mark - API methods with JSON response

- (void)setPin:(NSString *)pin
      username:(NSString *)username
      password:(NSString *)password
   resultBlock:(HGApiClientCompletion)resultBlock;

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
              resultBlock:(HGApiClientCompletion)resultBlock;

- (void)loginWithUsername:(NSString *)username
                      pin:(NSString *)pin
              resultBlock:(HGApiClientCompletion)resultBlock;

- (void)logout:(HGApiClientCompletion)resultBlock;

- (void)sendPushToken:(NSString *)token resultBlock:(HGApiClientCompletion)resultBlock;

#pragma mark - VPV methods

- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock;

- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                         type:(HGIdologyImageType)type
                                pendingUserId:(NSString *)userId
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock;

- (void)removeImageWithExternalId:(NSString *)externalId
                    pendingUserId:(NSString *)userId
                        imageType:(HGIdologyImageType)type;

@end
