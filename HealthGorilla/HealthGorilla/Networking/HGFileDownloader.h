//
//  HGFileDownloader.h
//  HG-Project
//
//  Created by not Pavel on 6/24/14.
//  Refactored by not Pavel on July 10, 2015
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@class HGAttachment;

@interface HGFileDownloader : AFHTTPSessionManager

- (NSURLSessionDataTask *)downloadAttachment:(HGAttachment *)attachment
                                 resultBlock:(void (^)(NSData *data, NSString *textEncodingName, NSString *mimeType, NSError *error))resultBlock;

- (NSURLSessionDataTask *)downloadPreview:(HGAttachment *)attachment
                              resultBlock:(void (^)(UIImage *icon, NSError *error))resultBlock;

- (NSURLSessionDataTask *)downloadFacilityIcon:(NSString *)facilityId
                                   resultBlock:(void (^)(UIImage *icon, NSError *error))resultBlock;

- (NSURLSessionDataTask *)downloadPhotoWithRelativeUrl:(NSString *)relativeUrl
                                           resultBlock:(void (^)(UIImage *image, NSError *error))resultBlock;

- (NSURLSessionDataTask *)downloadPhotoWithAbsoluteUrl:(NSString *)absoluteUrl
                                           resultBlock:(void (^)(UIImage *image, NSError *error))resultBlock;

- (NSURLSessionDataTask *)downloadFileWithRelativeUrl:(NSString *)relativeUrl
                                          resultBlock:(void (^)(NSData *responseData, NSError *error))resultBlock;

@end
