//
//  HGUserSession.h
//  HG-Project
//
//  Created by not Paul on 19.08.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

@interface HGUserSession : NSObject

+ (void)setLoggedInWithTimeout:(NSInteger)timeout;
+ (void)setLoggedOut;
+ (bool)isLoggedIn;

+ (NSInteger)timeoutSec;
+ (void)saveLastSessionTime;
+ (bool)isExpired;

@end
