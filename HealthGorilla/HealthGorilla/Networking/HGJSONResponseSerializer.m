//
//  HGJSONResponseSerializer.m
//  HG-Project
//
//  Created by not Pavel on 3/17/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGJSONResponseSerializer.h"

@implementation HGJSONResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
#if USE_RAW_JSON_LOGGING && defined(HG_QA)
    {
        NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"");
        NSLog(@"%@", json);
        NSLog(@"");
    }
#endif
    
    id JSONObject = [super responseObjectForResponse:response data:data error:error];
    
    if (response && [response isKindOfClass:[NSHTTPURLResponse class]]) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        
        // This case was created in order to handle 401 in one place. 401 means we need to login again
        // or something happened with session
        if ([httpResponse statusCode] == 401) {
            // Not Auth.
        } else if ([httpResponse statusCode] == 200) {
            if (error) {
                (*error) = nil;
            }
        }
    }
    
    return JSONObject;
}


@end
