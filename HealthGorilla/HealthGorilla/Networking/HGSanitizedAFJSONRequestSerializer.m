//
//  HGSanitizedAFJSONRequestSerializer.m
//  Health Gorilla
//
//  Created by Pavel Wasilenko on 18-09-07.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGSanitizedAFJSONRequestSerializer.h"
#import <AFNetworking/AFNetworking.h>

@implementation HGSanitizedAFJSONRequestSerializer


- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request withParameters:(id)parameters error:(NSError *__autoreleasing  _Nullable *)error {
    NSURLRequest *innerRequest = [super requestBySerializingRequest:request withParameters:parameters error:error];
    
    if (innerRequest.HTTPBody) {
        NSString *jsonString = [[NSString alloc] initWithData:innerRequest.HTTPBody encoding:NSUTF8StringEncoding];
        
        if (jsonString.hasValue) {
            NSString *sanitizedString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
            
            NSLog(@"\n\n\n !!!\n\n\n Sanitized String: \n\n%@", sanitizedString);
            
            NSMutableURLRequest *mutableRequest = [innerRequest mutableCopy];
            
            mutableRequest.HTTPBody = [sanitizedString dataUsingEncoding:NSUTF8StringEncoding];
            
            request = mutableRequest;
        }
    }
    
    return request;
}


//class HGSanitizedAFJSONRequestSerializer: AFJSONRequestSerializer
//{
//    override func requestBySerializingRequest(request: NSURLRequest!, withParameters parameters: AnyObject!, error: NSErrorPointer) -> NSURLRequest!
//    {
//        var request = super.requestBySerializingRequest(request, withParameters: parameters, error: error)
//
//        if let jsonData = request.HTTPBody
//        {
//            if let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as? String
//            {
//                let sanitizedString = jsonString.stringByReplacingOccurrencesOfString("\\/", withString: "/", options: NSStringCompareOptions.CaseInsensitiveSearch, range:nil) as NSString
//
//                print("sanitized json string: \(sanitizedString)")
//
//                var mutableRequest = request.mutableCopy() as! NSMutableURLRequest
//                mutableRequest.HTTPBody = sanitizedString.dataUsingEncoding(NSUTF8StringEncoding)
//                request = mutableRequest
//            }
//        }
//
//        return request
//    }
//}

@end


/*
 */
