//
//  HGHttpSession.h
//  HG-Project
//
//  Created by not Pavel on 6/10/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface HGHttpSession : AFHTTPSessionManager

@property (nonatomic, readonly) NSURLResponse *recentResponse;

- (void)resetMethodCounters;
- (void)resetToken;

- (NSURLSessionDataTask *)POST:(NSString *)method
                    parameters:(id)parameters
                   resultBlock:(void (^)(NSDictionary *responseResult, NSError *error))resultBlock;

@end
