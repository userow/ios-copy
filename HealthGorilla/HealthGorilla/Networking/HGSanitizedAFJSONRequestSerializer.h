//
//  HGSanitizedAFJSONRequestSerializer.h
//  Health Gorilla
//
//  Created by Pavel Wasilenko on 18-09-07.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface HGSanitizedAFJSONRequestSerializer : AFJSONRequestSerializer

@end
