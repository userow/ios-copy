//
//  HGHttpSession.m
//  HG-Project
//
//  Created by not Pavel on 6/10/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "HGHttpSession.h"
#import "HGJSONResponseSerializer.h"
#import "HGApplication.h"
#import "NSBundle+HG.h"
#import "HGSanitizedAFJSONRequestSerializer.h"

extern NSString * const HGErrorDomain;


@interface HGHttpSession ()

@property (nonatomic, strong) NSString *xsrfKey;
@property (nonatomic, strong) NSMutableDictionary *methodIds;
@property (nonatomic, strong) NSURLResponse *recentResponse;

@end


@implementation HGHttpSession

- (instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    self = [super initWithBaseURL:url sessionConfiguration:configuration];
    if (!self) return nil;
    
    HGJSONResponseSerializer *responseSerializer = [HGJSONResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json-rpc"];
    self.responseSerializer = responseSerializer;
    
    HGSanitizedAFJSONRequestSerializer *requestSerializer = [HGSanitizedAFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json-rpc;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json-rpc" forHTTPHeaderField:@"Accept"];
    self.requestSerializer = requestSerializer;
    
    _methodIds = [NSMutableDictionary dictionary];
    
    return self;
}


- (void)resetToken
{
    _xsrfKey = nil;
}


- (NSString *)methodIdForMethodName:(NSString *)name
{
    if (_methodIds[name]) {
        _methodIds[name] = @(1 + [_methodIds[name] integerValue]);
    } else {
        (_methodIds)[name] = @(1);
    }
    return [_methodIds[name] stringValue];
}


- (void)resetMethodCounters
{
    [_methodIds removeAllObjects];
}


- (NSURLSessionDataTask *)POST:(NSString *)method
                    parameters:(id)parameters
                   resultBlock:(void (^)(NSDictionary *responseResult, NSError *error))resultBlock
{
    NSArray *params = parameters ? @[parameters] : @[];
    
    NSMutableDictionary *__parameters = [[NSMutableDictionary alloc] initWithDictionary:@ {
        @"jsonrpc" : @"2.0",
        @"method" : method,
        @"params" : params,
        @"id" : [self methodIdForMethodName:method],
    }];
    if (self.xsrfKey) {
        [__parameters addEntriesFromDictionary:@ {
           @"xsrfKey": self.xsrfKey,
        }];
    }
    
    NSString *path = [NSString stringWithFormat:@"mobileservice?app_version=%@", [HGAppConfig portalAppVersion]];
    
    return [self POST:path parameters:__parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        self.recentResponse = task.response;
        
        NSDictionary *__result = [((NSDictionary *)responseObject)[HGResultParam] isKindOfClass:[NSDictionary class]] ? ((NSDictionary *)responseObject)[HGResultParam] : nil;
        NSDictionary *__error = ((NSDictionary *)responseObject)[HGErrorParam];

        NSInteger errorCode = 0;
        if (__error && __error[HGErrorCode]) {
            errorCode = [__error[HGErrorCode] integerValue];
        }
        
        NSString *errorMessage = @"Unknown error"; //TODO: Localize ?
        if (__error[HGErrorMessage]) {
            errorMessage = __error[HGErrorMessage];
        }
        else if (__error[HGMessageField]) {
            errorMessage = __error[HGMessageField];
        }
        else if (__result[HGErrorMessage]) {
            errorMessage = __result[HGErrorMessage];
        }
        // somethimes server returns message = "<null>"
        if ([errorMessage isKindOfClass:[NSNull class]]) {
            NSString *exeptionName = [__error valueForKeyPath:@"data.exceptionTypeName"];
          errorMessage = exeptionName ?: @"Unknown error"; //TODO: Localize ?
        }
        
        // unexpected server-side exception
        if (__result && __result[HGExceptionParam]) {
            resultBlock(nil, [NSError errorWithDomain:HGErrorDomain
                                                 code:-999
                                             userInfo:@ {
                                             NSLocalizedDescriptionKey:errorMessage,
                                             }]);
            return;
        }

        // 'result' field contains only raw data, not an dict, for example id of task
        if (!__result && !__error && responseObject && ((NSDictionary *)responseObject)[HGResultParam]) {
            resultBlock(((NSDictionary *)responseObject)[HGResultParam], nil);
            return;
        }
        
        static NSString const * xsrfTokenValue = @"xsrfTokenValue";
        if (__result && [__result isKindOfClass:[NSDictionary class]] && __result[xsrfTokenValue]) {
            self.xsrfKey = __result[xsrfTokenValue];
        }
        
        if (__result) {
            if ([method isEqualToString:HGMethod.login] && ![(NSNumber *)__result[HGSuccessfulParam] boolValue]) {
                resultBlock(__result, [NSError errorWithDomain:HGErrorDomain
                                                          code:-1
                                                      userInfo:@ {
                                                          NSLocalizedDescriptionKey: errorMessage
                                                      }]);
            } else {
                resultBlock(__result, nil);
            }
        } else if (__error && __result && __result[xsrfTokenValue]) {
            // Re-try, if servers sent new xsrfKey
            [self POST:method parameters:parameters resultBlock:resultBlock];
        } else if (errorCode == 403 || errorCode == 101) {

            [HGApplication postTimeoutNotification]; // IOS-272
            return;
        }
        else if (errorCode == 102) { // IOS-195
            
            PSPDFAlertView *alert = [HGAlertView alertViewWithTitle:[NSBundle displayName]
                                                         andMessage:[NSString stringWithFormat:@"The version of %@ application you are using is no longer supported. Please update to the latest version.".localized, [HGAppConfig appNameWhitelabeled]]];
            [alert addButtonWithTitle:@"Update".localized block:^(NSInteger buttonIndex) {
                NSURL *url = [NSURL URLWithString:[HGAppConfig HGAppStoreURL]]; //done: add spanish app URL
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }];
            [alert show];
            resultBlock(nil, [NSError errorWithDomain:HGErrorDomain
                                                 code:errorCode
                                             userInfo:@ {
                                                 NSLocalizedDescriptionKey: errorMessage,
                                             }]);
            
        } else if (!__result && !responseObject) {
          if (__error) { //TODO: check if the translation needed- added to .txt
                NSString *info = [NSString stringWithFormat:@"Failed %@ with params: %@ and error: %@", method, params, __error];
                [HGInstabug reportIssue:@"API" info:info];
                resultBlock(__result, [NSError errorWithDomain:HGErrorDomain
                                                          code:errorCode
                                                      userInfo:@ {
                                                          NSLocalizedDescriptionKey: errorMessage,
                                                      }]);
            } else {
                NSString *info = [NSString stringWithFormat:@"Failed %@ with params: %@ and error: %@", method, params, __error];
                [HGInstabug reportIssue:@"API" info:info];
                resultBlock(nil, [NSError errorWithDomain:HGErrorDomain
                                                     code:-999
                                                 userInfo:@ {
                                                     NSLocalizedDescriptionKey: @"Internal server error", //TODO: Localize ? - added to .txt
                                                 }]);
            }
        } else {
            NSString *info = [NSString stringWithFormat:@"Failed %@ with params: %@ and error: %@", method, params, __error];
            [HGInstabug reportIssue:@"API" info:info];
            resultBlock(__result, [NSError errorWithDomain:HGErrorDomain
                                                      code:errorCode
                                                  userInfo:@ {
                                                      NSLocalizedDescriptionKey: errorMessage,
                                                  }]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        // Omit reporting Cancelled, since we're cancel request often
        if (error.code != NSURLErrorCancelled) {
            [HGInstabug reportIssue:@"API"
                               info:[NSString stringWithFormat:@"Failed %@ with params: %@ and error: %@", method, params, error]];
        }
        resultBlock(nil, error);
    }];
}

@end
