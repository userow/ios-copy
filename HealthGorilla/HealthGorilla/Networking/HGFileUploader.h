//
//  HGFileUploader.h
//  HG-Project
//
//  Created by not Paul on 15.03.16.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGIdologyImageFieldItem.h"

@class HGExternalAttachment;

@interface HGFileUploader : AFHTTPSessionManager

- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock;


- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                         type:(HGIdologyImageType)type
                                pendingUserId:(NSString *)name
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock;


@end
