//
//  HGFileUploader.m
//  HG-Project
//
//  Created by not Paul on 15.03.16.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGFileUploader.h"
#import "HGJSONResponseSerializer.h"
#import "HGExternalAttachment.h"
#import "NSData+HGImageContentType.h"


@interface HGFileUploader ()

@end


@implementation HGFileUploader

- (instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    self = [super initWithBaseURL:url sessionConfiguration:configuration];
    if (!self) return nil;
    
    HGJSONResponseSerializer *responseSerializer = [HGJSONResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    self.responseSerializer = responseSerializer;
    
    return self;
}

- (NSURLSessionDataTask *)uploadData:(NSData *)data
                                name:(NSString *)name
                            fileName:(NSString *)fileName
                            mimeType:(NSString *)mimeType
                                type:(HGIdologyImageType)type
                       pendingUserId:(NSString *)userId
                            progress:(void (^)(NSProgress *))progress
                         resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock
{
    //<input type="hidden" name="jquery-file-upload" value="true">
    //<input type="hidden" name="jquery-file-upload-process-id" value="signup_idscan_front_">
    
    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    
    if (type != HGIdologyImageTypeUnknown) {
        parameters[@"jquery-file-upload" ] = @"true";
        
        if (type == HGIdologyImageTypeBack) {
            parameters[@"jquery-file-upload-process-id"] = [@"signup_idscan_back_" stringByAppendingString:userId];
        } else if (type == HGIdologyImageTypeFront) {
            parameters[@"jquery-file-upload-process-id"] = [@"signup_idscan_front_" stringByAppendingString:userId];
        }
    }
    
    NSString *url = [self.baseURL.absoluteString stringByAppendingPathComponent:@"upload"];
    return [self POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:data
                                    name:name
                                fileName:fileName
                                mimeType:mimeType];
        
    } progress:progress success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSError *error = nil;
        HGExternalAttachment *attachment = nil;
        
        if (type == HGIdologyImageTypeUnknown) {
            attachment = [[HGExternalAttachment alloc] initWithDictionary:responseObject error:&error];
        } else {
            if ([responseObject isKindOfClass:[NSDictionary class]] &&
                [[(NSDictionary *)responseObject objectForKey:@"files"] isKindOfClass:[NSArray class]]) {
//                NSDictionary *resp = (NSDictionary *) responseObject;
                attachment = [[HGExternalAttachment arrayOfModelsFromDictionaries:[(NSDictionary *)responseObject objectForKey:@"files"] error:&error] firstObject];
            }
        }
        
        //TODO: change implementation for type != unknown
        if (!attachment) {
            resultBlock(nil, error);
            return;
        }

        resultBlock(attachment, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        resultBlock(nil, error);
    }];
}


- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                         type:(HGIdologyImageType)type
                                pendingUserId:(NSString *)userId
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock
{
    NSString *mimeType = [NSData hg_contentTypeForImageData:data];
    NSString *extention = [mimeType componentsSeparatedByString:@"/"].lastObject;
    NSString *fileName = [NSString stringWithFormat:@"%@.%@", name, extention];
    
    return [self uploadData:data
                       name:@"file"
                   fileName:fileName
                   mimeType:mimeType
                       type:type
              pendingUserId:userId
                   progress:nil
                resultBlock:resultBlock];
}


- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock {
    return [self uploadImageWithData:data
                                name:name
                                type:HGIdologyImageTypeUnknown
                       pendingUserId:nil
                         resultBlock:resultBlock];
}

@end
