//
//  HGApiClient.m
//  HG-Project
//
//  Created by not Paul on 10.07.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGApiClient.h"
#import "HGHttpSession.h"
#import "HGFileDownloader.h"
#import "HGFileUploader.h"
#import "HGUserSession.h"
#import "HGRecipient.h"
#import "HGCareContact.h"
#import "HGExternalAttachment.h"
#import "HGRecipientInfo.h"
#import "HGAttachment.h"


NSString * const HGErrorDomain = @"HGUserErrorDomain";


@interface HGApiClient ()

@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) HGHttpSession *httpSession;
@property (nonatomic, strong) HGFileDownloader *fileDownloader;
@property (nonatomic, strong) HGFileUploader *fileUploader;
@property (nonatomic, strong) NSString *cookie;

@end


@implementation HGApiClient

SYNTHESIZE_SINGLETON(HGApiClient);

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
   
    // limit SDImageCache cache (not supported since SDWebImage 4.0)
    // [SDImageCache sharedImageCache].maxCacheSize = 10 * 1024 * 1024;
    
    // zero-size URL cache to meet HIPAA Compliance
    NSUInteger memoryCapacity = 0;
    NSUInteger diskCapacity = 0;
    NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:memoryCapacity diskCapacity:diskCapacity diskPath:nil];
    [NSURLCache setSharedURLCache:cache];
    
    // setup session configuration w/o caching
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPShouldSetCookies = YES;
    configuration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    configuration.URLCache = nil; // no caching
    
    _baseURL = [NSURL URLWithString:HGPortalURL];
    _httpSession = [[HGHttpSession alloc] initWithBaseURL:_baseURL sessionConfiguration:configuration];
    _fileDownloader = [[HGFileDownloader alloc] initWithBaseURL:_baseURL sessionConfiguration:configuration];
    _fileUploader = [[HGFileUploader alloc] initWithBaseURL:_baseURL sessionConfiguration:configuration];
    
    _cookie = @"";

    return self;
}


- (NSURLSessionDataTask *)POST:(NSString *)method
                    parameters:(id)parameters
                   resultBlock:(HGApiClientCompletion)resultBlock
{
    return [_httpSession POST:method parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        
        if (!error) {
            [self updateCoockie];
        }
        if (resultBlock) {
            resultBlock(response, error);
        }
    }];
}


- (void)updateCoockie
{
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:self.baseURL];
    if (cookies) {
        for (NSHTTPCookie *cockie in cookies) {
            self.cookie = [self.cookie stringByAppendingString:[NSString stringWithFormat:@"%@=%@;", cockie.name, cockie.value]];
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cockie];
        }
        [_httpSession.requestSerializer setValue:self.cookie forHTTPHeaderField:@"Cookie"];
        [_fileDownloader.requestSerializer setValue:self.cookie forHTTPHeaderField:@"Cookie"];
        [_fileUploader.requestSerializer setValue:self.cookie forHTTPHeaderField:@"Cookie"];
    }
}


+ (NSURLResponse *)recentResponse
{
    return [self shared].httpSession.recentResponse;
}


#pragma mark - API methods with JSON response

- (void)authWithMethod:(NSString *)method
            parameters:(NSDictionary *)parameters
           resultBlock:(HGApiClientCompletion)resultBlock
{
    NSMutableDictionary *moreParameters = [parameters mutableCopy];
    [moreParameters addEntriesFromDictionary:@ {
        HGAppIdParam : HGPortalAppID,
        HGDeviceUidParam : [HGUser deviceUid],
        HGTimezoneParam : [NSTimeZone localTimeZone].name,
    }];
    
    [self POST:method parameters:moreParameters resultBlock:^(NSDictionary *response, NSError *error) {
        
        if (!error) {
            
            // mobileMaxInactiveIntervalSec is user activity timeout (taps)
            // sessionMaxInactiveIntervalSec is network token timeout (xsrfKey)
            NSNumber *sessionTimeout = response[@"mobileMaxInactiveIntervalSec"];
            [HGUserSession setLoggedInWithTimeout:sessionTimeout.integerValue];
            
            [self.httpSession resetMethodCounters];
        }
        
        resultBlock(response, error);
    }];
}


- (void)setPin:(NSString *)pin
      username:(NSString *)username
      password:(NSString *)password
   resultBlock:(HGApiClientCompletion)resultBlock
{
    NSDictionary *parameters = @ {
        HGUserNameParam : username,
        HGPasswordParam : password,
        HGPinParam : pin,
    };
    [self authWithMethod:HGMethod.setDevicePin parameters:parameters resultBlock:resultBlock];
}


- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
              resultBlock:(HGApiClientCompletion)resultBlock
{
    [self.httpSession resetToken];
    
    NSDictionary *parameters = @ {
        HGUserNameParam : username,
        HGPasswordParam : password,
    };
    [self authWithMethod:HGMethod.login parameters:parameters resultBlock:resultBlock];
}


- (void)loginWithUsername:(NSString *)username
                      pin:(NSString *)pin
              resultBlock:(HGApiClientCompletion)resultBlock
{
    [self.httpSession resetToken];
    
    NSDictionary *parameters = @ {
        HGUserNameParam : username,
        HGPinParam : pin,
    };
    [self authWithMethod:HGMethod.login parameters:parameters resultBlock:resultBlock];
}


- (void)logout:(HGApiClientCompletion)resultBlock
{
    [HGUserSession setLoggedOut];
    
    [self POST:HGMethod.logout parameters:nil resultBlock:resultBlock];
}


- (void)sendPushToken:(NSString *)token resultBlock:(HGApiClientCompletion)resultBlock
{
    [self POST:HGMethod.sendPushToken parameters:token resultBlock:resultBlock];
}


- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock
{
    return [self.fileUploader uploadImageWithData:data name:name resultBlock:resultBlock];
}

- (NSURLSessionDataTask *)uploadImageWithData:(NSData *)data
                                         name:(NSString *)name
                                         type:(HGIdologyImageType)type
                                pendingUserId:(NSString *)userId
                                  resultBlock:(void (^)(HGExternalAttachment *attachment, NSError *error))resultBlock
{
    return [self.fileUploader uploadImageWithData:data
                                             name:name
                                             type:type
                                    pendingUserId:userId
                                      resultBlock:resultBlock];
}

- (void)removeImageWithExternalId:(NSString *)externalId
                    pendingUserId:(NSString *)userId
                        imageType:(HGIdologyImageType)type;
{
    if (type != HGIdologyImageTypeUnknown &&
        externalId.hasValue &&
        userId.hasValue )
    {
        NSMutableString *url = [[self.baseURL.absoluteString stringByAppendingPathComponent:@"upload"] mutableCopy] ;
        
        [url appendString:@"?delete=true&jquery-file-upload=true&externalId="];
        
        [url appendString:externalId];
        
        [url appendFormat:@"&jquery-file-upload-process-id=signup_idscan_%@_%@", (type == HGIdologyImageTypeBack) ? @"back" : @"front", userId];
        
        [self.fileUploader POST:url parameters:nil
                       progress:nil
                        success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            NSLog(@"Remove Image - response: \n\n%@\n", responseObject);
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            NSLog(@"Remove Image - error: \n\n%@\n", error);
                        }];
    }
}

@end
