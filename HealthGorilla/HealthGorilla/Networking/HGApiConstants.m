//
//  HGApiConstants.m
//  HG-Project
//
//  Created by not Paul on 25.09.15.
//  Copyright © 2015 HG-Inc. All rights reserved.
//

#import "HGApiConstants.h"

#define SET_STR(name) .name = @#name

const struct HGMethod HGMethod = {
    
    SET_STR(login),
    SET_STR(logout),
    SET_STR(setDevicePin),
    SET_STR(restorePassword),
    SET_STR(changePassword),
    SET_STR(acceptUserAgreement),
    SET_STR(sendPushToken),
    
    SET_STR(searchNPI),
    SET_STR(checkNPI),
    SET_STR(submitIdentityCheck),
    SET_STR(submitFindBySSN4),
    SET_STR(submitContactMe),
    SET_STR(submitResendCode),
    SET_STR(submitEmailConfirmation),
    SET_STR(checkInUseAndFormat),
    SET_STR(submitAutoLogin),
    SET_STR(submitDeletePendingUser),
    SET_STR(submitAccountInfo),
    SET_STR(performIdScanAction),
    SET_STR(performIdScanPollingResultsAction),
};



const struct HGUserType HGUserType = {
    
    .Patient = @"Patient",
    .Doctor = @"Doctor",
    .Facility = @"Facility",
};


const struct HGMedicalRequestStatus HGMedicalRequestStatus = {
    
    .InProgress = @"IN_PROGRESS",
    .Completed = @"COMPLETED",
    .Rejected = @"REJECTED",
};


const struct HGEncounterStatus HGEncounterStatus = {
    
    .Assigned = @"Assigned",
    .Completed = @"Completed",
};

#define SET_CONST(name,val) __unsafe_unretained NSString * const name = val

SET_CONST(HGUserNameParam, @"userName");
SET_CONST(HGPasswordParam, @"password");
SET_CONST(HGPinParam, @"pin");
SET_CONST(HGDeviceUidParam, @"deviceUid");
SET_CONST(HGAppIdParam, @"appId");
SET_CONST(HGTimezoneParam, @"tzId");
SET_CONST(HGResultParam, @"result");
SET_CONST(HGUserIdParam, @"userId");
SET_CONST(HGReadSaveParam, @"read$save");
SET_CONST(HGGeoCoordsParam, @"geoCoords");

SET_CONST(HGOnlyStaffParam, @"onlyStaff");
SET_CONST(HGPracticeIdParam, @"practiceId");
SET_CONST(HGPointsParam, @"points");
SET_CONST(HGPointParam, @"point");
SET_CONST(HGPopulateDistanceParam, @"populateDistance");
SET_CONST(HGSpecialityCode, @"specialityCode");
SET_CONST(HGAddressParam, @"address");
SET_CONST(HGAddress2Param, @"address2");
SET_CONST(HGZipParam, @"zip");
SET_CONST(HGDistanceParam, @"distance");
SET_CONST(HGFacilityTypeParam, @"facilityType");
SET_CONST(HGLocationsIdsParam, @"locationsIds");
SET_CONST(HGFacilityIdsParam, @"facilityIds");
SET_CONST(HGGenericCompendiumItemsParam, @"genericCompendiumItems");
SET_CONST(HGTenantField, @"tenant");
SET_CONST(HGLoginField, @"login");
SET_CONST(HGGroupSpecialitiesField, @"groupSpecialities");
SET_CONST(HGGroupTaxonomyCodesField, @"groupTaxonomyCodes");
SET_CONST(HGOtherSpecialitiesField, @"otherSpecialities");

SET_CONST(HGClassNameField, @"className");
SET_CONST(HGUserTypeField, @"userType");
SET_CONST(HGSearchPatternField, @"searchPattern");
SET_CONST(HGQueryField, @"query");
SET_CONST(HGClazzField, @"clazz");
SET_CONST(HGStartField, @"start");
SET_CONST(HGLimitField, @"limit");
SET_CONST(HGIncludeTotalField, @"includeTotal");
SET_CONST(HGIncludeInactiveField, @"includeInactive");
SET_CONST(HGInteractionIdsField, @"interactionIds");
SET_CONST(HGInteractionIdField, @"interactionId");
SET_CONST(HGForwardInterationIdField, @"forwardedInteractionId");
SET_CONST(HGForwardInterationTypeField, @"forwardedInteractionType");
SET_CONST(HGRepliedInteractionIdField, @"repliedInteractionId");
SET_CONST(HGRepliedInteractionTypeField, @"repliedInteractionType");
SET_CONST(HGSourceDocumentAttachmentField, @"sourceDocumentAttachmentId");
SET_CONST(HGAttachmentExternalIdsField, @"attachmentExternalIds");
SET_CONST(HGInteractionTypeField, @"interactionType");
SET_CONST(HGReadField, @"read");
SET_CONST(HGFollowUpField, @"followUp");
SET_CONST(HGLockedField, @"locked");
SET_CONST(HGMarkAllField, @"markAll");
SET_CONST(HGFirstNameField, @"firstName");
SET_CONST(HGLastNameField, @"lastName");
SET_CONST(HGGenderField, @"gender");
SET_CONST(HGCellPhoneField, @"cellPhone");
SET_CONST(HGDOBField, @"dateOfBirth");
SET_CONST(HGYearField, @"year");
SET_CONST(HGMonthField, @"month");
SET_CONST(HGDayField, @"day");
SET_CONST(HGMessageField, @"message");
SET_CONST(HGResetSourceAttachmentsField, @"resetSourceAttachments");

SET_CONST(HGAddField, @"add");
SET_CONST(HGRemoveField, @"remove");
SET_CONST(HGFaxNumberField, @"faxNumber");
SET_CONST(HGContactIdField, @"contactId");
SET_CONST(HGForceField, @"force");
SET_CONST(HGUpdatesField, @"updates");
SET_CONST(HGNpiField, @"npi");
SET_CONST(HGNpiNumberField, @"npiNumber");
SET_CONST(HGEmailField, @"email");
SET_CONST(HGInviteField, @"invite");

SET_CONST(HGGlobalPatientIdField, @"globalPatientId");
SET_CONST(HGNotesField, @"notes");
SET_CONST(HGExternalIdsField, @"externalIds");

SET_CONST(HGErrorParam, @"error");
SET_CONST(HGExceptionParam, @"exception");
SET_CONST(HGErrorMessage, @"errorMessage");
SET_CONST(HGErrorCode, @"code");
SET_CONST(HGSuccessfulParam, @"successful");

SET_CONST(HGUndefinedValue, @"undefined");
SET_CONST(HGDraftSavedValue, @"draftSaved");

SET_CONST(HGConfirmedJunglePriceField, @"confirmedJunglePrice");
SET_CONST(HGIgnoreFutureJunglePriceWarningField, @"ignoreFutureJunglePriceWarning");
SET_CONST(HGUnpaidField, @"unpaid");
