//
//  HGApiConstants.h
//  HG-Project
//
//  Created by not Pavel on 1/14/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#define DEF_STR(name) __unsafe_unretained NSString * const name

extern const struct HGMethod {
    
    DEF_STR(login);
    DEF_STR(logout);
    DEF_STR(setDevicePin);
    DEF_STR(restorePassword);
    DEF_STR(changePassword);
    DEF_STR(acceptUserAgreement);
    DEF_STR(sendPushToken);
    
    DEF_STR(searchNPI);
    DEF_STR(checkNPI);
    DEF_STR(submitIdentityCheck);
    DEF_STR(submitFindBySSN4);
    DEF_STR(submitContactMe);
    DEF_STR(submitResendCode);
    DEF_STR(submitEmailConfirmation);
    DEF_STR(checkInUseAndFormat);
    DEF_STR(submitAutoLogin);
    DEF_STR(submitDeletePendingUser);
    DEF_STR(submitAccountInfo);
    DEF_STR(performIdScanAction);
    DEF_STR(performIdScanPollingResultsAction);
  
    
    DEF_STR(executeBillingRequest);
    DEF_STR(cleanShowPaymentNotice);
    
    DEF_STR(getTelEncounterContext);
    DEF_STR(saveTelEncounter);
    
} HGMethod;


extern const struct HGUserType {
    
    DEF_STR(Patient);
    DEF_STR(Doctor);
    DEF_STR(Facility);
    
} HGUserType;


extern const struct HGMedicalRequestStatus {
    
    DEF_STR(InProgress);
    DEF_STR(Completed);
    DEF_STR(Rejected);
    
} HGMedicalRequestStatus;


extern const struct HGEncounterStatus {
    
    DEF_STR(Assigned);
    DEF_STR(Completed);
    
} HGEncounterStatus;

#define DEF_CONST(name) extern __unsafe_unretained NSString * const name

DEF_CONST(HGUserNameParam);
DEF_CONST(HGPasswordParam);
DEF_CONST(HGPinParam);
DEF_CONST(HGDeviceUidParam);
DEF_CONST(HGAppIdParam);
DEF_CONST(HGTimezoneParam);
DEF_CONST(HGResultParam);
DEF_CONST(HGUserIdParam);
DEF_CONST(HGReadSaveParam);
DEF_CONST(HGGeoCoordsParam);

DEF_CONST(HGResultTypeField);
DEF_CONST(HGResultTypeLab);
DEF_CONST(HGResultTypeRadiology);

DEF_CONST(HGOnlyStaffParam);
DEF_CONST(HGPracticeIdParam);
DEF_CONST(HGPointsParam);
DEF_CONST(HGPointParam);
DEF_CONST(HGPopulateDistanceParam);
DEF_CONST(HGSpecialityCode);
DEF_CONST(HGAddressParam);
DEF_CONST(HGAddress2Param);
DEF_CONST(HGZipParam);
DEF_CONST(HGDistanceParam);
DEF_CONST(HGFacilityTypeParam);
DEF_CONST(HGLocationsIdsParam);
DEF_CONST(HGFacilityIdsParam);
DEF_CONST(HGGenericCompendiumItemsParam);
DEF_CONST(HGTenantField);
DEF_CONST(HGLoginField);
DEF_CONST(HGGroupSpecialitiesField);
DEF_CONST(HGGroupTaxonomyCodesField);
DEF_CONST(HGOtherSpecialitiesField);

DEF_CONST(HGClassNameField);
DEF_CONST(HGUserTypeField);
DEF_CONST(HGSearchPatternField);
DEF_CONST(HGQueryField);
DEF_CONST(HGClazzField);
DEF_CONST(HGStartField);
DEF_CONST(HGLimitField);
DEF_CONST(HGIncludeTotalField);
DEF_CONST(HGIncludeInactiveField);
DEF_CONST(HGInteractionIdsField);
DEF_CONST(HGInteractionIdField);
DEF_CONST(HGForwardInterationIdField);
DEF_CONST(HGForwardInterationTypeField);
DEF_CONST(HGRepliedInteractionIdField);
DEF_CONST(HGRepliedInteractionTypeField);
DEF_CONST(HGSourceDocumentAttachmentField);
DEF_CONST(HGAttachmentExternalIdsField);
DEF_CONST(HGInteractionTypeField);
DEF_CONST(HGReadField);
DEF_CONST(HGFollowUpField);
DEF_CONST(HGLockedField);
DEF_CONST(HGMarkAllField);
DEF_CONST(HGFirstNameField);
DEF_CONST(HGLastNameField);
DEF_CONST(HGGenderField);
DEF_CONST(HGCellPhoneField);
DEF_CONST(HGDOBField);
DEF_CONST(HGYearField);
DEF_CONST(HGMonthField);
DEF_CONST(HGDayField);
DEF_CONST(HGMessageField);
DEF_CONST(HGResetSourceAttachmentsField);

DEF_CONST(HGAddField);
DEF_CONST(HGRemoveField);
DEF_CONST(HGFaxNumberField);
DEF_CONST(HGContactIdField);
DEF_CONST(HGForceField);
DEF_CONST(HGUpdatesField);
DEF_CONST(HGNpiField);
DEF_CONST(HGNpiNumberField);
DEF_CONST(HGEmailField);
DEF_CONST(HGInviteField);

DEF_CONST(HGGlobalPatientIdField);
DEF_CONST(HGNotesField);
DEF_CONST(HGExternalIdsField);

DEF_CONST(HGErrorParam);
DEF_CONST(HGExceptionParam);
DEF_CONST(HGErrorMessage);
DEF_CONST(HGErrorCode);
DEF_CONST(HGSuccessfulParam);

DEF_CONST(HGDraftSavedValue);
DEF_CONST(HGUndefinedValue);

DEF_CONST(HGConfirmedJunglePriceField);
DEF_CONST(HGIgnoreFutureJunglePriceWarningField);
DEF_CONST(HGUnpaidField);
