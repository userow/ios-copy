//
//  HGRouteHandler.m
//  HG-Project
//
//  Created by not Pavel on 1/21/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGRouteHandler.h"
#import "HGResultInfoViewController.h"
#import "HGDocumentDetailsViewController.h"
#import "HGReferralDetailsViewController.h"
#import "HGOrderDetailsViewController.h"
#import "HGMessageInfoViewController.h"
#import "HGInteractionsListViewController.h"
#import "HGAlertMessageViewController.h"
#import "NSBundle+HG.h"
#import "HGUserSession.h"


@interface HGRouteHandler ()

@property (nonatomic, strong) UIViewController *deferredController;

@end


@implementation HGRouteHandler

SYNTHESIZE_SINGLETON(HGRouteHandler);

- (void)handleURL:(NSURL *)url
{
    UIViewController *controller = nil;
    if ([url.host rangeOfString:@"result"].location != NSNotFound) {
        controller = [self showResultWithId:url.query];
    } else if ([url.host rangeOfString:@"document"].location != NSNotFound) {
        controller = [self showDocumentWithId:url.query];
    } else if ([url.host rangeOfString:@"referral"].location != NSNotFound) {
        controller = [self showReferralWithId:url.query];
    } else if ([url.host rangeOfString:@"order"].location != NSNotFound) {
        controller = [self showOrderWithId:url.query];
    } else if ([url.host rangeOfString:@"message"].location != NSNotFound) {
        controller = [self showMessageWithId:url.query];
    } else if ([url.host rangeOfString:@"signup-finished"].location != NSNotFound) {
        [self didFinishWebConfirmationWithQuery:url.query];
    } else {
        NSLog(@"Unknown URL: %@", url);
    }
    if (controller) {
        self.deferredController = controller;
    }
}


- (void)handlePush:(NSDictionary *)push onAppLaunch:(bool)onAppLaunch
{
    bool instantly = NO;
    bool shouldAsk = NO; // never ask user to show push message, see [IOS-78]
    
    UIApplication *application = [UIApplication sharedApplication];
    
#ifdef HG_QA
    NSLog(@"PUSH comes to %@ app with state %@",
          (onAppLaunch ? @"launching" : @"living"),
          (UIApplicationStateActive == application.applicationState) ? @"active" :
          (UIApplicationStateInactive == application.applicationState) ? @"inactive" : @"background");
    NSLog(@"%@", push);
#endif
    
    // check active app state
    bool activeApp = (UIApplicationStateActive == application.applicationState);
    if (activeApp) {
#ifdef HG_QA
        NSLog(@"App is active, hence Push is ignored");
#endif
        return; // [IOS-78] Suppress all pushes when application is active
    }
    
    // check inactive app state
    bool inactiveApp = (UIApplicationStateInactive == application.applicationState);
    if (inactiveApp) {
        if ([HGUserSession isLoggedIn] && NO == [HGUserSession isExpired]) {
            instantly = YES; // show right now to logged-in user w/o PIN screen
        }
    }
    
#ifdef HG_QA
    NSLog(@"User loggedIn = %@", @([HGUserSession isLoggedIn]));
    NSLog(@"User session expired = %@", @([HGUserSession isExpired]));
    NSLog(@"Show push instantly = %@", @(instantly));
#endif
    
    //    "aps" : {
    //        "alert" : "New order issued on behalf of the doctor"
    //    },
    //    "eventId" : "newOrder",
    //    "entityId" : "document",
    //    "objectId" : "12345678"
    
    NSString *eventId = push[@"eventId"];
    if (NO == [eventId isKindOfClass:[NSString class]]) {
        return; // no eventId, required
    }
    
    NSString *objectId = push[@"objectId"];
    NSString *entityId = push[@"entityId"];
    NSString *message = [push valueForKeyPath:@"aps.alert"];
    
    UIViewController * (^actionHandler)() = nil;
    if ([eventId isEqual:@"newOrderOnBehalf"]) {
        actionHandler = ^ UIViewController * {
            return [self showOrderWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"siqnatureRequired"]) {
        actionHandler = ^ UIViewController * {
            return [self showEntityWithId:entityId objectId:objectId];
        };
    }
    else if ([eventId isEqual:@"newOrderFrom"]) {
        actionHandler = ^ UIViewController * {
            return [self showOrderWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"newMessage"]) {
        actionHandler = ^ UIViewController * {
            return [self showMessageWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"newResult"]) {
        actionHandler = ^ UIViewController * {
            return [self showResultWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"newAbnormalResult"]) {
        actionHandler = ^ UIViewController * {
            return [self showResultWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"newReferral"]) {
        actionHandler = ^ UIViewController * {
            return [self showReferralWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"newReferralResponse"]) {
        actionHandler = ^ UIViewController * {
            return [self showReferralWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"newFax"]) {
        actionHandler = ^ UIViewController * {
            return [self showMessageWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"newDocument"]) {
        actionHandler = ^ UIViewController * {
            return [self showDocumentWithId:objectId];
        };
    }
    else if ([eventId isEqual:@"recordSigned"]) {
        actionHandler = ^ UIViewController * {
            return [self showEntityWithId:entityId objectId:objectId];
        };
    }
    else if ([eventId isEqual:@"recordLocked"]) {
        // planned in 2.5
        //actionHandler = ^ UIViewController * {
        //    return [self showEntityWithId:entityId objectId:objectId];
        //};
    }
    else if ([eventId isEqual:@"unreadMessages"]) {
        actionHandler = ^ UIViewController * {
            return [self showMessages];
        };
    }
    else if ([eventId isEqual:@"unmatchedFaxes"]) {
        actionHandler = ^ UIViewController * {
            return [self showMessages];
        };
    }
    else if ([eventId isEqual:@"pendingReferrals"]) {
        actionHandler = ^ UIViewController * {
            return [self showReferrals];
        };
    }
    else if ([eventId isEqual:@"pendingOrders"]) {
        actionHandler = ^ UIViewController * {
            return [self showOrders];
        };
    }
    else if ([eventId isEqual:@"pendingDocuments"]) {
        actionHandler = ^ UIViewController * {
            return [self showDocuments];
        };
    }
    else if ([eventId isEqual:@"unreadInbox"]) {
        actionHandler = ^ UIViewController * {
            return [self showMDInbox];
        };
    }
    else if ([eventId isEqual:@"newMedicalRecordsShared"]) {
        // show nothing 'My Requests' is not implemented
    }
    else if ([eventId isEqual:@"alert"]) {
        actionHandler = ^ UIViewController * {
            return [self showAlertMessage:message];
        };
    }
    
    if (!actionHandler) {
        return; // unknown eventId
    }
    
    if (instantly)
    {
        
#ifdef HG_QA
        NSLog(@"PUSH will show immediately");
#endif

        void (^presentationHandler)() = ^{
            UIViewController *controller = actionHandler();
            if (controller) {
                [self presentController:controller];
            }
        };
        
        if (!shouldAsk) {
            presentationHandler();
            return; // set to show instantly
        }
        
        NSString *title = [NSBundle displayName];
        NSString *message = [push valueForKeyPath:@"aps.alert"];
        
        if (NO == [message isKindOfClass:[NSString class]]) {
            presentationHandler();
            return; // no message to show
        }
        
        // show alert
        PSPDFAlertView *alert = [HGAlertView alertViewWithTitle:title andMessage:message];
        [alert addButtonWithTitle:@"Open" block:^(NSInteger buttonIndex) {
            presentationHandler();
        }];
        [alert setCancelButtonWithTitle:@"Cancel" block:^(NSInteger buttonIndex) {
            // nop
        }];
        [alert show];
    }
    else {
        
#ifdef HG_QA
        NSLog(@"PUSH is deferred");
#endif
        self.deferredController = actionHandler();
    }
}


- (UIViewController *)showEntityWithId:(NSString *)entityId objectId:(NSString *)objectId
{
    if (NO == [objectId isKindOfClass:[NSString class]]) {
        return nil; // no objectId
    }
    if (NO == [entityId isKindOfClass:[NSString class]]) {
        return nil; // no entityId
    }
    
    UIViewController *controller = nil;
    if ([entityId isEqual:@"order"]) {
        controller = [self showOrderWithId:objectId];
    }
    else if ([entityId isEqual:@"document"]) {
        controller = [self showDocumentWithId:objectId];
    }
    else if ([entityId isEqual:@"referral"]) {
        controller = [self showReferralWithId:objectId];
    }
    else if ([entityId isEqual:@"result"]) {
        controller = [self showResultWithId:objectId];
    }
    else if ([entityId isEqual:@"message"]) {
        controller = [self showMessageWithId:objectId];
    }
    return controller;
}


- (UIViewController *)showOrderWithId:(NSString *)objectId
{
    NSLog(@"%s, id = %@", __FUNCTION__, objectId);
    
    if (NO == [objectId isKindOfClass:[NSString class]]) {
        return nil; // no objectId
    }
    
    HGFacilityOrder *order = [[HGFacilityOrder alloc] init];
    order.uid = objectId;
    order.interactionType = HGInteractionType.Order;
    
    HGOrderDetailsViewController *controller = [HGOrderDetailsViewController instanceWithOrder:order];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showReferralWithId:(NSString *)objectId
{
    NSLog(@"%s, id = %@", __FUNCTION__, objectId);
    
    if (NO == [objectId isKindOfClass:[NSString class]]) {
        return nil; // no objectId
    }
    
    HGReferral *referral = [[HGReferral alloc] init];
    referral.uid = objectId;
    referral.interactionType = HGInteractionType.Referral;
    
    HGReferralDetailsViewController *controller = [HGReferralDetailsViewController instanceWithReferral:referral];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showMessageWithId:(NSString *)objectId
{
    NSLog(@"%s, id = %@", __FUNCTION__, objectId);
    
    if (NO == [objectId isKindOfClass:[NSString class]]) {
        return nil; // no objectId
    }
    
    HGMessage *message = [[HGMessage alloc] init];
    message.uid = objectId;
    message.interactionType = HGInteractionType.Message;
    
    HGMessageInfoViewController *controller = [HGMessageInfoViewController instanceWithMessage:message];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showResultWithId:(NSString *)objectId
{
    NSLog(@"%s, id = %@", __FUNCTION__, objectId);
    
    if (NO == [objectId isKindOfClass:[NSString class]]) {
        return nil; // no objectId
    }
    
    HGFacilityResult *result = [[HGFacilityResult alloc] init];
    result.uid = objectId;
    result.interactionType = HGInteractionType.Result;
    
    HGResultInfoViewController *controller = [HGResultInfoViewController instanceWithResult:result];
    controller.isExternalRoute = YES;
    return controller;

}


- (UIViewController *)showDocumentWithId:(NSString *)objectId
{
    NSLog(@"%s, id = %@", __FUNCTION__, objectId);
    
    if (NO == [objectId isKindOfClass:[NSString class]]) {
        return nil; // no objectId
    }
    
    HGDocument *document = [[HGDocument alloc] init];
    document.uid = objectId;
    document.interactionType = HGInteractionType.Document;
    
    HGDocumentDetailsViewController *controller = [HGDocumentDetailsViewController instanceWithDocument:document];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showMessages
{
    HGInteractionsListViewController *controller = [[UIStoryboard storyboardWithName:@"Messages" bundle:nil] instantiateInitialViewController];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showOrders
{
    HGInteractionsListViewController *controller = [[UIStoryboard storyboardWithName:@"Orders" bundle:nil] instantiateInitialViewController];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showResults
{
    HGInteractionsListViewController *controller = [[UIStoryboard storyboardWithName:@"Results" bundle:nil] instantiateInitialViewController];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showReferrals
{
    HGInteractionsListViewController *controller = [[UIStoryboard storyboardWithName:@"Referrals" bundle:nil] instantiateInitialViewController];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showDocuments
{
    HGInteractionsListViewController *controller = [[UIStoryboard storyboardWithName:@"Documents" bundle:nil] instantiateInitialViewController];
    controller.isExternalRoute = YES;
    return controller;
}


- (UIViewController *)showMDInbox
{
    UINavigationController *controller = [[UIStoryboard storyboardWithName:@"MainMD" bundle:nil] instantiateViewControllerWithIdentifier:@"HGInboxMDID"];
    return controller;
}


- (UIViewController *)showPTInbox
{
    UINavigationController *controller = [[UIStoryboard storyboardWithName:@"MainPT" bundle:nil] instantiateViewControllerWithIdentifier:@"HGInboxPTID"];
    return controller;
}


- (UIViewController *)showAlertMessage:(NSString *)message
{
    HGAlertMessageViewController *controller = [HGAlertMessageViewController instanceWithMessage:message];
    return controller;
}


- (void)didFinishWebConfirmationWithQuery:(NSString *)query
{
    // user=loginName
    NSString *username = nil;
    
    NSArray<NSString *> *args = [query componentsSeparatedByString:@"&"];
    for (NSString *item in args) {
        NSArray<NSString *> *components = [item componentsSeparatedByString:@"="];
        if (2 != components.count) {
            continue;
        }
        if ([components.firstObject.lowercaseString isEqualToString:@"user"]) {
            // FIX: decode %40 to @ in emails
            username = [components.lastObject stringByRemovingPercentEncoding];
        }
    }
    
    if (!username) {
        return; // something goes wrong in URL format
    }
    
    // send notification with username to Login screen
    [[NSNotificationCenter defaultCenter] postNotificationName:HGNotification.didFinishWebConfirmation object:@ {
        HGUserNameParam : username,
    }];
}


- (void)presentController:(UIViewController *)controller
{
    UIViewController *parent = [self presentingViewController];
    
    UIBarButtonItem *button = [HGBarButtonFactory doneButtonWithAction:^(id sender) {
        [parent dismissViewControllerAnimated:YES completion:^{
            [self clear];
        }];
    }];
    controller.navigationItem.leftBarButtonItem = button;
    
    HGNavigationViewController *nav = [[HGNavigationViewController alloc] initWithRootViewController:controller];
    [parent presentViewController:nav animated:YES completion:nil];
}


- (UIViewController *)presentingViewController
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    UIViewController *controller = window.visibleViewController;
    if (!controller) {
        controller = window.rootViewController;
    }
    return controller;
}


- (bool)hasDeferredController
{
    return nil != _deferredController;
}


- (void)presentDeferredController
{
    UIViewController *controller = self.deferredController;
    
    //FIX: warning: Unbalanced calls to begin/end appearance transitions for
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self presentController:controller];
    }];
}


- (void)clear
{
    self.deferredController = nil;
}

@end
