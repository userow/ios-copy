//
//  HGAlertMessageViewController.m
//  HG-Project
//
//  Created by not Paul on 17/04/2017.
//  Copyright © 2017 HG-Inc. All rights reserved.
//

#import "HGAlertMessageViewController.h"


@interface HGAlertMessageViewController ()

@property (nonatomic, strong) NSString *message;
//
@property (nonatomic, weak) IBOutlet UITextView *textView;

@end


@implementation HGAlertMessageViewController

+ (instancetype)instanceWithMessage:(NSString *)message
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    HGAlertMessageViewController *c = [storyboard instantiateViewControllerWithIdentifier:@"AlertMessageID"];
    
    c.message = message;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    self.navigationController.navigationBar.translucent = NO;
    self.textView.text = self.message;
}

@end
