//
//  HGPush.m
//  HG-Project
//
//  Created by not Pavel on 10.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

#import "HGPush.h"


NSString * const kDidRegisterTokenForRemoteNotifications = @"com.hg.didRegisterTokenForRemoteNotifications";
NSString * const kSuccessKey = @"success";

@implementation HGPush

+ (NSString *)tokenWithData:(NSData *)data
{
	NSCharacterSet *trimSet = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
	return [[[data description] stringByTrimmingCharactersInSet:trimSet]
			stringByReplacingOccurrencesOfString:@" " withString:@""];
}


+ (void)setTokenWithData:(NSData *)data
{
    NSString *token = [self tokenWithData:data];
    [HGUser setPushToken:token];
}


+ (BOOL)needAskPermissions
{
	return NO == [HGUser hasPushToken];
}


+ (BOOL)isRegisteredForRemoteNotifications
{
	if (NO == [HGUser hasPushToken]) {
		return NO;
	}
    // check system settings for an app (iOS 8.0+)
	UIApplication *application = [UIApplication sharedApplication];
    return [application isRegisteredForRemoteNotifications];
}


+ (void)registerForRemoteNotifications
{
	UIApplication *application = [UIApplication sharedApplication];
	
    // iOS 8.0+
    NSSet *categories = nil; //TODO: setup categories for interactive pushes
    UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    [application registerUserNotificationSettings:settings];
    
    // request for remote notification on device only
    if (NO == [HGDevice isSimulator]) {
        [application registerForRemoteNotifications];
    }
}


+ (void)notifyDidRegisterTokenWithSuccess:(bool)success
{
	NSDictionary *userInfo = @ {
		kSuccessKey : @(success)
	};
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:kDidRegisterTokenForRemoteNotifications object:nil userInfo:userInfo];
}

@end
