//
//  HGRouteHandler.h
//  HG-Project
//
//  Created by not Pavel on 1/21/15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

@interface HGRouteHandler : NSObject

DECLARE_SINGLETON(HGRouteHandler);

@property (nonatomic, readonly) bool hasDeferredController;

- (void)handleURL:(NSURL *)url;
- (void)handlePush:(NSDictionary *)push onAppLaunch:(bool)onAppLaunch;
- (void)presentDeferredController;
- (void)clear;

@end
