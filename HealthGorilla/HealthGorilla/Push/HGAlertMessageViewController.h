//
//  HGAlertMessageViewController.h
//  HG-Project
//
//  Created by not Paul on 17/04/2017.
//  Copyright © 2017 HG-Inc. All rights reserved.
//

@interface HGAlertMessageViewController : UIViewController

+ (instancetype)instanceWithMessage:(NSString *)message;

@end
