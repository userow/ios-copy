//
//  HGPush.h
//  HG-Project
//
//  Created by not Pavel on 10.06.15.
//  Copyright (c) 2015 HG-Inc. All rights reserved.
//

extern NSString * const kDidRegisterTokenForRemoteNotifications;
extern NSString * const kSuccessKey;

@interface HGPush : NSObject

+ (void)setTokenWithData:(NSData *)data;
+ (BOOL)needAskPermissions;
+ (BOOL)isRegisteredForRemoteNotifications;
+ (void)registerForRemoteNotifications;
+ (void)notifyDidRegisterTokenWithSuccess:(bool)success;

@end
