//
//  HGApplication.m
//  HG-Project
//
//  Created by not Pavel on 6/10/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "HGApplication.h"
#import "HGUserSession.h"


@interface HGApplication ()

@property (nonatomic, strong) NSTimer *idleTimer;

@end


@implementation HGApplication

- (void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    // Here we are listening for any touch. If the screen receives touch, the timer is reset
    if (nil == _idleTimer) {
        [self resetIdleTimer];
    }
    
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        
        if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded) {
            [self resetIdleTimer];
        }
        
    }
}

- (void)resetIdleTimer
{
    // Resets idle timer to it initial value.
    if (_idleTimer) {
        [_idleTimer invalidate];
    }
    
    if ([HGUserSession isLoggedIn]) {
        self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:[HGUserSession timeoutSec]
                                                          target:self
                                                        selector:@selector(idleTimerExceeded)
                                                        userInfo:nil
                                                         repeats:NO];
    }
}


- (void)idleTimerExceeded
{
    [[self class] postTimeoutNotification];
}


+ (void)postTimeoutNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:HGNotification.sessionDidTimeout object:nil];
}

@end
