//
//  main.m
//  HG-Project
//
//  Created by not Pavel on 5/26/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

#import "HGAppDelegate.h"
#import "HGApplication.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv,
                                 NSStringFromClass([HGApplication class]),
                                 NSStringFromClass([HGAppDelegate class]));
    }
}
