//
//  HGApplication.h
//  HG-Project
//
//  Created by not Pavel on 6/10/14.
//  Copyright (c) 2014 HG-Inc. All rights reserved.
//

@interface HGApplication : UIApplication

+ (void)postTimeoutNotification;

@end
