#!/bin/bash

mkdir ./temp
mkdir ./temp/en.lproj
mkdir ./temp/es.lproj
mkdir ./temp/Base.lproj 

printf " \n"
printf "    STORYBOARD: Billing\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Billing.*    ./Charts/en.lproj
mv ./temp/es.lproj/Billing.* ./Charts/es.lproj
mv ./temp/Base.lproj/Billing.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Billing
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Documents\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Documents.*    ./Charts/en.lproj
mv ./temp/es.lproj/Documents.* ./Charts/es.lproj
mv ./temp/Base.lproj/Documents.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Documents
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Encounters\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Encounters.*    ./Charts/en.lproj
mv ./temp/es.lproj/Encounters.* ./Charts/es.lproj
mv ./temp/Base.lproj/Encounters.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Encounters
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Messages\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Messages.*    ./Charts/en.lproj
mv ./temp/es.lproj/Messages.* ./Charts/es.lproj
mv ./temp/Base.lproj/Messages.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Messages
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: NewDocument\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/NewDocument.*    ./Charts/en.lproj
mv ./temp/es.lproj/NewDocument.* ./Charts/es.lproj
mv ./temp/Base.lproj/NewDocument.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags NewDocument
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: NewMessage\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/NewMessage.*    ./Charts/en.lproj
mv ./temp/es.lproj/NewMessage.* ./Charts/es.lproj
mv ./temp/Base.lproj/NewMessage.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags NewMessage
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: NewOrder\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/NewOrder.*    ./Charts/en.lproj
mv ./temp/es.lproj/NewOrder.* ./Charts/es.lproj
mv ./temp/Base.lproj/NewOrder.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags NewOrder
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: NewReferral\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/NewReferral.*    ./Charts/en.lproj
mv ./temp/es.lproj/NewReferral.* ./Charts/es.lproj
mv ./temp/Base.lproj/NewReferral.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags NewReferral
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: NewRequest\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/NewRequest.*    ./Charts/en.lproj
mv ./temp/es.lproj/NewRequest.* ./Charts/es.lproj
mv ./temp/Base.lproj/NewRequest.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags NewRequest
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Orders\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Orders.*    ./Charts/en.lproj
mv ./temp/es.lproj/Orders.* ./Charts/es.lproj
mv ./temp/Base.lproj/Orders.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Orders
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Referrals\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Referrals.*    ./Charts/en.lproj
mv ./temp/es.lproj/Referrals.* ./Charts/es.lproj
mv ./temp/Base.lproj/Referrals.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Referrals
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Results\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Results.*    ./Charts/en.lproj
mv ./temp/es.lproj/Results.* ./Charts/es.lproj
mv ./temp/Base.lproj/Results.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Results
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Sharing\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Sharing.*    ./Charts/en.lproj
mv ./temp/es.lproj/Sharing.* ./Charts/es.lproj
mv ./temp/Base.lproj/Sharing.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Sharing
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj


printf " \n"
printf "    STORYBOARD: Signature\n"
mv ./Charts/en.lproj/*.*    ./temp/en.lproj
mv ./Charts/es.lproj/*.* ./temp/es.lproj
mv ./Charts/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Signature.*    ./Charts/en.lproj
mv ./temp/es.lproj/Signature.* ./Charts/es.lproj
mv ./temp/Base.lproj/Signature.*  ./Charts/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Charts.txt ./Charts --developer-language en --consume-all --consume-comments --tags Signature
mv ./temp/en.lproj/*.*    ./Charts/en.lproj
mv ./temp/es.lproj/*.* ./Charts/es.lproj
mv ./temp/Base.lproj/*.*  ./Charts/Base.lproj


printf " \n"
printf "    STORYBOARD: DocViewer\n"
mv ./Main/en.lproj/*.*    ./temp/en.lproj
mv ./Main/es.lproj/*.* ./temp/es.lproj
mv ./Main/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/DocViewer.*    ./Main/en.lproj
mv ./temp/es.lproj/DocViewer.* ./Main/es.lproj
mv ./temp/Base.lproj/DocViewer.*  ./Main/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Main.txt ./Main --developer-language en --consume-all --consume-comments --tags DocViewer
mv ./temp/en.lproj/*.*    ./Main/en.lproj
mv ./temp/es.lproj/*.* ./Main/es.lproj
mv ./temp/Base.lproj/*.*  ./Main/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Invitation\n"
mv ./Main/en.lproj/*.*    ./temp/en.lproj
mv ./Main/es.lproj/*.* ./temp/es.lproj
mv ./Main/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Invitation.*    ./Main/en.lproj
mv ./temp/es.lproj/Invitation.* ./Main/es.lproj
mv ./temp/Base.lproj/Invitation.*  ./Main/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Main.txt ./Main --developer-language en --consume-all --consume-comments --tags Invitation
mv ./temp/en.lproj/*.*    ./Main/en.lproj
mv ./temp/es.lproj/*.* ./Main/es.lproj
mv ./temp/Base.lproj/*.*  ./Main/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Login\n"
mv ./Main/en.lproj/*.*    ./temp/en.lproj
mv ./Main/es.lproj/*.* ./temp/es.lproj
mv ./Main/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Login.*    ./Main/en.lproj
mv ./temp/es.lproj/Login.* ./Main/es.lproj
mv ./temp/Base.lproj/Login.*  ./Main/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Main.txt ./Main --developer-language en --consume-all --consume-comments --tags Login
mv ./temp/en.lproj/*.*    ./Main/en.lproj
mv ./temp/es.lproj/*.* ./Main/es.lproj
mv ./temp/Base.lproj/*.*  ./Main/Base.lproj
 

printf " \n"
printf "    STORYBOARD: MainMD\n"
mv ./Main/en.lproj/*.*    ./temp/en.lproj
mv ./Main/es.lproj/*.* ./temp/es.lproj
mv ./Main/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/MainMD.*    ./Main/en.lproj
mv ./temp/es.lproj/MainMD.* ./Main/es.lproj
mv ./temp/Base.lproj/MainMD.*  ./Main/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Main.txt ./Main --developer-language en --consume-all --consume-comments --tags MainMD
mv ./temp/en.lproj/*.*    ./Main/en.lproj
mv ./temp/es.lproj/*.* ./Main/es.lproj
mv ./temp/Base.lproj/*.*  ./Main/Base.lproj
 

printf " \n"
printf "    STORYBOARD: MainPT\n"
mv ./Main/en.lproj/*.*    ./temp/en.lproj
mv ./Main/es.lproj/*.* ./temp/es.lproj
mv ./Main/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/MainPT.*    ./Main/en.lproj
mv ./temp/es.lproj/MainPT.* ./Main/es.lproj
mv ./temp/Base.lproj/MainPT.*  ./Main/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Main.txt ./Main --developer-language en --consume-all --consume-comments --tags MainPT
mv ./temp/en.lproj/*.*    ./Main/en.lproj
mv ./temp/es.lproj/*.* ./Main/es.lproj
mv ./temp/Base.lproj/*.*  ./Main/Base.lproj
 

printf " \n"
printf "    STORYBOARD: PatientAttachment\n"
mv ./Patients/en.lproj/*.*    ./temp/en.lproj
mv ./Patients/es.lproj/*.* ./temp/es.lproj
mv ./Patients/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/PatientAttachment.*    ./Patients/en.lproj
mv ./temp/es.lproj/PatientAttachment.* ./Patients/es.lproj
mv ./temp/Base.lproj/PatientAttachment.*  ./Patients/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Patients.txt ./Patients --developer-language en --consume-all --consume-comments --tags PatientAttachment
mv ./temp/en.lproj/*.*    ./Patients/en.lproj
mv ./temp/es.lproj/*.* ./Patients/es.lproj
mv ./temp/Base.lproj/*.*  ./Patients/Base.lproj
 

printf " \n"
printf "    STORYBOARD: PatientProfile\n"
mv ./Patients/en.lproj/*.*    ./temp/en.lproj
mv ./Patients/es.lproj/*.* ./temp/es.lproj
mv ./Patients/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/PatientProfile.*    ./Patients/en.lproj
mv ./temp/es.lproj/PatientProfile.* ./Patients/es.lproj
mv ./temp/Base.lproj/PatientProfile.*  ./Patients/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Patients.txt ./Patients --developer-language en --consume-all --consume-comments --tags PatientProfile
mv ./temp/en.lproj/*.*    ./Patients/en.lproj
mv ./temp/es.lproj/*.* ./Patients/es.lproj
mv ./temp/Base.lproj/*.*  ./Patients/Base.lproj
 

printf " \n"
printf "    STORYBOARD: SelectPatientOrCreate\n"
mv ./Patients/en.lproj/*.*    ./temp/en.lproj
mv ./Patients/es.lproj/*.* ./temp/es.lproj
mv ./Patients/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/SelectPatientOrCreate.*    ./Patients/en.lproj
mv ./temp/es.lproj/SelectPatientOrCreate.* ./Patients/es.lproj
mv ./temp/Base.lproj/SelectPatientOrCreate.*  ./Patients/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Patients.txt ./Patients --developer-language en --consume-all --consume-comments --tags SelectPatientOrCreate
mv ./temp/en.lproj/*.*    ./Patients/en.lproj
mv ./temp/es.lproj/*.* ./Patients/es.lproj
mv ./temp/Base.lproj/*.*  ./Patients/Base.lproj
 

printf " \n"
printf "    STORYBOARD: Settings\n"
mv ./Settings/en.lproj/*.*    ./temp/en.lproj
mv ./Settings/es.lproj/*.* ./temp/es.lproj
mv ./Settings/Base.lproj/*.*  ./temp/Base.lproj
mv ./temp/en.lproj/Settings.*    ./Settings/en.lproj
mv ./temp/es.lproj/Settings.* ./Settings/es.lproj
mv ./temp/Base.lproj/Settings.*  ./Settings/Base.lproj
twine consume-all-localization-files ./twine-Storyboards-Settings.txt ./Settings --developer-language en --consume-all --consume-comments --tags Settings
mv ./temp/en.lproj/*.*    ./Settings/en.lproj
mv ./temp/es.lproj/*.* ./Settings/es.lproj
mv ./temp/Base.lproj/*.*  ./Settings/Base.lproj
 
