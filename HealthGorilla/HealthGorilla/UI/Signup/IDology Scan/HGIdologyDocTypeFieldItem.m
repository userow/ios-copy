//
//  HGIdologyDocTypeFieldItem.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGIdologyDocTypeFieldItem.h"

@implementation HGIdologyDocTypeFieldItem

+ (instancetype)itemWithId:(HGSignupFieldID)identifier errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler
{
    return [[self alloc] initWithId:identifier errorId:errorId getHandler:getHandler commitHandler:commitHandler localValidateHandler:localValidateHandler remoteValidateHandler:remoteValidateHandler];
}


#pragma mark - Overloads

- (NSString *)cellId
{
    static NSString *cellId = @"HGIdologyDocTypeFieldCell";
    return cellId;
}

@end
