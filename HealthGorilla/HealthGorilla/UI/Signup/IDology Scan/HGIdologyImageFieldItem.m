//
//  HGIdologyImageFieldItem.m
//  HG-Project
//
//  Created by Paul Vasilenko on 2018-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGIdologyImageFieldItem.h"

@implementation HGIdologyImageFieldItem

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _imageType = HGIdologyImageTypeFront;
    
    return self;
}

+ (instancetype)itemWithId:(HGSignupFieldID)identifier
                   errorId:(NSString *)errorId
                getHandler:(HGSignupAccountFieldGetHandler)getHandler
             commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler
      localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler
     remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler
{
    return [[self alloc] initWithId:identifier errorId:errorId getHandler:getHandler commitHandler:commitHandler localValidateHandler:localValidateHandler remoteValidateHandler:remoteValidateHandler];
}


#pragma mark - Overloads

- (NSString *)cellId
{
    static NSString *cellId = @"HGIdologyImageFieldCell";
    return cellId;
}

@end
