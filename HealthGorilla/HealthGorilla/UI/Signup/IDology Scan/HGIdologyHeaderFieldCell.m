//
//  HGIdologyHeaderFieldCell.h
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGIdologyHeaderFieldCell.h"
//#import "HGIdologyHeaderFieldItem.h" //???

@interface HGIdologyHeaderFieldCell ()

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation HGIdologyHeaderFieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _headerLabel.text = @"Thank you for your application. To complete your enrollment, we will need to collect some additional documentation. Please have your Driver's License, State Issued ID, or Passport on hand. Follow the instructions to upload a picture of your document.".localized;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
