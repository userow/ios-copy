//
//  HGIdologyDocTypeFieldItem.h
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGSignupAccountFieldItem.h"

@interface HGIdologyDocTypeFieldItem : HGSignupAccountFieldItem

+ (instancetype)itemWithId:(HGSignupFieldID)identifier errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler;

@end
