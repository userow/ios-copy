//
//  HGIdologyImageFieldItem.h
//  HG-Project
//
//  Created by Paul Vasilenko on 2018-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGSignupAccountFieldItem.h"
#import "HGAppConfig.h"

@interface HGIdologyImageFieldItem : HGSignupAccountFieldItem

@property (assign) HGIdologyImageType imageType;

+ (instancetype)itemWithId:(HGSignupFieldID)identifier
                   errorId:(NSString *)errorId
                getHandler:(HGSignupAccountFieldGetHandler)getHandler
             commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler
      localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler
     remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler;

@end
