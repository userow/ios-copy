//
//  HGIdologyDocTypeFieldCell.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGIdologyDocTypeFieldCell.h"
#import "HGIdologyDocTypeFieldItem.h"
#import "HGIdologyDocType.h"


@interface HGIdologyDocTypeFieldCell ()

@property (nonatomic, weak) IBOutlet UISegmentedControl *docTypeSegments;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end


@implementation HGIdologyDocTypeFieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    _titleLabel.text = @"Please select the document type".localized;
    
    for (int i = 0; i < _docTypeSegments.numberOfSegments; i++) {
        [_docTypeSegments setTitle:[(NSString *)[HGIdologyDocType allDocTypesDescriptions][i] localized] forSegmentAtIndex:i];
    }
}


#pragma mark - Overloads

- (void)onUpdateCell
{
    HGIdologyDocTypeFieldItem *item = (HGIdologyDocTypeFieldItem *)self.item;
    
    NSString *docType = item.getHandler();
    NSInteger index = [[HGIdologyDocType allDocTypesIds] indexOfObject:docType];
    
    _docTypeSegments.selectedSegmentIndex = index;
}


- (void)onUpdateError
{
    // nop
}


- (void)setFocusOn
{
    // show 'Required' under Gender when it's Next field
    [self updateCell];
}


- (bool)canSetFocusOn
{
    return NO;
}


#pragma mark - UISegmentedControl actions

- (IBAction)segmentedControlDidChangeValue:(UISegmentedControl *)sender
{
    NSInteger index = sender.selectedSegmentIndex;
    NSString *docType = [HGIdologyDocType allDocTypesIds][index];
    
    // commit updates cell, so after a tap on segment we wait a moment
    const dispatch_time_t delay = 100; // msec
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
        [self commitValue:docType updateCellOnCommit:YES];
    });
}

@end
