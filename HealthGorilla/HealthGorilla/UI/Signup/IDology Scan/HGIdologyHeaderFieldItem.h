//
//  HGIdologyHeaderFieldItem.h
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGSignupAccountFieldItem.h"

@interface HGIdologyHeaderFieldItem : HGSignupAccountFieldItem

@end
