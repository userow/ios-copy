//
//  HGIdologyPhotoTipsFieldCell.h
//  HG-Project.
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGIdologyPhotoTipsFieldCell.h"

@interface HGIdologyPhotoTipsFieldCell ()

@property (weak, nonatomic) IBOutlet UILabel *tipLabel;

@end

@implementation HGIdologyPhotoTipsFieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //done: label localization
    /*
     Photo Tips:
     • Place document on a dark, flat surface in a well-lit room.
     • Position camera directly over document (not angled), avoid glare.
     • Fill screen, but let background surface border all corners of the document.
     • The image should be in sharp focus, not rotated or skewed, and taken in a well-lit room on a camera without flash enabled.
     */
    
    _tipLabel.text =
    [NSString stringWithFormat:
     @"%@\n"
     "• %@\n"
     "• %@\n"
     "• %@\n"
     "• %@",
     @"Photo Tips:".localized,
     @"Place document on a dark, flat surface in a well-lit room.".localized,
     @"Position camera directly over document (not angled), avoid glare.".localized,
     @"Fill screen, but let background surface border all corners of the document.".localized,
     @"The image should be in sharp focus, not rotated or skewed, and taken in a well-lit room on a camera without flash enabled.".localized
     ];
    
    //TODO: highlighting "Photo Tips:"
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}

@end
