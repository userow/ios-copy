//
//  HGIdologyPhotoTipsFieldItem.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGIdologyPhotoTipsFieldItem.h"

@implementation HGIdologyPhotoTipsFieldItem

#pragma mark - Overloads

- (NSString *)cellId
{
    static NSString *cellId = @"HGIdologyPhotoTipsFieldCell";
    return cellId;
}

@end
