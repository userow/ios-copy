//
//  HGIdologyImageTypeFieldCell.m
//  HG-Project
//
//  Created by Paul Vasilenko on 2018-08-11.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "HGIdologyImageFieldCell.h"
#import "HGIdologyImageFieldItem.h"
#import "HGGender.h"


@interface HGIdologyImageFieldCell ()

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIView *docView;
@property (weak, nonatomic) IBOutlet UIImageView *docImageView;
@property (weak, nonatomic) IBOutlet UIView *placeholderView;


@end


@implementation HGIdologyImageFieldCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    _placeholderView.layer.borderColor = [UIColor contactsInstantGreen].CGColor;
    _placeholderView.layer.borderWidth = 1.0f;
    _placeholderView.layer.cornerRadius = 10.0f;
}

#pragma mark - Overloads

- (void)onUpdateCell
{
    //done: convert from PNG base 64 to UIImage, set UIImage, hide placeholderView;
    
    switch ([(HGIdologyImageFieldItem *)self.item imageType]) {
        case HGIdologyImageTypeBack:
            _headerLabel.text = @"Upload image with back of document".localized;
            break;
        default:
            _headerLabel.text = @"Upload image with front of document".localized;
            break;
    }
    
    NSString * imageBase64 =  self.item.getHandler();
    UIImage *image = [UIImage imageFromBase64:imageBase64];
    
    _docImageView.image = image;
    
    //done: hide placeholder, show image and checkmark ?
    if (image) {
        _placeholderView.alpha = 0.0;
        _docView.alpha = 1.0;
        
        NSLog(@"image shown");
    } else {
        _placeholderView.alpha = 1.0;
        _docView.alpha = 0.0;
        
        NSLog(@"placeholder shown");
    }
}


- (void)onUpdateError
{
    // nop
}


- (void)setFocusOn
{
    // show 'Required' under Gender when it's Next field
    [self updateCell];
}


- (bool)canSetFocusOn
{
    return YES;
}


#pragma mark - UISegmentedControl actions

//TODO: ??? move to ViewController && did select cell ???

//- (IBAction)segmentedControlDidChangeValue:(UISegmentedControl *)sender
//{
//    NSInteger index = sender.selectedSegmentIndex;
//    NSString *gender = [HGGender allGenders][index];
//
//    // commit updates cell, so after a tap on segment we wait a moment
//    const dispatch_time_t delay = 100; // msec
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
//        [self commitValue:gender updateCellOnCommit:YES];
//    });
//}

@end
