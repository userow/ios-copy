//
//  HGIdologyPhotoTipsFieldItem.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-11.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGIdologyHeaderFieldItem.h"

@implementation HGIdologyHeaderFieldItem

#pragma mark - Overloads

- (NSString *)cellId
{
    static NSString *cellId = @"HGIdologyHeaderFieldCell";
    return cellId;
}

@end
