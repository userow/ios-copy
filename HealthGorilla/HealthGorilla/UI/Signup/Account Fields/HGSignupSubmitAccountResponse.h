//
//  HGSignupSubmitAccountResponse.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupQuestionItem.h"
#import "HGSignupValidationErrorItem.h"

@interface HGSignupSubmitAccountResponse : JSONModel

@property (nonatomic) bool needAskSSN4;
@property (nonatomic) bool patientRestricted;
@property (nonatomic) bool validationFailed;
@property (nonatomic) bool idologySuccess;
@property (nonatomic) bool idologyScanNeeded;
@property (nonatomic, strong) NSArray<HGSignupQuestionItem> *questions;
@property (nonatomic, strong) NSArray<HGSignupValidationErrorItem> *validationErrors;
@property (nonatomic, strong) NSString *pendingUserId;

@end
