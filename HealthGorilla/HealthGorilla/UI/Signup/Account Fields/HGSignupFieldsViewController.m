//
//  HGSignupFieldsViewController.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupFieldsViewController.h"
#import "HGSignupTextFieldCell.h"
#import "HGSignupFieldFactory.h"
#import "HGSignupValidator.h"
#import "HGSignupAccountFieldItem.h"
#import "HGSignupValidationErrorItem.h"
#import "HGKeyboardObserver.h"
#import "HGHypertextLabel.h"
#import "HGEulaWorkflow.h"


@interface HGSignupFieldsViewController ()

@property (nonatomic, copy) HGSignupFieldsCompletion completion;
//
@property (nonatomic, strong) HGKeyboardObserver *keyboardObserver;
//
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableBottomConstraint;
@property (nonatomic, weak) IBOutlet HGHypertextLabel *agreementLabel;
@property (nonatomic, weak) IBOutlet UILabel *failedAndStartedOverLabel;

@end


@implementation HGSignupFieldsViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupFieldsCompletion)completion
{
    HGSignupFieldsViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    __weak typeof(self) weakSelf = self;
    _keyboardObserver = [[HGKeyboardObserver alloc] init];
    _keyboardObserver.preTransitionHandler = ^(CGRect kbBeginFrame, CGRect kbEndFrame, bool show) {
        [weakSelf onPreTransitionKeyboardFromRect:kbBeginFrame toRect:kbEndFrame show:show];
    };
    _keyboardObserver.transitionHandler = ^(CGRect kbBeginFrame, CGRect kbEndFrame, bool show) {
        [weakSelf onTransitionKeyboardFromRect:kbBeginFrame toRect:kbEndFrame show:show];
    };
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 120;
    
    [self buildCancelButton];
    [self buildFields];
    [self buildFooter];
    [self validateNextButton];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // update header to reflect to 'Failed And Started Over' state
    [self updateHeader];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    _keyboardObserver.subscribed = YES;
    
    HGSignupAccountFieldCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell setFocusOn];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //
    _keyboardObserver.subscribed = NO;
}


#pragma mark - Keyboard

- (void)onPreTransitionKeyboardFromRect:(CGRect)fromRect toRect:(CGRect)toRect show:(bool)show
{
    _tableBottomConstraint.constant = show ? CGRectGetHeight(toRect) : 0;
}


- (void)onTransitionKeyboardFromRect:(CGRect)fromRect toRect:(CGRect)toRect show:(bool)show
{
    [self.view layoutIfNeeded];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGSignupAccountFieldItem *item = self.items[indexPath.row];
    HGSignupTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:item.cellId];
    
    __weak typeof(self) weakSelf = self;
    [cell updateWithItem:item updateCellHandler:^{
        const dispatch_time_t delay = 100; // msec
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
            [weakSelf didUpdateValueOnItem:item indexPath:indexPath];
        });
    } validateFormHandler:^{
        [weakSelf validateFormOnItem:item];
    } nextFieldHandler:^{
        [weakSelf activateFieldAfter:item];
    }];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    HGSignupAccountFieldItem *item = self.items[indexPath.row];
    [self activateField:item];
}


#pragma mark - Methods

- (void)validateNextButton
{
    bool valid = YES;
    for (HGSignupAccountFieldItem *item in self.items) {
        valid &= item.isValid;
    }
    [self setNextButtonEnabled:valid];
}


- (void)setNextButtonEnabled:(bool)enabled
{
    self.navigationItem.rightBarButtonItem.enabled = enabled;
}


- (void)tapOnBack // overwritten
{
    // Stage One: 'Back' for Clinician only
    // Stage Two: all roles
    
    // hide keyboard to call commit from active text field to context (to avoid commit after the clearing)
    [self hideKeyboard];
    
    // clear fields while tapping 'Back' on 1st stage only
    if ([self shouldClearOnBackstep]) {
        [self.context clearAllFields];
    }
}


- (void)tapOnCancel
{
    // Stage One: 'Cancel' for Patient, Staff
    // Stage Two: unexpected case
    
    // hide keyboard to call commit from active text field to context (to avoid commit after the clearing)
    [self hideKeyboard];
    
    bool cancelled = YES;
    self.completion(cancelled, nil);
}


- (IBAction)tapOnNext:(id)sender
{
    // hide keyboard to call commit from active text field to context
    [self hideKeyboard];
    
    __weak typeof(self) weakSelf = self;
    
    [self setNextButtonEnabled:NO];
    [self launchAllRemoteValidatorsWithCompletion:^(bool success) {
        [self setNextButtonEnabled:YES];
        if (success) {
            [weakSelf performNextStep];
        }
    }];
}


- (void)launchAllRemoteValidatorsWithCompletion:(void (^)(bool success))completion
{
    NSArray<HGSignupAccountFieldItem *> *items = self.items;
    
    // run remote validators on background queue
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        __block bool serverDetectsError = NO;
        
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        for (HGSignupAccountFieldItem *item in items) {
            if (!item.remoteValidateHandler || [item isKindOfClass:[HGIdologyImageFieldItem class]]) {
                continue; // item has no remote validation handler OR item with image uploading through Validator
            }
            NSString *value = item.getHandler();
            if (!value.hasValue) {
                continue; // skip nils, e.g. email for MD/Staff is optional and can be empty
            }
            item.remoteValidateHandler(value, ^(NSString *message) {
                if (message.hasValue) {
                    serverDetectsError = YES;
                }
                // notify that task is complete
                dispatch_semaphore_signal(semaphore);
            });
            // wait for remote tasks is complete
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }

        // notify that all remote validators are finished
        dispatch_sync(dispatch_get_main_queue(), ^{
            completion(!serverDetectsError);
        });
    });
}


- (void)performNextStep
{
    __weak typeof(self) weakSelf = self;
    HGSignupFieldsValidationCallback validationCallback = ^(NSArray<HGSignupValidationErrorItem *> *validationErrors) {
        [weakSelf applyValidationErrors:validationErrors];
    };
    
    bool cancelled = NO;
    self.completion(cancelled, validationCallback);
}


- (void)applyValidationErrors:(NSArray<HGSignupValidationErrorItem *> *)validationErrors
{
    NSMutableString *unfitMessage = [[NSMutableString alloc] init];
    
    for (HGSignupValidationErrorItem *error in validationErrors) {
        HGSignupAccountFieldItem *item = [self itemWithErrorId:error.identifier];
        if (item) {
            item.error = error.message;
        } else {
            if (unfitMessage.hasValue) {
                [unfitMessage appendString:@" "];
            }
            [unfitMessage appendString:error.message];
        }
    }
    
    if (unfitMessage.hasValue) {
        PSPDFAlertView *alert = [HGAlertView alertViewWithTitle:@"Error".localized andMessage:unfitMessage];
        [alert setCancelButtonWithTitle:@"OK".localized block:nil];
        [alert show];
    }
}


- (HGSignupAccountFieldItem *)itemWithErrorId:(NSString *)errorId
{
    for (HGSignupAccountFieldItem *item in self.items) {
        if ([item.errorId isEqual:errorId]) {
            return item;
        }
    }
    return nil;
}


- (void)buildCancelButton
{
    if ([self canShowBackButtonWithUserType:self.context.userType]) {
        return; // show 'Back' button
    }
    __weak typeof(self) weakSelf = self;
    self.navigationItem.leftBarButtonItem = [HGBarButtonFactory cancelButtonWithAction:^(id sender) {
        [weakSelf tapOnCancel];
    }];
}


- (void)buildFields
{
    self.items = [HGSignupFieldFactory fieldsForStage:[self signupStage] context:self.context];
}


- (void)buildFooter
{
    NSString *text = [NSString stringWithFormat:@"By taping Next, you confirm that you have read and agreed to <0:%@ Terms>".localized, [HGAppConfig appNameWhitelabeled]];
    
    __weak typeof(self) weakSelf = self;
    
    _agreementLabel.hypertext = text;
    _agreementLabel.linkTapHandler = ^(NSUInteger linkId) {
        [weakSelf presentEula];
    };
}


- (void)updateHeader
{
    if (!_failedAndStartedOverLabel) {
        return; // Stage Two has no 'Failed' header
    }
    CGFloat headerHeight;
    if (self.context.failedAndStartedOver) {
        _failedAndStartedOverLabel.hidden = NO;
        headerHeight = 70;
    } else {
        _failedAndStartedOverLabel.hidden = YES;
        headerHeight = 0.01; // zero resets height to default value
    }
    CGRect rect = self.tableView.tableHeaderView.frame;
    if (headerHeight != rect.size.height) {
        rect.size.height = headerHeight;
        self.tableView.tableHeaderView.frame = rect;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [UIView beginAnimations:@"tableHeaderView" context:nil];
            self.tableView.tableHeaderView = self.tableView.tableHeaderView;
            [UIView commitAnimations];
        }];
    }
}


- (void)presentEula
{
    [self hideKeyboard];
    
    HGEulaLocator *locator = nil;
    switch (self.context.userType) {
        case HGSignupUserPatient:
            locator = [HGUserProxy eulaLocatorForPatient];
            break;
        case HGSignupUserClinician:
        case HGSignupUserStaff:
            locator = [HGUserProxy eulaLocatorForDoctor];
            break;
        default:
            break;
    }
    if (locator) {
        [HGEulaWorkflow startWithLocator:locator navigation:self.navigationController];
    }
}


- (void)hideKeyboard
{
    [self.view endEditing:YES];
}


- (void)reloadCellAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}


- (void)activateFieldAfter:(HGSignupAccountFieldItem *)after
{
    HGSignupAccountFieldItem *next = [self itemNextAfter:after];
    if (!next) {
        [self hideKeyboard];
        return; // no next item
    }
    [self activateField:next];
}


- (void)activateField:(HGSignupAccountFieldItem *)item
{
    NSInteger rowIndex = [self.items indexOfObject:item];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];
    HGSignupAccountFieldCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    
    [cell setFocusOn]; // call it always rather of canSetFocusOn
    
    if (!cell.canSetFocusOn) {
        [self hideKeyboard];
        return; // the next item is not a text field
    }
    //[_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


- (HGSignupAccountFieldItem *)itemNextAfter:(HGSignupAccountFieldItem *)after
{
    __block HGSignupAccountFieldItem *current = nil;
    __block HGSignupAccountFieldItem *next = nil;
    [self.items enumerateObjectsUsingBlock:^(HGSignupAccountFieldItem * item, NSUInteger index, BOOL *stop) {
        if (current) {
            next = item;
            *stop = YES;
            return;
        }
        if (after.identifier == item.identifier) {
            current = item;
        }
    }];
    return next;
}


- (void)validateFormOnItem:(HGSignupAccountFieldItem *)item
{
    // hide keyboard to call commit from active text field to context. See IOS-657
    if (HGSignupFieldGender == item.identifier) {
        [self hideKeyboard];
    }
    [self validateNextButton];
}


- (void)didUpdateValueOnItem:(HGSignupAccountFieldItem *)item indexPath:(NSIndexPath *)indexPath
{
    // validate and update error text
    NSString *value = item.getHandler();
    
    if (item.localValidateHandler) {
        item.error = item.localValidateHandler(value);
    }
    
    // run remote validation if available
    bool remoteValidationLaunched = [self asyncRemoteValidationWithItem:item indexPath:indexPath];
    
    // reload cell before remote validation or after, not both
    if (!remoteValidationLaunched) {
        [self reloadCellAtIndexPath:indexPath];
    }
}


- (bool)asyncRemoteValidationWithItem:(HGSignupAccountFieldItem *)item indexPath:(NSIndexPath *)indexPath
{
    NSString *value = item.getHandler();
    if (!value.hasValue) {
        item.invalidatedByServer = NO; // reset remote validation flag
        return NO; // empty
    }
    __weak typeof(self) weakSelf = self;
    void (^remoteValidationCallback)(NSString *) = ^(NSString *message) {
        item.invalidatedByServer = message.hasValue;
        if (message.hasValue) {
            item.error = message;
        }
        [weakSelf reloadCellAtIndexPath:indexPath];
        [weakSelf validateNextButton];
    };
    if (item.remoteValidateHandler) {
        item.remoteValidateHandler(value, ^(NSString *message) {
            remoteValidationCallback(message);
        });
        return YES; // remote validation launched
    }
    return NO;
}


#pragma mark - Overloaded in subclasses

- (HGSignupStage)signupStage
{
    return HGSignupStageUndefined;
}


- (bool)canShowBackButtonWithUserType:(HGSignupUserType)userType
{
    return NO;
}


- (bool)shouldClearOnBackstep
{
    return NO;
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
