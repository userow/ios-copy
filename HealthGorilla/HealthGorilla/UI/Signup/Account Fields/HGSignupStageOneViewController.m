//
//  HGSignupStageOneViewController.m
//  HG-Project
//
//  Created by not Paul on 28/11/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupStageOneViewController.h"


@implementation HGSignupStageOneViewController

- (HGSignupStage)signupStage // overloaded
{
    return HGSignupStageOne;
}

- (bool)canShowBackButtonWithUserType:(HGSignupUserType)userType // overloaded
{
    return (HGSignupUserClinician == userType); // Back - for Clinician, Cancel - for Patient/Staff
}

- (bool)shouldClearOnBackstep
{
    return YES;
}

- (IBAction)unwindPatientOrStaffToSignupStart:(UIStoryboardSegue *)unwindSegue
{
    // way to unwind to 1st screen (stage One only)
}

@end
