//
//  HGPerformIdScanActionResponse.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-25.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGPerformIdScanActionResponse.h"

@implementation HGPerformIdScanActionResponse

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@ {
        @"idscanSuccess"    : @"idscanSuccess",
        @"errorMessage"     : @"errorMessage"
    }];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

@end
