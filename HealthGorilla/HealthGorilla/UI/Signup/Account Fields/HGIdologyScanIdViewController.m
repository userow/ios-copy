//
//  HGIdologyScanIdViewController.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-08.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGIdologyScanIdViewController.h"


#import "HGIdologyImageFieldCell.h"
#import "HGIdologyImageFieldItem.h"

#import "HGIdologyDocTypeFieldCell.h"

#import "HGSignupTextFieldCell.h"

#import "HGSignupContext.h"

#import "HGSignupFieldFactory.h"

#import "HGImagePicker.h"

#import "UIImagePickerController+LayoutFlag.h"

@implementation HGIdologyScanIdViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.contentInset = UIEdgeInsetsZero;
    
    //disabling empty cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView.backgroundColor = [UIColor blueColor];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableHeaderView.backgroundColor = [UIColor purpleColor];
}

#pragma mark - HGSignupFieldsViewController methods overriding

- (HGSignupStage)signupStage // overloaded
{
    return HGSignupStageIdology;
}

- (bool)canShowBackButtonWithUserType:(HGSignupUserType)userType // overloaded
{
    return YES; // always 'Back' button on stage Two    
}

- (bool)shouldClearOnBackstep
{
    return NO;
}

#pragma mark - TableView Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    super tableView
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[HGIdologyImageFieldCell class]]) {
        HGIdologyImageFieldCell *imageCell = (HGIdologyImageFieldCell *)cell;
        
        NSString *itemValue = imageCell.item.getHandler();
        
        [self changeImageWithTarget:[(HGIdologyImageFieldItem *)imageCell.item imageType]
                        atIndexPath:indexPath
                          canRemove:itemValue.hasValue];
    }
}

#pragma mark - ImagePicker Methods

- (void)changeImageWithTarget:(HGIdologyImageType)type
                  atIndexPath:(NSIndexPath *)indexPath
                    canRemove:(BOOL)canRemove;
{
    NSInteger removePhotoIndex = -1;
    NSInteger takePhotoIndex = -1;
    NSInteger chooseImageIndex = -1;
    
    NSInteger chooseDocumentVerifiedIndex = -1;
    NSInteger chooseIdNotReadableIndex = -1;
    NSInteger chooseDocumentNotVerifiedIndex = -1;
    
    
    NSMutableArray *buttons = [[NSMutableArray alloc] init];
    
    //done: removing of a photo should be optional
    if (canRemove) {
        removePhotoIndex = buttons.count;
        [buttons addObject:@"Remove Current Photo".localized];
    }
    
    if ([HGImagePicker isCameraSourceAvailable]) {
        takePhotoIndex = buttons.count;
        [buttons addObject:@"Take Photo".localized];
    }
    
    if ([HGImagePicker isPhotoLibrarySourceAvailable]) {
        chooseImageIndex = buttons.count;
        [buttons addObject:@"Choose from Library".localized];
    }
    
    if ([HGAppConfig isQAbuild]) {
        chooseDocumentVerifiedIndex = buttons.count;
        [buttons addObject:@"[QA ONLY!] Completed"];
        
        chooseIdNotReadableIndex = buttons.count;
        [buttons addObject:@"[QA ONLY!] Image Error"];
        
        chooseDocumentNotVerifiedIndex = buttons.count;
        [buttons addObject:@"[QA ONLY!] Internal Error"];
    }
    
    //done: Rename
    NSString *title = @"Document Photo".localized;
    [UIActionSheet presentOnView:self.view
                          sender:self
                       withTitle:title
                    otherButtons:buttons onCancel:^(UIActionSheet *sheet) {
        // cancel
    } onClickedButton:^(UIActionSheet *sheet, NSUInteger index) {
        
        if (removePhotoIndex == index) {
            [self removePhotoAtIndex:indexPath];
            [self changePhoto:nil
                  atIndexPath:indexPath];
        }
        else if (takePhotoIndex == index) {
            [self pickImageFromSource:UIImagePickerControllerSourceTypeCamera
                          atIndexPath:(NSIndexPath *)indexPath];
        }
        else if (chooseImageIndex == index) {
            [self pickImageFromSource:UIImagePickerControllerSourceTypePhotoLibrary
                          atIndexPath:(NSIndexPath *)indexPath];
        }
        else if (chooseDocumentVerifiedIndex == index) {
            [self changePhotoType:HGIdologyPhotoTypeCompleted
                      atIndexPath:indexPath];
        }
        else if (chooseIdNotReadableIndex == index) {
            [self changePhotoType:HGIdologyPhotoTypeImageError
                      atIndexPath:indexPath];
        }
        else if (chooseDocumentNotVerifiedIndex == index) {
            [self changePhotoType:HGIdologyPhotoTypeInternalError
                      atIndexPath:indexPath];
        }
    }];
}


- (void)pickImageFromSource:(UIImagePickerControllerSourceType)source
                atIndexPath:(NSIndexPath *)indexPath
{
    [HGImagePicker pickWithPresenter:self
                        configurator:^(UIImagePickerController *picker) {
        picker.sourceType = source;
        picker.allowsEditing = NO;
        picker.addFrameLayout = YES;
    } completion:^(UIImage *image) {
        [self changePhoto:image
              atIndexPath:indexPath];
    }];
}


- (void)changePhoto:(UIImage *)photo
        atIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[HGIdologyImageFieldCell class]]) {
        HGIdologyImageFieldCell *imageCell = (HGIdologyImageFieldCell *)cell;
        
//        HGIdologyImageFieldItem *item = (HGIdologyImageFieldItem *)imageCell.item;

//        if (item.imageType == HGIdologyImageTypeFront) {
//            self.context.scanFrontImage = photo;
//        } else if (item.imageType == HGIdologyImageTypeBack) {
//            self.context.scanBackImage = photo;
//        }

//        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

        NSString *photoBase64 = photo.base64StringFromImage;
        
        // commit updates cell, so after an image selection we wait a moment
        const dispatch_time_t delay = 100; // msec
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
            [imageCell commitValue:photoBase64 updateCellOnCommit:YES];
        });
        
    }
}

- (void)changePhotoType:(HGIdologyPhotoType)type
            atIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[HGIdologyImageFieldCell class]]) {
        HGIdologyImageFieldCell *imageCell = (HGIdologyImageFieldCell *)cell;
        
        NSString *photoBase64 = [HGAppConfig testPhotoBase64StringWithType:type];
        
        // commit updates cell, so after an image selection we wait a moment
        const dispatch_time_t delay = 100; // msec
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
            [imageCell commitValue:photoBase64 updateCellOnCommit:YES];
        });
    }
}

- (void)removePhotoAtIndex:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[HGIdologyImageFieldCell class]]) {
        HGIdologyImageFieldCell *imageCell = (HGIdologyImageFieldCell *)cell;
        
        HGIdologyImageFieldItem *imageItem = (HGIdologyImageFieldItem *)imageCell.item;
        
        HGIdologyImageType type = imageItem.imageType;
        
        NSString *externalId = type == HGIdologyImageTypeBack ? super.context.scanBackExternalId : super.context.scanFrontExternalId;
        
        [[HGApiClient shared] removeImageWithExternalId:externalId
                                          pendingUserId:super.context.pendingUserId
                                              imageType:type];
    }
}

- (void)didUpdateValueOnItem:(HGSignupAccountFieldItem *)item indexPath:(NSIndexPath *)indexPath {
    [super didUpdateValueOnItem:(HGSignupAccountFieldItem *)item indexPath:(NSIndexPath *)indexPath];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[HGIdologyDocTypeFieldCell class]]) {
        NSString *docType = item.getHandler();
        
        //Search item array for ImageFieldItem
        NSMutableArray<HGSignupAccountFieldItem *> *dataItems = [super.items mutableCopy];
        
        HGIdologyImageFieldItem *imageItem;
        
        for (HGSignupAccountFieldItem *object in dataItems) {
            if ([object isKindOfClass:[HGIdologyImageFieldItem class]]) {
                imageItem = (HGIdologyImageFieldItem *)object;
                
                if (imageItem.imageType != HGIdologyImageTypeBack) {
                    imageItem = nil;
                } else {
                    break;
                }
            }
        }
        
        if ([docType isEqualToString:@"passport"]) { //docType is passport - remove item from array
            
            
            //if there IS imageItem -
            if (imageItem) {
                //0) index of item
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[super.items indexOfObject:imageItem] inSection:0];
                
                [self removePhotoAtIndex:indexPath];
                                          
                //1) remove item from array
                [dataItems removeObject:imageItem];
                super.items = [dataItems copy];
                
                //2) remove value from context
                super.context.scanBackBase64 = nil;
            
                //3) reload table
                [super.tableView reloadData];
            }
        }
        else { //the docType != passport  -  if there is no imageBack item - we should add one;
            if (!imageItem) {
                super.items = [HGSignupFieldFactory fieldsForStage:HGSignupStageIdology context:super.context];
                
                [super.tableView reloadData];
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HGSignupAccountFieldItem *item = super.items[indexPath.row];
    HGSignupTextFieldCell *cell = [super.tableView dequeueReusableCellWithIdentifier:item.cellId];
    
    __weak typeof(self) weakSelf = self;
    [cell updateWithItem:item updateCellHandler:^{
        const dispatch_time_t delay = 100; // msec
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
            [weakSelf didUpdateValueOnItem:item indexPath:indexPath];
        });
    } validateFormHandler:^{
        [weakSelf validateFormOnItem:item];
    } nextFieldHandler:^{
        [weakSelf activateFieldAfter:item];
    }];
    
    return cell;
}

- (void)validateFormOnItem:(HGSignupAccountFieldItem *)item {
    [super validateFormOnItem:item];
}

- (void)activateFieldAfter:(HGSignupAccountFieldItem *)after {
    [super activateFieldAfter:after];
}

@end
