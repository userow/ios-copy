//
//  HGSignupValidationErrorItem.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

extern const struct HGSignupValidationErrors {
    __unsafe_unretained NSString *email;
    __unsafe_unretained NSString *username;
    __unsafe_unretained NSString *password;
    __unsafe_unretained NSString *zip;
    __unsafe_unretained NSString *dob;
    __unsafe_unretained NSString *captcha;
} HGSignupValidationErrors;


@protocol HGSignupValidationErrorItem <NSObject>
// to support array in JSONModel
@end


@interface HGSignupValidationErrorItem : JSONModel

@property (nonatomic, strong) NSString *identifier;  // HGSignupValidationFields
@property (nonatomic, strong) NSString *message;

@end
