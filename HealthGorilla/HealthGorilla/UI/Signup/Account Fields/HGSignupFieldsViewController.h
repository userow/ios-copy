//
//  HGSignupFieldsViewController.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"

@class HGSignupValidationErrorItem;

typedef void (^HGSignupFieldsValidationCallback)(NSArray<HGSignupValidationErrorItem *> *validationErrors);
typedef void (^HGSignupFieldsCompletion)(bool cancelled, HGSignupFieldsValidationCallback validationCallback);

@interface HGSignupFieldsViewController : UIViewController


@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

+ (instancetype)instanceWithContext:(HGSignupContext *)context
                         storyboard:(UIStoryboard *)storyboard
                         completion:(HGSignupFieldsCompletion)completion;

//IOS-923 - disable back side for passport

@property (nonatomic, strong) NSArray<HGSignupAccountFieldItem *> *items;

- (void)didUpdateValueOnItem:(HGSignupAccountFieldItem *)item indexPath:(NSIndexPath *)indexPath;
- (void)validateFormOnItem:(HGSignupAccountFieldItem *)item;
- (void)activateFieldAfter:(HGSignupAccountFieldItem *)after;

@end
