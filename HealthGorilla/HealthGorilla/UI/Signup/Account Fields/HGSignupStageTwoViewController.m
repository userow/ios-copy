//
//  HGSignupStageTwoViewController.m
//  HG-Project
//
//  Created by not Paul on 28/11/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupStageTwoViewController.h"


@implementation HGSignupStageTwoViewController

- (HGSignupStage)signupStage // overloaded
{
    return HGSignupStageTwo;
}

- (bool)canShowBackButtonWithUserType:(HGSignupUserType)userType // overloaded
{
    return YES; // always 'Back' button on stage Two
}

- (bool)shouldClearOnBackstep
{
    return NO;
}

//- (IBAction)unwindPatientOrStaffToSignupStart:(UIStoryboardSegue *)unwindSegue
//{
//    // way to unwind to 1st screen (stage One only)
//}

@end
