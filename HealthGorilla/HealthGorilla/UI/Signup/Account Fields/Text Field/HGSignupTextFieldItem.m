//
//  HGSignupTextFieldItem.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupTextFieldItem.h"


@implementation HGSignupTextFieldItem

- (instancetype)initWithId:(HGSignupFieldID)identifier keyboardType:(UIKeyboardType)keyboardType capitalization:(bool)capitalization secure:(bool)secure title:(NSString *)title placeholder:(NSString *)placeholder info:(NSString *)info errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler
{
    self = [super initWithId:identifier errorId:errorId getHandler:getHandler commitHandler:commitHandler localValidateHandler:localValidateHandler remoteValidateHandler:remoteValidateHandler];
    if (!self) return nil;
    
    _keyboardType = keyboardType;
    _secure = secure;
    _autocapitalizationType = capitalization ? UITextAutocapitalizationTypeWords : UITextAutocapitalizationTypeNone;
    _title = title;
    _placeholder = placeholder;
    _info = info;

    return self;
}


+ (instancetype)itemWithId:(HGSignupFieldID)identifier keyboardType:(UIKeyboardType)keyboardType capitalization:(bool)capitalization secure:(bool)secure title:(NSString *)title placeholder:(NSString *)placeholder info:(NSString *)info errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler
{
    return [[self alloc] initWithId:identifier keyboardType:keyboardType capitalization:capitalization secure:secure title:title placeholder:placeholder info:info errorId:errorId getHandler:getHandler commitHandler:commitHandler localValidateHandler:localValidateHandler remoteValidateHandler:remoteValidateHandler];
}


#pragma mark - Overloads

- (NSString *)cellId
{
    static NSString *cellId = @"TextFieldCell";
    return cellId;
}

@end
