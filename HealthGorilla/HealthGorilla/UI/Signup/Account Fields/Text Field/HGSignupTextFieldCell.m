//
//  HGSignupTextFieldCell.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupTextFieldCell.h"
#import "HGSignupTextFieldItem.h"


@interface HGSignupTextFieldCell () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *infoTopConstraint;

@end


@implementation HGSignupTextFieldCell

#pragma mark - Overloads

- (void)onUpdateCell
{
    HGSignupTextFieldItem *item = (HGSignupTextFieldItem *)self.item;
    
    _textField.text = item.getHandler();
    _textField.placeholder = item.placeholder;
    _textField.secureTextEntry = item.secure;
    _textField.keyboardType = item.keyboardType;
    _textField.autocapitalizationType = item.autocapitalizationType;
    _textField.inputAccessoryView = [self keyboardAccessoryView];
    _infoLabel.text = item.info;
    _titleLabel.text = item.title;
}


- (void)onUpdateError
{
    HGSignupTextFieldItem *item = (HGSignupTextFieldItem *)self.item;
    _infoTopConstraint.constant = (item.error.hasValue && item.info.hasValue) ? 3 : 0;
}


- (void)setFocusOn
{
    [_textField becomeFirstResponder];
}


- (bool)canSetFocusOn
{
    return YES;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self commitValue:textField.text updateCellOnCommit:YES];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self commitValue:str updateCellOnCommit:NO];
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self commitValue:nil updateCellOnCommit:NO];
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
