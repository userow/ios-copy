//
//  HGSignupTextFieldItem.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupAccountFieldItem.h"

@interface HGSignupTextFieldItem : HGSignupAccountFieldItem

@property (nonatomic) UIKeyboardType keyboardType;
@property (nonatomic) UITextAutocapitalizationType autocapitalizationType;
@property (nonatomic) bool secure;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) NSString *info;

+ (instancetype)itemWithId:(HGSignupFieldID)identifier keyboardType:(UIKeyboardType)keyboardType capitalization:(bool)capitalization secure:(bool)secure title:(NSString *)title placeholder:(NSString *)placeholder info:(NSString *)info errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler;

@end
