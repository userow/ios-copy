//
//  HGPerformIdScanPollingResultsActionResponse.m
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-31.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

#import "HGPerformIdScanPollingResultsActionResponse.h"

@implementation HGPerformIdScanPollingResultsActionResponse

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@ {
        @"idologySuccess" : @"idologySuccess",
        @"idscanPending" : @"idscanPending",
        @"idscanObsolete" : @"idscanObsolete",
        @"emailConfirmNeeded" : @"emailConfirmNeeded",
        @"email" : @"email",
    }];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

@end
