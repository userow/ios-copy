//
//  HGSignupValidator.m
//  HG-Project
//
//  Created by not Paul on 05/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupValidator.h"
#import "HGSignupContext.h"


@implementation HGSignupValidator

+ (NSString *)errorTextForEmail:(NSString *)email optional:(bool)optional
{
    if (email.hasValue && !email.isValidEmail) {
        return @"Invalid email format".localized;
    } else if(!email.hasValue && !optional) {
        return @"Required".localized;
    }
    return nil;
}


+ (NSString *)errorTextForUsername:(NSString *)username
{
    if (!username.hasValue) {
        return @"Required".localized;
    }
    if (username.length < 3 || username.length > 16) {
        return @"Length should be 3 to 16 symbols and include low-case characters, digits, hyphen, underscore, @ or dot symbols.".localized;
    }
    return nil;
}


+ (NSString *)errorTextForName:(NSString *)name
{
    if (!name.hasValue) {
        return @"Required".localized;
    }
    return nil;
}


+ (NSString *)errorTextForPassword:(NSString *)password
{
    if (!password.hasValue) {
        return @"Required".localized;
    }
    if (!password.isValidPassword) {
        return @"Password contains the characters that are not allowed".localized;
    }
    if (!password.isStrongPassword) {
        return @"Password too weak".localized;
    }
    return nil;
}


+ (NSString *)errorTextForZip:(NSString *)zip
{
    if (!zip.hasValue) {
        return @"Required".localized;
    }
    if (zip.length != 5 || !zip.containsDigitsOnly) {
        return @"Must have 5 digits only".localized;
    }
    return nil;
}


+ (NSString *)errorTextForDOB:(NSString *)dob
{
    HGDate *dobAsDate = [HGDate dobWithString:dob];
    NSDate *dobAsNSDate = [dobAsDate date];
    if ([dobAsNSDate compare:[NSDate date]] == NSOrderedDescending) {
        return @"DOB should be chosen in the past".localized;
    }
    if (!dob.hasValue) {
        return @"Required".localized;
    }
    return nil;
}


+ (NSString *)errorTextForGender:(NSString *)gender
{
    if (!gender.hasValue) {
        return @"Required".localized;
    }
    return nil;
}

#pragma mark - IdologyScanID

+ (NSString *)errorTextForIdologyDocumentType:(NSString *)docType;
{
    if (!docType.hasValue) {
        return @"Required".localized;
    }
    return nil;
}


+ (NSString *)errorTextForIdologyImage:(NSString *)imageBase64;
{
    if (!imageBase64.hasValue) {
        return @"Required".localized;
    }
        
    return nil;
}

@end
