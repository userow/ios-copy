//
//  HGSignupFieldFactory.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupFieldFactory.h"

#import "HGSignupContext.h"

#import "HGSignupValidator.h"
#import "HGSignupValidationErrorItem.h"

#import "HGSignupTextFieldItem.h"

#import "HGSignupGenderFieldItem.h"
#import "HGSignupDOBFieldItem.h"
#import "HGSignupZipFieldItem.h"

#import "HGIdologyHeaderFieldItem.h"
#import "HGIdologyDocTypeFieldItem.h"
#import "HGIdologyImageFieldItem.h"
#import "HGIdologyPhotoTipsFieldItem.h"

#import "HGApiClient+Signup.h"

#import "NSData+Base64.h"

#import "HGExternalAttachment.h"

@implementation HGSignupFieldFactory

+ (NSArray<HGSignupAccountFieldItem *> *)fieldsForStage:(HGSignupStage)stage context:(HGSignupContext *)context
{
    NSArray<NSNumber *> *sequence = nil;
    switch (context.userType) {
        case HGSignupUserClinician:
            if (HGSignupStageOne == stage) {
                sequence = @
                [
                 @(HGSignupFieldUsername),
                 @(HGSignupFieldPassword),
                 @(HGSignupFieldEmail),
                 ];
            } else if (HGSignupStageTwo == stage) {
                sequence = @
                [
                 @(HGSignupFieldDOB),
                 @(HGSignupFieldZip),
                 ];
            }
            break;
        case HGSignupUserStaff:
            if (HGSignupStageOne == stage) {
                sequence = @
                [
                 @(HGSignupFieldFirstName),
                 @(HGSignupFieldMiddleName),
                 @(HGSignupFieldLastName),
                 @(HGSignupFieldPracticeName),
                 @(HGSignupFieldUsername),
                 @(HGSignupFieldPassword),
                 @(HGSignupFieldEmail),
                 ];
            } else if (HGSignupStageTwo == stage) {
                sequence = @
                [
                 @(HGSignupFieldDOB),
                 @(HGSignupFieldZip),
                 ];
            }
            break;
        case HGSignupUserPatient: {
            
#warning REMOVE BEFORE RELEASE !!!
            if ([HGAppConfig isQAbuild] && [HGAppConfig isAngelesClinical] && [HGAppConfig isDebug] && !context.firstName.hasValue) {
                context.firstName = @"Pablo";
                context.middleName = @"Escobar";
                context.lastName = @"Vasilenko";
                context.email = @"userow@gmail.com";
                context.password = @"Linavm31415";
                context.dob = @"1981 01 25";
                context.gender = @"male";
                context.zip = @"12345";
                context.documentType = @"driverLicense";
                context.refreshRequest = @NO;
            }
            
            if (HGSignupStageOne == stage) {
                sequence = @
                [
                 @(HGSignupFieldFirstName),
                 @(HGSignupFieldMiddleName),
                 @(HGSignupFieldLastName),
                 @(HGSignupFieldEmail),
                 @(HGSignupFieldPassword),
                 ];
            } else if (HGSignupStageTwo == stage) {
                sequence = @
                [
                 @(HGSignupFieldDOB),
                 @(HGSignupFieldZip),
                 @(HGSignupFieldGender),
                 ];
            } else if (HGSignupStageIdology) {
                sequence = @
                [
                 @(HGIdologyHeaderField),
                 @(HGIdologyDocTypeField),
                 @(HGIdologyImageFrontField),
                 @(HGIdologyImageBackField),
                 @(HGIdologyPhotoTipsField),
//                 @(HGIdologyNeedHelpField),
                ];
            }
            break;
        }
        default:
            break;
    }
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    for (NSNumber *identifier in sequence) {
        [items addObject:({
            [self itemWithId:identifier.integerValue context:context];
        })];
    }
    return [items copy]; // immutable
}


+ (HGSignupAccountFieldItem *)itemWithId:(HGSignupFieldID)identifier
                                 context:(HGSignupContext *)context
{
    switch (identifier) {
        case HGSignupFieldEmail: {
            bool isPatient = (HGSignupUserPatient == context.userType);
            bool optionalEmail = !isPatient;
            NSString *placeholder = nil, *info = nil;
            if (isPatient) {
                placeholder = @"doe@domain.com".localized;
                info = @"Your email will be used as your Username".localized;
            } else {
                placeholder = @"doe@domain.com".localized;
                info = nil;
            }
            return [HGSignupTextFieldItem itemWithId:identifier
                                        keyboardType:UIKeyboardTypeEmailAddress
                                      capitalization:NO
                                              secure:NO
                                               title:@"Email".localized
                                         placeholder:placeholder
                                                info:info
                                             errorId:HGSignupValidationErrors.email
                                          getHandler:^NSString *{
                                              return context.email;
                                          } commitHandler:^(NSString *value) {
                                              context.email = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForEmail:value optional:optionalEmail];
                                          } remoteValidateHandler:^(NSString *value, HGRemoteValidateCallback callback) {
                                              // async remote validation
                                              [[HGApiClient shared] checkInUseEmail:value handler:^(bool success, NSString *message, NSError *error) {
                                                  callback(message);
                                              }];
                                          }];
        }
        case HGSignupFieldUsername: {
            return [HGSignupTextFieldItem itemWithId:identifier
                                        keyboardType:UIKeyboardTypeASCIICapable
                                      capitalization:NO
                                              secure:NO
                                               title:@"Username".localized
                                         placeholder:@"johndoe".localized
                                                info:nil
                                             errorId:HGSignupValidationErrors.username
                                          getHandler:^NSString *{
                                              return context.username;
                                          } commitHandler:^(NSString *value) {
                                              context.username = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForUsername:value];
                                          } remoteValidateHandler:^(NSString *value, HGRemoteValidateCallback callback) {
                                              // async remote validation
                                              [[HGApiClient shared] checkInUseUsername:value handler:^(bool success, NSString *message, NSError *error) {
                                                  callback(message);
                                              }];
                                          }];
        }
        case HGSignupFieldPassword: {
            return [HGSignupTextFieldItem itemWithId:identifier
                                        keyboardType:UIKeyboardTypeASCIICapable
                                      capitalization:NO
                                              secure:YES
                                               title:@"Password".localized
                                         placeholder:@"Password".localized
                                                info:@"Eight or more characters, upper and lowercase letters, at least one digit.".localized
                                             errorId:HGSignupValidationErrors.password
                                          getHandler:^NSString *{
                                              return context.password;
                                          } commitHandler:^(NSString *value) {
                                              context.password = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForPassword:value];
                                          } remoteValidateHandler:nil];
        }
        case HGSignupFieldFirstName: {
            return [HGSignupTextFieldItem itemWithId:identifier
                                        keyboardType:UIKeyboardTypeDefault
                                      capitalization:YES
                                              secure:NO
                                               title:@"First Name".localized
                                         placeholder:@"John".localized
                                                info:nil
                                             errorId:nil
                                          getHandler:^NSString *{
                                              return context.firstName;
                                          } commitHandler:^(NSString *value) {
                                              context.firstName = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForName:value];
                                          } remoteValidateHandler:nil];
        }
        case HGSignupFieldMiddleName: {
            return [HGSignupTextFieldItem itemWithId:identifier
                                        keyboardType:UIKeyboardTypeDefault
                                      capitalization:YES
                                              secure:NO
                                               title:@"Middle Name".localized
                                         placeholder:@"John".localized
                                                info:@"Optional".localized
                                             errorId:nil
                                          getHandler:^NSString *{
                                              return context.middleName;
                                          } commitHandler:^(NSString *value) {
                                              context.middleName = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return @"";
                                          } remoteValidateHandler:nil];
        }
        case HGSignupFieldLastName: {
            return [HGSignupTextFieldItem itemWithId:identifier
                                        keyboardType:UIKeyboardTypeDefault
                                      capitalization:YES
                                              secure:NO
                                               title:@"Last Name".localized
                                         placeholder:@"Doe".localized
                                                info:nil
                                             errorId:nil
                                          getHandler:^NSString *{
                                              return context.lastName;
                                          } commitHandler:^(NSString *value) {
                                              context.lastName = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForName:value];
                                          } remoteValidateHandler:nil];
        }
        case HGSignupFieldPracticeName: {
            return [HGSignupTextFieldItem itemWithId:identifier
                                        keyboardType:UIKeyboardTypeDefault
                                      capitalization:YES
                                              secure:NO
                                               title:@"Practice".localized
                                         placeholder:@"John Doe Practice".localized
                                                info:nil
                                             errorId:nil
                                          getHandler:^NSString *{
                                              return context.practiceName;
                                          } commitHandler:^(NSString *value) {
                                              context.practiceName = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForName:value];
                                          } remoteValidateHandler:nil];
        }
        case HGSignupFieldDOB: {
            return [HGSignupDOBFieldItem itemWithId:identifier
                                             errorId:HGSignupValidationErrors.dob
                                          getHandler:^NSString *{
                                              return context.dob;
                                          } commitHandler:^(NSString *value) {
                                              context.dob = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForDOB:value];
                                          } remoteValidateHandler:nil];
        }
        case HGSignupFieldGender: {
            return [HGSignupGenderFieldItem itemWithId:identifier
                                             errorId:nil
                                          getHandler:^NSString *{
                                              return context.gender;
                                          } commitHandler:^(NSString *value) {
                                              context.gender = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForGender:value];
                                          } remoteValidateHandler:nil];
        }
        case HGSignupFieldZip: {
            return [HGSignupZipFieldItem itemWithId:identifier
                                             errorId:HGSignupValidationErrors.zip
                                          getHandler:^NSString *{
                                              return context.zip;
                                          } commitHandler:^(NSString *value) {
                                              context.zip = value;
                                          } localValidateHandler:^NSString *(NSString *value) {
                                              return [HGSignupValidator errorTextForZip:value];
                                          } remoteValidateHandler:nil];
        }
        case HGIdologyHeaderField: {
            
            return [[HGIdologyHeaderFieldItem alloc] initWithId:identifier
                                                        errorId:nil
                                                     getHandler:nil
                                                  commitHandler:nil
                                           localValidateHandler:nil
                                          remoteValidateHandler:nil];
        }
        case HGIdologyDocTypeField: {
            return [HGIdologyDocTypeFieldItem itemWithId:identifier
                                               errorId:nil
                                            getHandler:^NSString *{
                                                return context.documentType;
                                            } commitHandler:^(NSString *value) {
                                                context.documentType = value;
                                            } localValidateHandler:^NSString *(NSString *value) {
                                                return [HGSignupValidator errorTextForIdologyDocumentType:value];
                                            } remoteValidateHandler:nil];
        }
        case HGIdologyImageFrontField: {
            //done: init HGIdologyImageFieldItem with FRONT mark
            
            HGIdologyImageFieldItem *item = [HGIdologyImageFieldItem itemWithId:identifier
                                                 errorId:nil
                                              getHandler:^NSString *{
                                                  return context.scanFrontBase64;
                                              } commitHandler:^(NSString *value) {
                                                  context.scanFrontBase64 = value;
                                              } localValidateHandler:^NSString *(NSString *value) {
                                                  return [HGSignupValidator errorTextForIdologyImage:value];
                                              } remoteValidateHandler:
//                                             nil];
                                                         ^(NSString *value, HGRemoteValidateCallback callback)
                        {
                            // async remote validation
                            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
                            [SVProgressHUD show];

                            NSData *data = [NSData dataFromBase64String:value];

                            [[HGApiClient shared] uploadImageWithData:data
                                                                 name:@"signup_idscan_front"
                                                                 type:HGIdologyImageTypeFront
                                                        pendingUserId:context.pendingUserId
                                                          resultBlock:^(HGExternalAttachment *attachment, NSError *error) {
                                                              [SVProgressHUD dismiss];
                                                              if (error.localizedDescription.hasValue || ! attachment.externalId.hasValue) {
                                                                  NSString *errorMessage = error.localizedDescription;
                                                                  if (!errorMessage.hasValue) {
                                                                      errorMessage = @"Failed".localized;
                                                                  }
                                                                  callback(errorMessage);
                                                              } else {
                                                                  context.scanFrontExternalId = attachment.externalId;
                                                                  
                                                                  callback(nil);
                                                              }
                                                          }];
                        }];
            
            item.imageType = HGIdologyImageTypeFront;
            
            return item;
        }
        case HGIdologyImageBackField: {
            //done: init HGIdologyImageFieldItem with BACK mark
            
            HGIdologyImageFieldItem *item = [HGIdologyImageFieldItem itemWithId:identifier
                                                                        errorId:nil
                                                                     getHandler:^NSString *{
                                                                         return context.scanBackBase64;
                                                                     } commitHandler:^(NSString *value) {
                                                                         context.scanBackBase64 = value;
                                                                     } localValidateHandler:^NSString *(NSString *value) {
                                                                         return nil;
                                                                     } remoteValidateHandler:
//                                             nil];
                                             ^(NSString *value, HGRemoteValidateCallback callback)
            {
                // async remote validation
                [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
                [SVProgressHUD show];

                NSData *data = [NSData dataFromBase64String:value];

                [[HGApiClient shared] uploadImageWithData:data
                                                     name:@"signup_idscan_back"
                                                     type:HGIdologyImageTypeBack
                                            pendingUserId:context.pendingUserId
                                              resultBlock:^(HGExternalAttachment *attachment, NSError *error) {
                                                  [SVProgressHUD dismiss];
                                                  if (error.localizedDescription.hasValue || ! attachment.externalId.hasValue) {
                                                      NSString *errorMessage = error.localizedDescription;
                                                      if (!errorMessage.hasValue) {
                                                          errorMessage = @"Failed".localized;
                                                      }
                                                      callback(errorMessage);
                                                  } else {
                                                      context.scanBackExternalId = attachment.externalId;
                                                      
                                                      callback(nil);
                                                  }
                                              }];

            }];

            item.imageType = HGIdologyImageTypeBack;
            
            return item;
        }
        case HGIdologyPhotoTipsField: {
            return [[HGIdologyPhotoTipsFieldItem alloc] initWithId:identifier
                                                           errorId:nil
                                                        getHandler:nil
                                                     commitHandler:nil
                                              localValidateHandler:nil
                                             remoteValidateHandler:nil];
            
        }
        case HGIdologyNeedHelpField: {
            //TODO: implement "NeedHelp?" cell with promt for phone number to call
            return  nil;
        }
    }
    return nil;
}

@end
