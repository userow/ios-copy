//
//  HGPerformIdScanActionResponse.h
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-25.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

@interface HGPerformIdScanActionResponse : JSONModel

/*
 Response parameters:
 
 success    boolean    If true then validation passed and no errors occurred
 
 */

/**
 boolean    If true then ID scan request accepted by IDology and polling results can be started. IF FALSE - Sign Up should be terminated
 */
@property (nonatomic) bool idscanSuccess;

/**
 errorMessage    String    Error message occured on server side
 */
@property (nonatomic, strong) NSString *errorMessage;

@end
