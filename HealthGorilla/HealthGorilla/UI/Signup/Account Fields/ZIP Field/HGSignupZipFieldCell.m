//
//  HGSignupZipFieldCell.m
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupZipFieldCell.h"
#import "HGSignupZipFieldItem.h"
#import "OCMaskedTextFieldView.h"


@interface HGSignupZipFieldCell () <OCMaskedTextFieldDelegate>

@property(nonatomic, weak) IBOutlet OCMaskedTextFieldView *zipField;

@end


@implementation HGSignupZipFieldCell

#pragma mark - Overloads

- (void)onUpdateCell
{
    HGSignupZipFieldItem *item = (HGSignupZipFieldItem *)self.item;
    
    [_zipField clear]; // FIX: clear before updating
    [_zipField setBackgroundColor:[UIColor clearColor]];
    [_zipField.maskedTextField setBorderStyle:UITextBorderStyleNone];
    [_zipField.maskedTextField setFont:[UIFont lightSystemFontOfSize:16]];
    [_zipField.maskedTextField setPlaceholder:@"90000"];
    [_zipField.maskedTextField setInputAccessoryView:[self keyboardAccessoryView]];
    [_zipField setMask:@"#####"];
    [_zipField setInternal_delegate:self];
    [_zipField setRawInput:item.getHandler()];
}


- (void)onUpdateError
{
    // nop
}


- (void)setFocusOn
{
    [_zipField.maskedTextField becomeFirstResponder];
}


- (bool)canSetFocusOn
{
    return YES;
}


#pragma mark - OCMaskedTextFieldDelegate

- (void)maskedTextFieldDidBeginEditing:(OCMaskedTextFieldView *)textField
{
    // nop
}


- (void)maskedTextFieldDidEndEditing:(OCMaskedTextFieldView *)textField
{
    NSString *value = [textField getRawInputText];
    [self commitValue:value updateCellOnCommit:YES];
}


- (void)textFieldDidChange:(OCMaskedTextFieldView *)textField
{
    NSString *value = [textField getRawInputText];
    if ((value.length == 0) || (value.length == 5)) {
        [self commitValue:value updateCellOnCommit:YES];
    }
    else{
        [self commitValue:value updateCellOnCommit:NO];
    }
}

@end
