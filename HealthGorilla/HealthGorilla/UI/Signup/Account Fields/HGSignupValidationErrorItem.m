//
//  HGSignupValidationErrorItem.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupValidationErrorItem.h"


const struct HGSignupValidationErrors HGSignupValidationErrors = {
    .email = @"email",
    .username = @"reglgn",
    .password = @"regpwd",
    .zip = @"zipcode",
    .dob = @"dob",
    .captcha = @"captcha",
};


@implementation HGSignupValidationErrorItem

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@ {
        @"identifier" : @"fieldName",
        @"message" : @"fieldError",
    }];
}

@end
