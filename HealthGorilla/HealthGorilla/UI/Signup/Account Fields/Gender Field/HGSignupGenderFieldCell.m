//
//  HGSignupGenderFieldCell.m
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupGenderFieldCell.h"
#import "HGSignupGenderFieldItem.h"
#import "HGGender.h"


@interface HGSignupGenderFieldCell ()

@property (nonatomic, weak) IBOutlet UISegmentedControl *genderSegments;

@end


@implementation HGSignupGenderFieldCell

#pragma mark - Overloads

- (void)onUpdateCell
{
    HGSignupGenderFieldItem *item = (HGSignupGenderFieldItem *)self.item;
    
    NSString *gender = item.getHandler();
    NSInteger index = [[HGGender allGenders] indexOfObject:gender];
    
    _genderSegments.selectedSegmentIndex = index;
}


- (void)onUpdateError
{
    // nop
}


- (void)setFocusOn
{
    // show 'Required' under Gender when it's Next field
    [self updateCell];
}


- (bool)canSetFocusOn
{
    return NO;
}


#pragma mark - UISegmentedControl actions

- (IBAction)segmentedControlDidChangeValue:(UISegmentedControl *)sender
{
    NSInteger index = sender.selectedSegmentIndex;
    NSString *gender = [HGGender allGenders][index];
    
    // commit updates cell, so after a tap on segment we wait a moment
    const dispatch_time_t delay = 100; // msec
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
        [self commitValue:gender updateCellOnCommit:YES];
    });
}

@end
