//
//  HGSignupAccountFieldItem.m
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupAccountFieldItem.h"


const struct HGSignupAccountFieldAttributes HGSignupAccountFieldAttributes = {
    .error = @"error",
};


@interface HGSignupAccountFieldItem ()
@end


@implementation HGSignupAccountFieldItem

- (instancetype)initWithId:(HGSignupFieldID)identifier errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler
{
    self = [super init];
    if (!self) return nil;
    
    _identifier = identifier;
    _errorId = errorId;
    _getHandler = getHandler;
    _commitHandler = commitHandler;
    _localValidateHandler = localValidateHandler;
    _remoteValidateHandler = remoteValidateHandler;

    return self;
}


- (bool)isValid
{
    NSString *error = nil;
    
    if (nil != self.getHandler) {
        NSString *value = self.getHandler();
        if (nil != self.localValidateHandler) {
            error = self.localValidateHandler(value);
        }
    }
    return !error.hasValue && !self.invalidatedByServer;
}


#pragma mark - Overloads

- (NSString *)cellId
{
    return nil;
}

@end
