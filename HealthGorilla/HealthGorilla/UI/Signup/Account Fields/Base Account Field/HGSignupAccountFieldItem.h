//
//  HGSignupAccountFieldItem.h
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupFieldsIdentifiers.h"


extern const struct HGSignupAccountFieldAttributes {
    __unsafe_unretained NSString *error;
} HGSignupAccountFieldAttributes;


typedef NSString * (^HGSignupAccountFieldGetHandler)();
typedef void (^HGSignupAccountFieldCommitHandler)(NSString *value);
typedef NSString * (^HGSignupAccountFieldLocalValidateHandler)(NSString *value);
typedef void (^HGRemoteValidateCallback)(NSString *message);
typedef void (^HGSignupAccountFieldRemoteValidateHandler)(NSString *value, HGRemoteValidateCallback callback);


@interface HGSignupAccountFieldItem : NSObject

@property (nonatomic) HGSignupFieldID identifier;
@property (nonatomic, strong) NSString *errorId; // HGSignupValidationErrors
@property (nonatomic, readonly) NSString *cellId; // overloaded in subclasses
//
@property (nonatomic, strong) NSString *error; // error text
@property (nonatomic) bool invalidatedByServer;
//
@property (nonatomic, copy) HGSignupAccountFieldGetHandler getHandler;
@property (nonatomic, copy) HGSignupAccountFieldCommitHandler commitHandler;
@property (nonatomic, copy) HGSignupAccountFieldLocalValidateHandler localValidateHandler;
@property (nonatomic, copy) HGSignupAccountFieldRemoteValidateHandler remoteValidateHandler;

- (instancetype)initWithId:(HGSignupFieldID)identifier errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler;

- (bool)isValid;

@end
