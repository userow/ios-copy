//
//  HGSignupAccountFieldCell.m
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupAccountFieldCell.h"
#import "HGSignupAccountFieldItem.h"
#import "HGObserver.h"


@interface HGSignupAccountFieldCell ()

@property (nonatomic, copy) HGSignupAccountFieldUpdateCellHandler updateCellHandler;
@property (nonatomic, copy) HGSignupAccountFieldValidateFormHandler validateFormHandler;
@property (nonatomic, copy) HGSignupAccountFieldNextFieldHandler nextFieldHandler;
@property (nonatomic, strong) HGObserver *observer;
//
@property (nonatomic, weak) IBOutlet UILabel *errorLabel;

@end


@implementation HGSignupAccountFieldCell

- (void)updateWithItem:(HGSignupAccountFieldItem *)item updateCellHandler:(HGSignupAccountFieldUpdateCellHandler)updateCellHandler validateFormHandler:(HGSignupAccountFieldValidateFormHandler)validateFormHandler nextFieldHandler:(HGSignupAccountFieldNextFieldHandler)nextFieldHandler
{
    self.item = item;
    self.updateCellHandler = updateCellHandler;
    self.validateFormHandler = validateFormHandler;
    self.nextFieldHandler = nextFieldHandler;
    
    // error observer
    __weak typeof(self) weakSelf = self;
    _observer = [[HGObserver alloc] initWithObject:item keyPath:HGSignupAccountFieldAttributes.error action:^(NSObject *object, NSString *keyPath, NSDictionary *change) {
        NSString *error = [object valueForKeyPath:keyPath];
        [weakSelf updateWithErrorText:error];
    }];
    
    [self onUpdateCell]; // overloaded in subclasses
    [self updateWithErrorText:item.error];
    
    [self.contentView setNeedsLayout];
}


- (void)prepareForReuse
{
    [super prepareForReuse];
    //
    self.item = nil;
    self.updateCellHandler = nil;
    self.validateFormHandler = nil;
    self.observer = nil;
}


- (void)updateWithErrorText:(NSString *)error
{
    self.errorLabel.text = error;
    [self onUpdateError]; // overloaded in subclasses
}


- (void)commitValue:(NSString *)value updateCellOnCommit:(bool)updateCellOnCommit
{
    // commit changes back to context
    self.item.commitHandler(value);
    
    // notify cell about changes
    if (updateCellOnCommit) {
        [self updateCell];
    }
    
    // force controller to validate Next button
    self.validateFormHandler();
}


- (void)updateCell
{
    if (_updateCellHandler) {
        self.updateCellHandler();
    }
}


- (void)activateNextField
{
    if (_nextFieldHandler) {
        self.nextFieldHandler();
    }
}


#pragma mark - Overloads

- (void)onUpdateCell
{
    // nop
}


- (void)onUpdateError
{
    // nop
}


- (void)setFocusOn
{
    // nop
}


- (bool)canSetFocusOn
{
    return NO;
}


#pragma mark - Keyboard Accessory View

- (UIView *)keyboardAccessoryView
{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleDefault];
    [toolbar sizeToFit];
    
    __weak typeof(self) weakSelf = self;
    UIBarButtonItem *doneButton = [HGBarButtonFactory doneButtonWithTitle:@"Next".localized action:^(id sender) {
        [weakSelf activateNextField];
    }];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[ flexibleSpace, doneButton ];
    
    return toolbar;
}

@end
