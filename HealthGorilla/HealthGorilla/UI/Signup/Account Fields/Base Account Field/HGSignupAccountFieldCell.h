//
//  HGSignupAccountFieldCell.h
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@class HGSignupAccountFieldItem;

typedef void (^HGSignupAccountFieldUpdateCellHandler)();
typedef void (^HGSignupAccountFieldValidateFormHandler)();
typedef void (^HGSignupAccountFieldNextFieldHandler)();

@interface HGSignupAccountFieldCell : UITableViewCell

@property (nonatomic, strong) HGSignupAccountFieldItem *item;

- (void)updateWithItem:(HGSignupAccountFieldItem *)item
     updateCellHandler:(HGSignupAccountFieldUpdateCellHandler)updateCellHandler
   validateFormHandler:(HGSignupAccountFieldValidateFormHandler)validateFormHandler
      nextFieldHandler:(HGSignupAccountFieldNextFieldHandler)nextFieldHandler;

- (void)commitValue:(NSString *)value updateCellOnCommit:(bool)updateCellOnCommit;
- (UIView *)keyboardAccessoryView;
- (void)updateCell;

// overloads
- (void)onUpdateCell;
- (void)onUpdateError;
- (void)setFocusOn;
- (bool)canSetFocusOn;

@end
