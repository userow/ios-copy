//
//  HGSignupValidator.h
//  HG-Project
//
//  Created by not Paul on 05/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@interface HGSignupValidator : NSObject

+ (NSString *)errorTextForEmail:(NSString *)email optional:(bool)optional;
+ (NSString *)errorTextForUsername:(NSString *)username;
+ (NSString *)errorTextForName:(NSString *)name;
+ (NSString *)errorTextForPassword:(NSString *)password;
+ (NSString *)errorTextForZip:(NSString *)zip;
+ (NSString *)errorTextForDOB:(NSString *)dob;
+ (NSString *)errorTextForGender:(NSString *)gender;

#pragma mark - Idology ScanID

+ (NSString *)errorTextForIdologyDocumentType:(NSString *)docType;
+ (NSString *)errorTextForIdologyImage:(NSString *)image;

 @end
