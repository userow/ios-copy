//
//  HGPerformIdScanPollingResultsActionResponse.h
//  HG-Project
//
//  Created by Pavel Wasilenko on 18-08-31.
//  Copyright © 2018 HG-Inc. All rights reserved.
//

@interface HGPerformIdScanPollingResultsActionResponse : JSONModel

/*
 Response parameters:
 
 idologySuccess    boolean    If true then user passedIDology check
 idscanPending    boolean    If true then means results are not ready - do nothing and repeat at next iteration
 idscanObsolete    boolean
 True is possible if after ID scan completion further process takes more the 2 sec (recommended interation interval) and next performIdScanPollingResultsAction request sent before previous response return - in this case just stop polling
 
 emailConfirmNeeded    boolean    If true then email was provided at 1st step and system sent user confirmation code to email address at 'email' field
 email    String    Email address where confirmation code was sent
 loginResponse    LoginResponse    LoginResponse object from auto login
 */

/**
 If true then user passedIDology check
 */
@property (nonatomic) bool idologySuccess;
/**
 If true then means results are not ready - do nothing and repeat at next iteration
 */
@property (nonatomic) bool idscanPending;
/**
 True is possible if after ID scan completion further process takes more the 2 sec (recommended interation interval) and next performIdScanPollingResultsAction request sent before previous response return - in this case just stop polling
 */
@property (nonatomic) bool idscanObsolete;
/**
 If true then email was provided at 1st step and system sent user confirmation code to email address at 'email' field
 */
@property (nonatomic) bool emailConfirmNeeded;
/**
 Email address where confirmation code was sent
 */
@property (nonatomic, strong) NSString *email;

//TODO: Add AutoLogin implementetion !!!

@end
