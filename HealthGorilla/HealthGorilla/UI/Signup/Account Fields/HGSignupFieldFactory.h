//
//  HGSignupFieldFactory.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupStages.h"

@class HGSignupAccountFieldItem, HGSignupContext;

@interface HGSignupFieldFactory : NSObject

+ (NSArray<HGSignupAccountFieldItem *> *)fieldsForStage:(HGSignupStage)stage context:(HGSignupContext *)context;

@end
