//
//  HGSignupDOBFieldCell.m
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupDOBFieldCell.h"
#import "HGSignupDOBFieldItem.h"
#import "IQDropDownTextField.h"


@interface HGSignupDOBFieldCell () <IQDropDownTextFieldDelegate>

@property(nonatomic, weak) IBOutlet IQDropDownTextField *dobField;

@end


@implementation HGSignupDOBFieldCell

#pragma mark - Overloads

- (void)onUpdateCell
{
    HGSignupDOBFieldItem *item = (HGSignupDOBFieldItem *)self.item;

    _dobField.dropDownMode = IQDropDownModeDatePicker;
    _dobField.inputAccessoryView = [self keyboardAccessoryView];
    _dobField.datePicker.maximumDate = [NSDate date];
    
    NSString *value = item.getHandler();
    if (value.hasValue) {
        _dobField.date = [HGDate dobWithString:value].date;
    } else {
        _dobField.selectedItem = nil; // show placeholder
    }
}

- (void)doneAction:(UITextField *)textField
{
    NSString *value = [[[HGDate alloc] initWithNSDateObject:_dobField.datePicker.date] dobAsString];
    [self commitValue:value updateCellOnCommit:NO];
}


- (void)onUpdateError
{
    // nop
}


- (void)setFocusOn
{
    [_dobField becomeFirstResponder];
}


- (bool)canSetFocusOn
{
    return YES;
}


#pragma mark - IQDropDownTextFieldDelegate

- (void)textField:(IQDropDownTextField *)textField didSelectDate:(NSDate *)date
{
    NSString *value = [[[HGDate alloc] initWithNSDateObject:date] dobAsString];
    [self commitValue:value updateCellOnCommit:NO];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    HGSignupDOBFieldItem *item = (HGSignupDOBFieldItem *)self.item;
    
    // initialize empty DOB with default DOB
    NSString *value = item.getHandler();
    if (!value.hasValue) {
        _dobField.date = [HGDate defaultDOB].date;
        // commit value to context
        [self textField:_dobField didSelectDate:_dobField.date];
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    _dobField.date = nil;
    [self commitValue:nil updateCellOnCommit:YES];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(_dobField.date){
        NSString *value = [[[HGDate alloc] initWithNSDateObject:_dobField.date] dobAsString];
        [self commitValue:value updateCellOnCommit:YES];
    }
}

@end
