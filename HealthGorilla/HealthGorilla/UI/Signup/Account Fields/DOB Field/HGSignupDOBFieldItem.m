//
//  HGSignupDOBFieldItem.m
//  HG-Project
//
//  Created by not Paul on 08/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupDOBFieldItem.h"

@implementation HGSignupDOBFieldItem

+ (instancetype)itemWithId:(HGSignupFieldID)identifier errorId:(NSString *)errorId getHandler:(HGSignupAccountFieldGetHandler)getHandler commitHandler:(HGSignupAccountFieldCommitHandler)commitHandler localValidateHandler:(HGSignupAccountFieldLocalValidateHandler)localValidateHandler remoteValidateHandler:(HGSignupAccountFieldRemoteValidateHandler)remoteValidateHandler
{
    return [[self alloc] initWithId:identifier errorId:errorId getHandler:getHandler commitHandler:commitHandler localValidateHandler:localValidateHandler remoteValidateHandler:remoteValidateHandler];
}


#pragma mark - Overloads

- (NSString *)cellId
{
    static NSString *cellId = @"DOBFieldCell";
    return cellId;
}

@end
