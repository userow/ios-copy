//
//  HGSignupFieldsIdentifiers.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

typedef NS_ENUM(NSUInteger, HGSignupFieldID) {
    HGSignupFieldEmail,
    HGSignupFieldUsername,
    HGSignupFieldPassword,
    HGSignupFieldFirstName,
    HGSignupFieldMiddleName,
    HGSignupFieldLastName,
    HGSignupFieldPracticeName,
    HGSignupFieldGender,
    HGSignupFieldDOB,
    HGSignupFieldZip,
    
    HGIdologyHeaderField,
    HGIdologyDocTypeField,
    HGIdologyImageFrontField,
    HGIdologyImageBackField,
    HGIdologyPhotoTipsField,
    HGIdologyNeedHelpField,
};
