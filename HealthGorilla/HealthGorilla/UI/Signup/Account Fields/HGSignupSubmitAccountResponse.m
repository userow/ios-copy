//
//  HGSignupSubmitAccountResponse.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupSubmitAccountResponse.h"

@implementation HGSignupSubmitAccountResponse

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@ {
        @"needAskSSN4" : @"askSsn4",
        @"validationErrors" : @"validationErrorList",
    }];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

@end
