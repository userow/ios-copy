//
//  HGSignupSSN4CheckViewController.m
//  HG-Project
//
//  Created by not Paul on 02/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupSSN4CheckViewController.h"


@interface HGSignupSSN4CheckViewController () <UITextFieldDelegate>

@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, copy) HGSignupSSN4CheckCompletion completion;
@property (nonatomic) bool allowBack;
//
@property (nonatomic, strong) NSString *ssn4code;

@end


@implementation HGSignupSSN4CheckViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context allowBack:(bool)allowBack storyboard:(UIStoryboard *)storyboard completion:(HGSignupSSN4CheckCompletion)completion
{
    HGSignupSSN4CheckViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    c.allowBack = allowBack;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    if (!self.allowBack) {
        __weak typeof(self) weakSelf = self;
        self.navigationItem.leftBarButtonItem = [HGBarButtonFactory cancelButtonWithAction:^(id sender) {
            [weakSelf tapOnCancel:nil];
        }];
    }
    [self validateNextButton];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (str.length > 4) {
        return NO; // too long
    }
    if (str.hasValue && !str.containsDigitsOnly) {
        return NO; // not digits
    }
    self.ssn4code = str;
    
    [self validateNextButton];
    
    return YES;
}


#pragma mark - Methods

- (void)validateNextButton
{
    bool valid = (self.ssn4code.length == 4) && self.ssn4code.containsDigitsOnly;
    self.navigationItem.rightBarButtonItem.enabled = valid;
}


- (IBAction)tapOnNext:(id)sender
{
    bool cancelled = NO;
    bool needHelp = NO;
    self.completion(cancelled, self.ssn4code, needHelp);
}


- (IBAction)tapOnNeedHelp:(id)sender
{
    bool cancelled = NO;
    bool needHelp = YES;
    self.completion(cancelled, nil, needHelp);
}


- (void)tapOnBack // overwritten
{
    // 'Back' works when allowBack=YES
    [self tapOnCancel:nil];
}


- (void)tapOnCancel:(id)sender
{
    bool cancelled = YES;
    bool needHelp = NO;
    self.completion(cancelled, nil, needHelp);
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
