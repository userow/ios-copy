//
//  HGSignupSSN4CheckViewController.h
//  HG-Project
//
//  Created by not Paul on 02/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"

typedef void (^HGSignupSSN4CheckCompletion)(bool cancelled, NSString *ssn4code, bool needHelp);

@interface HGSignupSSN4CheckViewController : UIViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context allowBack:(bool)allowBack storyboard:(UIStoryboard *)storyboard completion:(HGSignupSSN4CheckCompletion)completion;

@end
