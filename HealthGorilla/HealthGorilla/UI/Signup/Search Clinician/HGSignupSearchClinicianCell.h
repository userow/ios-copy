//
//  HGSignupSearchClinicianCell.h
//  HG-Project
//
//  Created by not Paul on 06/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@interface HGSignupSearchClinicianCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;

@end
