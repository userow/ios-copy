//
//  HGSignupSearchClinicianViewController.m
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupSearchClinicianViewController.h"
#import "HGSignupSearchClinicianCell.h"
#import "HGSignupSearchClinicianItem.h"
#import "HGSearchTimer.h"
#import "HGApiClient+Signup.h"


@interface HGSignupSearchClinicianViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, copy) HGSignupSearchClinicianCompletion completion;
//
@property (nonatomic, strong) NSArray<HGSignupSearchClinicianItem *> *items;
@property (nonatomic, strong) HGSearchTimer *searchTimer;
@property (nonatomic) bool isLoading;
//
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end


@implementation HGSignupSearchClinicianViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupSearchClinicianCompletion)completion
{
    HGSignupSearchClinicianViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60;
    
    self.searchTimer = [[HGSearchTimer alloc] init];
    
    [self validateNextButton];
    [self updateFooterVisibility];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    [self.searchBar becomeFirstResponder];
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self hideKeyboard];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isLoading) {
        return 1;
    } else if (self.hasNoRecordsForQuery) {
        return 1;
    }
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isLoading) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell"];
        return cell;
        
    } else if (self.hasNoRecordsForQuery) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoRecordsCell"];
        return cell;
    }
    
    HGSignupSearchClinicianCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClinicianCell"];
    
    HGSignupSearchClinicianItem *item = self.items[indexPath.row];
    bool selected = [item.npi isEqual:self.context.clinician.npi];

    cell.nameLabel.text = [NSString stringWithFormat:@"%@, NPI %@".localized, item.name, item.npi];
    cell.addressLabel.text = item.address;
    cell.accessoryType = (selected) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGSignupSearchClinicianItem *item = self.items[indexPath.row];
    self.context.clinician = item;
    
    [tableView beginUpdates];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView endUpdates];
    
    [self validateNextButton];
}


#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    __weak typeof(self) weakSelf = self;
    [self.searchTimer delayQuery:searchText completion:^(NSString *query) {
        [weakSelf reloadDataSourceWithQuery:query];
    }];
}


- (void)reloadDataSourceWithQuery:(NSString *)query
{
    self.items = nil;
    self.context.clinician = nil;
    
    bool needSearch = query.hasValue;
    
    [self.tableView reloadData];
    [self updateFooterVisibility];
    
    if (!needSearch) {
        return; // 'Clear' button
    }
    
    self.isLoading = YES;
    
    __weak typeof(self) weakSelf = self;
    [[HGApiClient shared] searchClinitian:query handler:^(NSArray<HGSignupSearchClinicianItem *> *items, NSError *error) {
        
        weakSelf.items = items;
        weakSelf.isLoading = NO;
        
        [weakSelf.tableView reloadData];
        [weakSelf updateFooterVisibility];
    }];
}


#pragma mark - Methods

- (void)validateNextButton
{
    bool valid = (nil != self.context.clinician);
    self.navigationItem.rightBarButtonItem.enabled = valid;
}

/*
- (void)tapOnBack // overwritten
{
    self.context.clinician = nil;
}
*/

- (IBAction)tapOnCancel:(id)sender
{
    [self hideKeyboard];
    
    bool cancelled = YES;
    self.completion(cancelled);
}


- (IBAction)tapOnNext:(id)sender
{
    [self hideKeyboard];
    
    bool cancelled = NO;
    self.completion(cancelled);
}


- (void)updateFooterVisibility
{
    bool visible = (0 == self.items.count) && !self.searchBar.text.hasValue && !self.isLoading;
    self.tableView.tableFooterView.hidden = !visible;
}


- (bool)hasNoRecordsForQuery
{
    return (0 == self.items.count && self.searchBar.text.hasValue);
}


- (void)hideKeyboard
{
    [self.searchBar resignFirstResponder];
}


- (IBAction)unwindClinicianToSignupStart:(UIStoryboardSegue *)unwindSegue
{
    // way to unwind to 1st screen
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
