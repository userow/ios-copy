//
//  HGSignupSearchClinicianViewController.h
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"

typedef void (^HGSignupSearchClinicianCompletion)(bool cancelled);

@interface HGSignupSearchClinicianViewController : UIViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupSearchClinicianCompletion)completion;

@end
