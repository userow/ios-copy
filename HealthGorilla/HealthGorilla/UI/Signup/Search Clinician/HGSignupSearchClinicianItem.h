//
//  HGSignupSearchClinicianItem.h
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@interface HGSignupSearchClinicianItem : JSONModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *npi;
@property (nonatomic, strong) NSString *address;

@end
