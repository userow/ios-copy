//
//  HGTutorialSelectUserTypeViewController.h
//  HG-Project
//
//  Created by not Paul on 16/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupUserTypes.h"

typedef void (^HGTutorialSelectUserTypeCompletion)(bool cancelled, HGSignupUserType userType);

@interface HGTutorialSelectUserTypeViewController : UIViewController

+ (instancetype)instanceWithStoryboard:(UIStoryboard *)storyboard completion:(HGTutorialSelectUserTypeCompletion)completion;

@end
