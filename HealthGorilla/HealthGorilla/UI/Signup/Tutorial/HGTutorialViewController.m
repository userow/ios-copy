//
//  HGTutorialViewController.m
//  HG-Project
//
//  Created by not Paul on 15/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGTutorialViewController.h"
#import "HGHypertextLabel.h"


@interface HGTutorialViewController () <UIScrollViewDelegate>

@property (nonatomic) HGSignupUserType userType;
@property (nonatomic, copy) HGTutorialCompletion completion;
@property (nonatomic, strong) NSArray *pageViews;
//
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIButton *signupButton;
@property (nonatomic, weak) IBOutlet HGHypertextLabel *loginLabel;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *md_pageViews;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *pt_pageViews;

@end


@implementation HGTutorialViewController

+ (instancetype)instanceWithUserType:(HGSignupUserType)userType storyboard:(UIStoryboard *)storyboard completion:(HGTutorialCompletion)completion
{
    HGTutorialViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.userType = userType;
    c.completion = completion;

    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    self.navigationItem.hidesBackButton = YES;
    
    switch (_userType) {
        case HGSignupUserPatient:
            self.pageViews = self.pt_pageViews;
            break;
        case HGSignupUserStaff:
        case HGSignupUserClinician:
            self.pageViews = self.md_pageViews;
            break;
        default:
            break;
    }
    
    // add subpages to scroll view
    for (UIView *pageView in _pageViews) {
        pageView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:pageView];
    }
    
    NSInteger startPageIndex = 0;
    
    // configure page control
    _pageControl.numberOfPages = _pageViews.count;
    _pageControl.currentPage = startPageIndex;
    
    [self updateSignupButtonOnPageIndex:startPageIndex];
    [self updateLoginButton];
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    //
    [self layoutSubpages];
}


- (void)layoutSubpages
{
    CGRect bounds = _scrollView.bounds;
    
    const CGFloat pageWidth = CGRectGetWidth(bounds);
    const CGFloat pageHeight = CGRectGetHeight(bounds);
    
    _scrollView.contentSize = CGSizeMake(pageWidth * _pageViews.count, pageHeight);
    
    // layout subpages
    [_pageViews enumerateObjectsUsingBlock:^(UIView *pageView, NSUInteger index, BOOL * _Nonnull stop) {
        
        const CGFloat pageOffset = index * pageWidth;
        pageView.frame = CGRectMake(pageOffset, 0, pageWidth, pageHeight);
    }];
    
    CGFloat contentOffset = pageWidth * _pageControl.currentPage;
    if (_scrollView.contentOffset.x != contentOffset) {
        [_scrollView setContentOffset:CGPointMake(contentOffset, 0) animated:NO];
    }
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updatePageIndexAfterScroll];
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self updatePageIndexAfterScroll];
}


#pragma mark - Methods

- (void)updatePageIndexAfterScroll
{
    CGFloat pageWidth = _scrollView.bounds.size.width;
    NSInteger pageIndex = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = pageIndex;
    
    [self updateSignupButtonOnPageIndex:pageIndex];
}


- (void)updateSignupButtonOnPageIndex:(NSInteger)pageIndex
{
    NSString *signupTitle = (pageIndex < _pageViews.count-1) ? @"Next".localized.uppercaseString : @"GET STARTED".localized;
    [_signupButton setTitle:signupTitle forState:UIControlStateNormal];
}


- (void)updateLoginButton
{
    NSString *text = @"Already have an account? <0:Login>".localized;
    
    __weak typeof(self) weakSelf = self;
    
    _loginLabel.hypertext = text;
    _loginLabel.linkTapHandler = ^(NSUInteger linkId) {
        [weakSelf tapOnLogin];
    };
}


- (void)tapOnLogin
{
    bool cancelled = YES;
    self.completion(cancelled);
}


- (IBAction)tapOnSignup:(id)sender
{
    NSInteger pageIndex = _pageControl.currentPage;
    bool needSignup = (pageIndex == _pageViews.count-1);
    
    if (needSignup) {
        bool cancelled = NO;
        self.completion(cancelled);
    } else {
        [self scrollToPage:pageIndex+1]; // next page
    }
}


- (void)scrollToPage:(NSInteger)pageIndex
{
    CGFloat pageWidth = _scrollView.bounds.size.width;
    CGFloat pageOffset = pageWidth * pageIndex;
    [_scrollView setContentOffset:CGPointMake(pageOffset, 0) animated:YES];
}


#pragma mark - Light Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
