//
//  HGTutorialViewController.h
//  HG-Project
//
//  Created by not Paul on 15/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupUserTypes.h"

typedef void (^HGTutorialCompletion)(bool cancelled);

@interface HGTutorialViewController : UIViewController

+ (instancetype)instanceWithUserType:(HGSignupUserType)userType storyboard:(UIStoryboard *)storyboard completion:(HGTutorialCompletion)completion;

@end
