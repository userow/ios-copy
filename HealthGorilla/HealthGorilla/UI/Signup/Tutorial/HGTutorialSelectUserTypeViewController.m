//
//  HGTutorialSelectUserTypeViewController.m
//  HG-Project
//
//  Created by not Paul on 16/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGTutorialSelectUserTypeViewController.h"
#import "HGHypertextLabel.h"


@interface HGTutorialSelectUserTypeViewController ()

@property (nonatomic, copy) HGTutorialSelectUserTypeCompletion completion;
//
@property (nonatomic, weak) IBOutlet HGHypertextLabel *loginLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tutorialLogoImageView;

@property (weak, nonatomic) IBOutlet UIButton *patientButton;
@property (weak, nonatomic) IBOutlet UIButton *clinicianButton;
@property (weak, nonatomic) IBOutlet UIButton *medicalStaffButton;

@end


@implementation HGTutorialSelectUserTypeViewController

+ (instancetype)instanceWithStoryboard:(UIStoryboard *)storyboard completion:(HGTutorialSelectUserTypeCompletion)completion
{
    HGTutorialSelectUserTypeViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.completion = completion;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    
    [self updateLoginButton];
    
    if ([HGAppConfig isAngelesClinical]) {
        self.tutorialLogoImageView.image = [UIImage imageNamed:@"tutorial-logoAc"];
        
        self.clinicianButton.hidden = YES;
        self.medicalStaffButton.hidden = YES;
    }
}


#pragma mark - Methods

- (void)updateLoginButton
{
    NSString *text = @"Already have an account? <0:Login>".localized;
    
    __weak typeof(self) weakSelf = self;
    
    _loginLabel.hypertext = text;
    _loginLabel.linkTapHandler = ^(NSUInteger linkId) {
        [weakSelf tapOnLogin];
    };
}


- (IBAction)tapOnPatient:(id)sender
{
    bool cancelled = NO;
    self.completion(cancelled, HGSignupUserPatient);
}


- (IBAction)tapOnClinician:(id)sender
{
    bool cancelled = NO;
    self.completion(cancelled, HGSignupUserClinician);
}


- (IBAction)tapOnStaff:(id)sender
{
    bool cancelled = NO;
    self.completion(cancelled, HGSignupUserStaff);
}


- (void)tapOnLogin
{
    bool cancelled = YES;
    self.completion(cancelled, HGSignupUserUndefined);
}


#pragma mark - Light Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
