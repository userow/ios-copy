//
//  HGSignupVerifyEmailWorkflow.h
//  HG-Project
//
//  Created by not Paul on 23/11/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@class HGSignupContext;

typedef void (^HGSignupVerifyEmailWorkflowCompletion)(bool cancelled);

@interface HGSignupVerifyEmailWorkflow : NSObject

+ (UIViewController *)controllerWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupVerifyEmailWorkflowCompletion)completion;

@end
