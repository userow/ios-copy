//
//  HGSignupVerifyEmailWorkflow.m
//  HG-Project
//
//  Created by not Paul on 23/11/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupVerifyEmailWorkflow.h"
#import "HGSignupVerifyEmailViewController.h"
#import "HGApiClient+Signup.h"


typedef void (^HGResendVerificationCodeCompletion)(NSString *email);


@implementation HGSignupVerifyEmailWorkflow

+ (UIViewController *)controllerWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupVerifyEmailWorkflowCompletion)completion
{
    UIViewController *c = [HGSignupVerifyEmailViewController instanceWithContext:context storyboard:storyboard completion:^(bool cancelled, NSString *code, bool needResend, bool skipConfirmation, bool webConfirmed, HGSignupVerifyEmailCallback resendEmailCallback) {
        
        if (cancelled) {
            completion(cancelled);
            return; // verification cancelled
        }
        
        if (needResend) {
            // ask email and submit it
            [self resendWithDefaultEmail:context.email completion:^(NSString *email) {
                // new code was sent to updated email
                context.email = email;
                // update email in the screen
                resendEmailCallback(email);
            }];
            return; // need resend
        }
        
        if (webConfirmed) {
            completion(cancelled);
            return; // email confirmed by URL in email, ready to autoLogin
        }
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD show];
        
        [[HGApiClient shared] submitEmailConfirmation:code skipConfirmation:skipConfirmation handler:^(bool success, NSError *error) {
            
            [SVProgressHUD dismiss];
            if (error || !success) {
                [self showAlertWithError:error];
                return; // error
            }
            // signup after email confirmation, ready to autoLogin
            completion(cancelled);
        }];
    }];
    
    return c;
}


+ (void)resendWithDefaultEmail:(NSString *)defaultEmail completion:(HGResendVerificationCodeCompletion)completion
{
    [self askEmailWithDefaultEmail:defaultEmail completion:^(NSString *email) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD show];
        
        [[HGApiClient shared] submitResendCode:email handler:^(bool success, NSError *error) {
            
            [SVProgressHUD dismiss];
            if (error || !success) {
                [self showAlertWithError:error];
                return; // error
            }
            // OK, submitResendCode succeeded
            completion(email);
        }];
    }];
}


+ (void)askEmailWithDefaultEmail:(NSString *)defaultEmail completion:(HGResendVerificationCodeCompletion)completion
{
    NSString *title = @"Didn't get the code?".localized;
    NSString *message = @"If you did not receive the email with verification code, try sending it again to:".localized;
    
    PSPDFAlertView *contactAlert = [HGAlertView alertViewWithTitle:title andMessage:message];
    contactAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField *contactField = [contactAlert textFieldAtIndex:0];
    contactField.keyboardType = UIKeyboardTypeEmailAddress;
    contactField.text = defaultEmail; // pre-filled
    
    [contactAlert addButtonWithTitle:@"Resend code".localized block:^(NSInteger buttonIndex) {
        
        NSString *email = contactField.text;
        if (email.isValidEmail) {
            
            completion(email);
            
        } else {
            
            PSPDFAlertView *retryAlert = [HGAlertView alertViewWithTitle:@"Email format is incorrect".localized andMessage:@""];
            [retryAlert addButtonWithTitle:@"Retry".localized block:^(NSInteger buttonIndex) {
                [self askEmailWithDefaultEmail:email completion:completion];
            }];
            [retryAlert setCancelButtonWithTitle:@"Cancel".localized block:^(NSInteger buttonIndex) {
                // nop
            }];
            [retryAlert show];
        }
    }];
    [contactAlert setCancelButtonWithTitle:@"Cancel".localized block:^(NSInteger buttonIndex) {
        // nop
    }];
    [contactAlert show];
}


+ (void)showAlertWithError:(NSError *)error
{
    NSString *message = error.localizedDescription;
    if (!message.hasValue) {
        message = @"Something goes wrong.".localized;
    }
    PSPDFAlertView *alert = [HGAlertView alertViewWithTitle:@"Error".localized andMessage:message];
    [alert setCancelButtonWithTitle:@"OK".localized block:^(NSInteger buttonIndex) {
        // nop
    }];
    [alert show];
}

@end
