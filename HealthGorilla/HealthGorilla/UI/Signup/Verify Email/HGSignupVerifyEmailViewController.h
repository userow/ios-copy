//
//  HGSignupVerifyEmailViewController.h
//  HG-Project
//
//  Created by not Paul on 03/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"

typedef void (^HGSignupVerifyEmailCallback)(NSString *email);
typedef void (^HGSignupVerifyEmailCompletion)(bool cancelled, NSString *code, bool needResend, bool skipConfirmation, bool webConfirmed, HGSignupVerifyEmailCallback resendEmailCallback);

@interface HGSignupVerifyEmailViewController : UIViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupVerifyEmailCompletion)completion;

@end
