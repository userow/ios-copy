//
//  HGSignupVerifyEmailViewController.m
//  HG-Project
//
//  Created by not Paul on 03/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupVerifyEmailViewController.h"


@interface HGSignupVerifyEmailViewController ()

@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, copy) HGSignupVerifyEmailCompletion completion;
//
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) id webConfirmationObserver;
//
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UIButton *verifyLaterButton;

@end


@implementation HGSignupVerifyEmailViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupVerifyEmailCompletion)completion
{
    HGSignupVerifyEmailViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    
    return c;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (!self) return nil;
    
    __weak typeof(self) weakSelf = self;
    _webConfirmationObserver = [[NSNotificationCenter defaultCenter] addObserverForName:HGNotification.didFinishWebConfirmation object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *username = [note.object objectForKey:HGUserNameParam];
        [weakSelf onDidFinishWebConfirmationWithUserName:username];
    }];
    
    return self;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_webConfirmationObserver];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    __weak typeof(self) weakSelf = self;
    self.navigationItem.leftBarButtonItem = [HGBarButtonFactory cancelButtonWithAction:^(id sender) {
        [weakSelf tapOnCancel:nil];
    }];
    
    [self validateNextButton];
    [self updateVerifyLaterButton];
    [self updateHeader];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    self.code = str;
    
    [self validateNextButton];
    
    return YES;
}


#pragma mark - Methods

- (void)validateNextButton
{
    bool valid = self.code.hasValue;
    self.navigationItem.rightBarButtonItem.enabled = valid;
}


- (IBAction)tapOnNext:(id)sender
{
    bool cancelled = NO;
    bool needResend = NO;
    bool skipConfirmation = NO;
    bool webConfirmed = NO;
    self.completion(cancelled, self.code, needResend, skipConfirmation, webConfirmed, nil);
}


- (IBAction)tapOnNeedResend:(id)sender
{
    bool cancelled = NO;
    bool needResend = YES;
    bool skipConfirmation = NO;
    bool webConfirmed = NO;
    self.completion(cancelled, nil, needResend, skipConfirmation, webConfirmed, ^(NSString *email) {
        [self updateHeader];
    });
}


- (IBAction)tapOnVerifyLater:(id)sender
{
    bool cancelled = NO;
    bool needResend = NO;
    bool skipConfirmation = YES;
    bool webConfirmed = NO;
    self.completion(cancelled, nil, needResend, skipConfirmation, webConfirmed, nil);
}


- (void)tapOnCancel:(id)sender
{
    bool cancelled = YES;
    bool needResend = NO;
    bool skipConfirmation = NO;
    bool webConfirmed = NO;
    self.completion(cancelled, nil, needResend, skipConfirmation, webConfirmed, nil);
}


- (void)onDidFinishWebConfirmationWithUserName:(NSString *)username
{
    // need to check username?  (it's email for PT, username for MD, Staff)
    
    bool cancelled = NO;
    bool needResend = NO;
    bool skipConfirmation = NO;
    bool webConfirmed = YES;
    self.completion(cancelled, nil, needResend, skipConfirmation, webConfirmed, nil);
}


- (void)updateVerifyLaterButton
{
    bool visible = NO;
    switch (self.context.userType) {
        case HGSignupUserStaff:
        case HGSignupUserClinician:
            visible = YES;
            break;
        default:
            break;
    }
    self.verifyLaterButton.hidden = !visible;
}


- (void)updateHeader
{
    self.infoLabel.text = [NSString stringWithFormat:@"We sent verification code to your email address %@.\n\nPlease enter it here:".localized, self.context.email];
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
