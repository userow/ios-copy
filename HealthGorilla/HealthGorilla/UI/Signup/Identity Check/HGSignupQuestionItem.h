//
//  HGSignupQuestionItem.h
//  HG-Project
//
//  Created by not Paul on 29/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@protocol HGSignupQuestionItem <NSObject>
// to support array in JSONModel
@end


@interface HGSignupQuestionItem : JSONModel

@property (nonatomic, strong) NSString *prompt;
@property (nonatomic, strong) NSArray *answers;

@property (nonatomic, strong) NSNumber<Ignore> *selectionIndex;

@end
