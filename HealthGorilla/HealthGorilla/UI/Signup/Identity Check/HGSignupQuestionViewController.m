//
//  HGSignupQuestionViewController.m
//  HG-Project
//
//  Created by not Paul on 29/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupQuestionViewController.h"


@interface HGSignupQuestionViewController ()

@property (nonatomic, copy) HGSignupQuestionCompletion completion;
@property (nonatomic, strong) HGSignupQuestionItem *question;
//
@property (nonatomic, weak) IBOutlet UILabel *questionLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end


@implementation HGSignupQuestionViewController

+ (instancetype)instanceWithQuestion:(HGSignupQuestionItem *)question storyboard:(UIStoryboard *)storyboard completion:(HGSignupQuestionCompletion)completion
{
    HGSignupQuestionViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.completion = completion;
    c.question = question;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    [self updateHeader];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.question.answers.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AnswerCell"];
    
    NSString *item = self.question.answers[indexPath.row];
    cell.textLabel.text = item;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    self.completion(index);
    
    __weak typeof(self) weakSelf = self;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1; // to hide header in grouped-style table view
}

#pragma mark - Methods

- (void)updateHeader
{
    self.questionLabel.text = self.question.prompt;
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
