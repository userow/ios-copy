//
//  HGSignupIdentityCheckViewController.h
//  HG-Project
//
//  Created by not Paul on 29/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"
#import "HGSignupQuestionItem.h"

typedef void (^HGSignupIdentityCheckCompletion)(bool cancelled, NSArray<NSString *> *answers);

@interface HGSignupIdentityCheckViewController : UIViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context questions:(NSArray<HGSignupQuestionItem *> *)questions allowBack:(bool)allowBack storyboard:(UIStoryboard *)storyboard completion:(HGSignupIdentityCheckCompletion)completion;

@end
