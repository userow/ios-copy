//
//  HGSignupIdentityCheckCell.h
//  HG-Project
//
//  Created by not Paul on 06/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@interface HGSignupIdentityCheckCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *promptLabel;
@property (nonatomic, weak) IBOutlet UILabel *answerLabel;

@end
