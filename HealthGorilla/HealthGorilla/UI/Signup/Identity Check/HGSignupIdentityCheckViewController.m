//
//  HGSignupIdentityCheckViewController.m
//  HG-Project
//
//  Created by not Paul on 29/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupIdentityCheckViewController.h"
#import "HGSignupIdentityCheckCell.h"
#import "HGSignupQuestionViewController.h"


@interface HGSignupIdentityCheckViewController ()

@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, copy) HGSignupIdentityCheckCompletion completion;
@property (nonatomic, strong) NSArray<HGSignupQuestionItem *> *items;
@property (nonatomic) bool allowBack;
//
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end


@implementation HGSignupIdentityCheckViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context questions:(NSArray<HGSignupQuestionItem *> *)questions allowBack:(bool)allowBack storyboard:(UIStoryboard *)storyboard completion:(HGSignupIdentityCheckCompletion)completion
{
    HGSignupIdentityCheckViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    c.items = questions;
    c.allowBack = allowBack;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    if (!self.allowBack) {
        __weak typeof(self) weakSelf = self;
        self.navigationItem.leftBarButtonItem = [HGBarButtonFactory cancelButtonWithAction:^(id sender) {
            [weakSelf tapOnCancel:nil];
        }];
    }
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    
    [self validateNextButton];
    [self updateHeader];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGSignupIdentityCheckCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuestionCell"];
    
    HGSignupQuestionItem *item = self.items[indexPath.row];
    
    cell.promptLabel.text = item.prompt;
    cell.answerLabel.text = item.selectionIndex ? item.answers[item.selectionIndex.integerValue] : @" "; // removed red 'Tap to answer'
    cell.answerLabel.textColor = item.selectionIndex ? [UIColor blackColor] : [UIColor redColor];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGSignupQuestionItem *item = self.items[indexPath.row];

    __weak typeof(self) weakSelf = self;
    UIViewController *c = [HGSignupQuestionViewController instanceWithQuestion:item storyboard:self.storyboard completion:^(NSInteger index) {
        
        item.selectionIndex = @(index);
        
        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf validateNextButton];
    }];
    [self.navigationController pushViewController:c animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1; // to hide header in grouped-style table view
}


#pragma mark - Methods

- (void)validateNextButton
{
    __block bool valid = YES;
    [_items enumerateObjectsUsingBlock:^(HGSignupQuestionItem * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        if (nil == item.selectionIndex) {
            valid = NO;
        }
    }];
    self.navigationItem.rightBarButtonItem.enabled = valid;
}


- (void)tapOnBack // overwritten
{
    [_items enumerateObjectsUsingBlock:^(HGSignupQuestionItem * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        item.selectionIndex = nil;
    }];
    
    // 'Back' works when allowBack=YES
    [self tapOnCancel:nil];
}


- (void)tapOnCancel:(id)sender
{
    bool cancelled = YES;
    self.completion(cancelled, nil);
}


- (IBAction)tapOnNext:(id)sender
{
    NSMutableArray<NSString *> *answers = [[NSMutableArray alloc] init];
    [_items enumerateObjectsUsingBlock:^(HGSignupQuestionItem * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *answer = item.answers[item.selectionIndex.integerValue];
        [answers addObject:answer];
    }];
    bool cancelled = NO;
    self.completion(cancelled, answers);
}


- (void)updateHeader
{
    NSString *text = @"For privacy protection we need to verify your identity. Please answer the following personal questions:".localized;
    if (1 == self.items.count) {
        text = @"We could not verify your identity based on the previous answers and have to ask more:".localized;
    }
    self.infoLabel.text = text;
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
