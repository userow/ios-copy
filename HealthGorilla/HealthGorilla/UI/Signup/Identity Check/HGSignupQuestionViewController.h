//
//  HGSignupQuestionViewController.h
//  HG-Project
//
//  Created by not Paul on 29/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupQuestionItem.h"

typedef void (^HGSignupQuestionCompletion)(NSInteger index);

@interface HGSignupQuestionViewController : UIViewController

+ (instancetype)instanceWithQuestion:(HGSignupQuestionItem *)question storyboard:(UIStoryboard *)storyboard completion:(HGSignupQuestionCompletion)completion;

@end
