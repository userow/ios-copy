//
//  HGSignupSelectInvitationViewController.h
//  HG-Project
//
//  Created by not Paul on 28/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"
#import "HGSignupInvitationItem.h"

typedef void (^HGSignupSelectInvitationCompletion)();

@interface HGSignupSelectInvitationViewController : UIViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context invitations:(NSArray<HGSignupInvitationItem *> *)invitations storyboard:(UIStoryboard *)storyboard completion:(HGSignupSelectInvitationCompletion)completion;

@end
