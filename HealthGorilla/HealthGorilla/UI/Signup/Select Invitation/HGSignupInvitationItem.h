//
//  HGSignupInvitationItem.h
//  HG-Project
//
//  Created by not Paul on 28/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

@interface HGSignupInvitationItem : JSONModel

@property (nonatomic, strong) NSString *invitationId;
@property (nonatomic, strong) NSString *practiceName;

@end
