//
//  HGSignupInvitationItem.m
//  HG-Project
//
//  Created by not Paul on 28/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupInvitationItem.h"

@implementation HGSignupInvitationItem

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@ {
        @"invitationId" : @"invPendingUserId",
        @"practiceName" : @"invPracticeName",
    }];
}

@end
