//
//  HGSignupSelectInvitationViewController.m
//  HG-Project
//
//  Created by not Paul on 28/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupSelectInvitationViewController.h"


@interface HGSignupSelectInvitationViewController ()

@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, copy) HGSignupSelectInvitationCompletion completion;
@property (nonatomic, strong) NSArray<HGSignupInvitationItem *> *items;
//
@property (nonatomic, weak) IBOutlet UILabel *doctorLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end


@implementation HGSignupSelectInvitationViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context invitations:(NSArray<HGSignupInvitationItem *> *)invitations storyboard:(UIStoryboard *)storyboard completion:(HGSignupSelectInvitationCompletion)completion
{
    HGSignupSelectInvitationViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    c.items = invitations;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    [self validateNextButton];    
    [self updateHeader];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvitationCell"];
    
    HGSignupInvitationItem *item = self.items[indexPath.row];
    bool selected = [item.invitationId isEqual:self.context.invitation.invitationId];
    
    cell.textLabel.text = item.practiceName;
    cell.accessoryType = (selected) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    CGFloat fontSize = cell.textLabel.font.pointSize;
    cell.textLabel.font = (selected) ? [UIFont boldSystemFontOfSize:fontSize] : [UIFont lightSystemFontOfSize:fontSize];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HGSignupInvitationItem *item = self.items[indexPath.row];
    self.context.invitation = item;
    
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self validateNextButton];
}


#pragma mark - Methods

- (void)validateNextButton
{
    bool valid = (nil != self.context.invitation);
    self.navigationItem.rightBarButtonItem.enabled = valid;
}


- (void)tapOnBack // overwritten
{
    self.context.invitation = nil;
}


- (IBAction)tapOnNext:(id)sender
{
    self.completion();
}


- (void)updateHeader
{
    self.doctorLabel.text = [NSString stringWithFormat:@"%@, NPI %@".localized, self.context.clinician.name, self.context.clinician.npi];
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
