//
//  HGSignupDoneViewController.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupDoneViewController.h"


@interface HGSignupDoneViewController ()

@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, copy) HGSignupDoneCompletion completion;
//
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;

@end


@implementation HGSignupDoneViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupDoneCompletion)completion
{
    HGSignupDoneViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    self.navigationItem.hidesBackButton = YES;
    
    [self updateContent];
}


#pragma mark - Methods

- (IBAction)tapOnNext:(id)sender
{
    self.completion();
}


- (void)updateContent
{
    NSString *title = nil;
    NSString *message = nil;
    switch (self.context.userType) {
        case HGSignupUserPatient:
            title = @"Request Your Medical Records".localized;
            message = [NSString stringWithFormat:@"At last, get control of your health records! With %@ Clinical Network profile you can securely collect, view and share your medical history.".localized, [HGAppConfig appNameWhitelabeled]];
            break;
        case HGSignupUserClinician:
            title = @"Account confirmed".localized;
            message = [NSString stringWithFormat:@"Great job, your account is ready!\n\nNow you will be assigned to FREE plan with some restrictions of functionality. To choose a plan and setup billing please use your web account on %@ Clinical Network.".localized, [HGAppConfig appNameWhitelabeled] ];
            break;
        case HGSignupUserStaff:
            title = @"Account confirmed".localized;
            message = @"Great job, your account is ready!";
            break;
        default:
            break;
    }
    self.title = title;
    self.messageLabel.text = message;
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
