//
//  HGSignupFailedViewController.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"

typedef void (^HGSignupFailedCompletion)(bool showSupportContacts);

@interface HGSignupFailedViewController : UIViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupFailedCompletion)completion;

@end
