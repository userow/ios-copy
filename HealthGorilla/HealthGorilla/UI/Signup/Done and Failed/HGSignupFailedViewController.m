//
//  HGSignupFailedViewController.m
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupFailedViewController.h"
#import "HGHypertextLabel.h"


@interface HGSignupFailedViewController ()

@property (nonatomic, strong) HGSignupContext *context;
@property (nonatomic, copy) HGSignupFailedCompletion completion;
//
@property (nonatomic, weak) IBOutlet HGHypertextLabel *contactLabel;

@end


@implementation HGSignupFailedViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupFailedCompletion)completion
{
    HGSignupFailedViewController *c = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    c.context = context;
    c.completion = completion;
    
    return c;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    self.navigationItem.hidesBackButton = YES;
    
    [self buildContactUs];
}


#pragma mark - Methods

- (IBAction)tapOnStartOver:(id)sender
{
    bool showSupportContacts = NO;
    self.completion(showSupportContacts);
}


- (void)showSupportContacts
{
    bool showSupportContacts = YES;
    self.completion(showSupportContacts);
}


- (void)buildContactUs
{
    __weak typeof(self) weakSelf = self;
    
    _contactLabel.hypertext = _contactLabel.text;
    _contactLabel.linkTapHandler = ^(NSUInteger linkId) {
        [weakSelf showSupportContacts];
    };
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return [HGDevice isPad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([HGDevice isPad]) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

@end
