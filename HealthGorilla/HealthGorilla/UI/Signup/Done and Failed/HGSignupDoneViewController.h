//
//  HGSignupDoneViewController.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"

typedef void (^HGSignupDoneCompletion)();

@interface HGSignupDoneViewController : UIViewController

+ (instancetype)instanceWithContext:(HGSignupContext *)context storyboard:(UIStoryboard *)storyboard completion:(HGSignupDoneCompletion)completion;

@end
