//
//  HGSignupWorkflow.h
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupUserTypes.h"

typedef void (^HGSignupWorkflowCompletion)(bool cancelled, NSString *username, NSString *password);

@interface HGSignupWorkflow : NSObject

+ (UIViewController *)startTutorialWithCompletion:(HGSignupWorkflowCompletion)completion;

@end
