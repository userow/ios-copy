//
//  HGSignupWorkflow.m
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupWorkflow.h"

#import "HGSignupContext.h"

#import "HGSignupSearchClinicianViewController.h"
#import "HGSignupSelectInvitationViewController.h"
#import "HGSignupIdentityCheckViewController.h"
#import "HGSignupSSN4CheckViewController.h"

#import "HGSignupVerifyEmailWorkflow.h"

#import "HGSignupDoneViewController.h"
#import "HGSignupFailedViewController.h"
#import "HGSignupStageOneViewController.h"
#import "HGSignupStageTwoViewController.h"
#import "HGTutorialViewController.h"
#import "HGTutorialSelectUserTypeViewController.h"
#import "HGHelpViewController.h"

#import "HGIdologyScanIdViewController.h"

#import "HGApiClient+Signup.h"
#import "HGNotifications.h"

#import <SVProgressHUD/SVProgressHUD.h>

typedef NS_ENUM(NSUInteger, HGSignupCancelAction) {
    HGCancelAndDismiss,
    HGCancelAndUnwind,
};


@implementation HGSignupWorkflow

#warning success = true - is a marker that error occured in request

+ (UIViewController *)startTutorialWithCompletion:(HGSignupWorkflowCompletion)completion
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"SignupStart"];
    
    // hide navbar for tutorial screens
    nav.navigationBarHidden = YES;
    
    [self selectUserTypeWithNavigation:nav completion:completion];
    
    return nav;
}


+ (void)selectUserTypeWithNavigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    
    UIViewController *c = [HGTutorialSelectUserTypeViewController instanceWithStoryboard:nav.storyboard completion:^(bool cancelled, HGSignupUserType userType) {
        
        if (cancelled) {
            [self dismissSignupWithNavigation:navigation completion:completion];
            return; // tap on Login
        }
        [self showTutorialPagesWithUserType:userType navigation:navigation completion:completion];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)showTutorialPagesWithUserType:(HGSignupUserType)userType navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    
    UIViewController *c = [HGTutorialViewController instanceWithUserType:userType storyboard:nav.storyboard completion:^(bool cancelled) {
        
        if (cancelled) {
            [self dismissSignupWithNavigation:navigation completion:completion];
            return; // tap on Login
        }        
        
        [self startSignupWithUserType:userType navigation:navigation completion:completion];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)startSignupWithUserType:(HGSignupUserType)userType navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    HGSignupContext *context = [[HGSignupContext alloc] init];
    context.userType = userType;
    
    // unhide navbar that was hidden for tutorial screens
    nav.navigationBarHidden = NO;
    
    // follow workflow depend on user type
    switch (userType) {
        case HGSignupUserPatient:
        case HGSignupUserStaff: {
            context.unwindSegue = @"unwindPatientOrStaffToSignupStart";
            [self showFieldsOnStageOneWithContext:context navigation:nav completion:completion];
            break;
        }
        case HGSignupUserClinician: {
            context.unwindSegue = @"unwindClinicianToSignupStart";
            [self searchClinicianWithContext:context navigation:nav completion:completion];
            break;
        }
        case HGSignupUserUndefined: {
            // unexpected
            break;
        }
    }
}


+ (void)dismissSignupCancelled:(bool)cancelled username:(NSString *)username password:(NSString *)password navigation:(UINavigationController *)navigation completion:(HGSignupWorkflowCompletion)completion
{
    void (^finalizer)() = ^{
        completion(cancelled, username, password);
    };
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (navigation.presentingViewController) {
            // signup from Login screen
            [navigation dismissViewControllerAnimated:YES completion:^{
                finalizer();
            }];
        } else {
            // singup from app launch
            finalizer();
        }
    }];
}


+ (void)dismissSignupWithNavigation:(UINavigationController *)navigation completion:(HGSignupWorkflowCompletion)completion
{
    [self dismissSignupCancelled:YES username:nil password:nil navigation:navigation completion:completion];
}


+ (void)dismissSignupWithUsername:(NSString *)username password:(NSString *)password navigation:(UINavigationController *)navigation completion:(HGSignupWorkflowCompletion)completion
{
    [self dismissSignupCancelled:NO username:username password:password navigation:navigation completion:completion];
}


+ (void)unwindSignupWithSegue:(NSString *)unwindSegue navigation:(UINavigationController *)navigation
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        UIViewController *sender = navigation.topViewController;
        [sender performSegueWithIdentifier:unwindSegue sender:sender];
    }];
}


+ (void)cancelSignupWithContext:(HGSignupContext *)context action:(HGSignupCancelAction)action navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    
    void (^finalizer)() = ^{
        switch (action) {
            case HGCancelAndDismiss: // dismiss all signup screens
                [self dismissSignupWithNavigation:navigation completion:completion];
                break;
            case HGCancelAndUnwind:  // unwind to 1st signup screen
                [self unwindSignupWithSegue:context.unwindSegue navigation:navigation];
                break;
        }
    };
    
    if (!context.pendingUserId.hasValue) {
        finalizer();
        return;
    }
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD show];
    
    [[HGApiClient shared] deletePendingUserWithId:context.pendingUserId handler:^(bool success, NSError *error) {
        
        [SVProgressHUD dismiss];
        if (error || !success) {
            [self showAlertWithError:error];
            return; // error
        }
        finalizer();
    }];
}


+ (void)showFieldsOnStageOneWithContext:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    
    // show input fields of StageOne
    UIViewController *c = [HGSignupStageOneViewController instanceWithContext:context storyboard:nav.storyboard completion:^(bool cancelled, HGSignupFieldsValidationCallback validationCallback) {
        
        if (cancelled) {
            [self cancelSignupWithContext:context action:HGCancelAndDismiss navigation:navigation completion:completion];
            return; // stageOne cancelled
        }
        
        // next step to stage B (dob, zip, gender)
        [self showFieldsOnStageTwoWithContext:context navigation:navigation completion:completion];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)showFieldsOnStageTwoWithContext:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    
    // show input fields of StageTwo
    UIViewController *c = [HGSignupStageTwoViewController instanceWithContext:context storyboard:nav.storyboard completion:^(bool cancelled, HGSignupFieldsValidationCallback validationCallback) {
        
        if (cancelled) {
            // not expected case (always Back)
            return; // stageTwo cancelled
        }
        
        // try to submit account
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD show];
        
        [[HGApiClient shared]
         submitAccountWithUserType:context.userType
         npi:context.clinician.npi
         firstName:context.firstName
         middleName:context.middleName
         lastName:context.lastName
         practiceName:context.practiceName
         email:context.email
         username:context.username
         password:context.password
         gender:context.gender
         zip:context.zip
         dob:context.dob
         invitationId:context.invitation.invitationId
         handler:^(bool success, HGSignupSubmitAccountResponse *response, NSError *error) {
            
            [SVProgressHUD dismiss];
            if (error || !success) {
                [self showAlertWithError:error];
                return; // error
            }
            if (response.validationFailed) {
                validationCallback(response.validationErrors);
                return; // validation failed
            }
            if (response.patientRestricted) {
                [self showAlertWithMessage:@"We apologize but for now the service is not available in your area. We will notify you as soon as the Medical Records Request service is available in your area.".localized title:@"Patient Region Restriction".localized];
                return; // patient region restriction
            }
            if (!success) {
                [self showAlertWithError:error];
                return; // not succeeded
            }
            if (response.needAskSSN4) {
                context.pendingUserId = response.pendingUserId;
                [self askSSN4CheckWithContext:context allowBack:YES navigation:navigation completion:completion];
                return; // ask SSN4
            }
            if (response.idologySuccess && ! response.idologyScanNeeded) {
                context.pendingUserId = response.pendingUserId;
                [self askIdentityCheckWithQuestions:response.questions allowBack:YES context:context navigation:navigation completion:completion];
                return; // IDology starts
            }
          
          //IOS-891 - idology scanid
          if (response.idologyScanNeeded) {
              context.pendingUserId = response.pendingUserId;
              [self askIdentityCheckWithIdologyScanIDAllowBack:YES
                                                       context:context
                                                    navigation:navigation
                                                    completion:completion];
             
            return; // IDology Scan ID starts
          }
        }];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)searchClinicianWithContext:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
/*  A set of test methods  
 
    [self failedWithContext:context navigation:nav completion:completion];
    
    return;  // TODO: REMOVE IT


    [self finishedWithContext:context navigation:nav completion:completion];
    
    return;  // TODO: REMOVE IT


    [self askEmailVerificationCodeWithContext:context navigation:nav completion:completion];
    
    return;  // TODO: REMOVE IT


    [self askSSN4CheckWithContext:context allowBack:NO navigation:nav completion:completion];
    
    return;  // TODO: REMOVE IT

    NSArray *items = @
    [
     @ {
         @"prompt": @"When did you purchase the property at PEACHTREE PLACE?",
         @"answers": @[
                       @"Ans 1 For Q1",
                       @"Ans 2 For Q1",
                       @"Ans 3 For Q1",
                       @"None of the above",
                       ],
     },
     @ {
         @"prompt": @"In which city is ANY STREET?",
         @"answers": @[
                       @"Ans 1 For Q2",
                       @"Ans 2 For Q2",
                       @"Ans 3 For Q2",
                       @"None of the above",
                       ],
     },
     @ {
         @"prompt": @"At which of the following addresses have you lived?",
         @"answers": @[
                       @"Ans 1 For Q3",
                       @"Ans 2 For Q3",
                       @"Ans 3 For Q3",
                       @"None of the above",
                       ],
     },
     @ {
         @"prompt": @"When did you purchase or lease your Ford Expedition?",
         @"answers": @[
                       @"Ans 1 For Q4",
                       @"Ans 2 For Q4",
                       @"Ans 3 For Q4",
                       @"None of the above",
                       ],
     },
    ];
    NSError *error = nil;
    NSArray *questions = [HGSignupQuestionItem arrayOfModelsFromDictionaries:items error:&error];
    if (error) {
        NSLog(@"Error: %@", error);
        return;
    }
    [self askIdentityCheckWithQuestions:questions allowBack:NO context:context navigation:nav completion:completion];
    
    return;  // TODO: REMOVE IT
*/
    __weak UINavigationController *navigation = nav;
    UIViewController *c = [HGSignupSearchClinicianViewController instanceWithContext:context storyboard:nav.storyboard completion:^ (bool cancelled) {
        
        if (cancelled) {
            [self cancelSignupWithContext:context action:HGCancelAndDismiss navigation:navigation completion:completion];
            return; // cancelled
        }
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD show];
        
        [[HGApiClient shared] checkNPI:context.clinician.npi handler:^(bool registered, NSArray<HGSignupInvitationItem *> *items, NSError *error) {
            
            [SVProgressHUD dismiss];
            if (error) {
                [self showAlertWithError:error];
                return; // error
            }
            if (registered) {
                [self showAlertWithMessage:@"Account for this clinician already exists. Please login or search again.".localized];
                return; // already registered
            }
            if (0 == items.count) {
                
                // show fields to sign up as Clinician w/o invitations
                [self showFieldsOnStageOneWithContext:context navigation:navigation completion:completion];
                
            } else if (1 == items.count) {
                
                // show fields to sign up as Clinician with single intitations
                context.invitation = items.firstObject;
                [self showFieldsOnStageOneWithContext:context navigation:navigation completion:completion];
                
            } else if (items.count > 1) {
                
                // need to select invitation
                [self selectInvitation:items context:context navigation:navigation completion:completion];
            }
        }];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)selectInvitation:(NSArray<HGSignupInvitationItem *> *)invitations context:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    UIViewController *c = [HGSignupSelectInvitationViewController instanceWithContext:context invitations:invitations storyboard:nav.storyboard completion:^{
    
        // show fields to sign up as Clinician with selected invitation
        [self showFieldsOnStageOneWithContext:context navigation:navigation completion:completion];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)askIdentityCheckWithQuestions:(NSArray<HGSignupQuestionItem *> *)questions allowBack:(bool)allowBack context:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    
    __weak UINavigationController *navigation = nav;
    UIViewController *c = [HGSignupIdentityCheckViewController instanceWithContext:context questions:questions allowBack:allowBack storyboard:nav.storyboard completion:^(bool cancelled, NSArray<NSString *> *answers) {
        
        if (cancelled) {
            if (allowBack) {
                // tap on 'Back' button
            } else {
                // unwind to 1st signup screen
                [self cancelSignupWithContext:context action:HGCancelAndUnwind navigation:navigation completion:completion];
            }
            return; // identity check cancelled, step back or rollback to start
        }
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD show];
        
        [[HGApiClient shared] submitIdentityCheckAnswers:answers
                                           pendingUserId:context.pendingUserId
                                                 handler:^(bool success,
                                                           bool idologySuccess,
                                                           bool needExtraChallenge,
                                                           NSArray<HGSignupQuestionItem *> *questions,
                                                           bool needEmailConfirmation,
                                                           NSString *email,
                                                           NSError *error) {

            [SVProgressHUD dismiss];
            if (error || !success) {
                // Registration terminated at this step as failed if server returns an error with code 505
                if (505 == error.code) {
                    [self failedWithContext:context navigation:navigation completion:completion];
                    return; // special code 505
                }
                [self showAlertWithError:error];
                return; // error
            }
            if (idologySuccess) {
                // user passed IDology
                if (needEmailConfirmation) {
                    [self askEmailVerificationCodeWithContext:context navigation:navigation completion:completion];
                } else {
                    // signup w/o email confirmation, ready to auto-login
                    [self finishedWithContext:context navigation:navigation completion:completion];
                }
            } else if (needExtraChallenge) {
                // extra identity check with one more question
                [self askIdentityCheckWithQuestions:questions allowBack:NO context:context navigation:navigation completion:completion];
            }
        }];
    }];
    
    [nav pushViewController:c animated:YES];
}

#pragma mark - my impact

+ (void)askIdentityCheckWithIdologyScanIDAllowBack:(bool)allowBack
                                           context:(HGSignupContext *)context
                                        navigation:(UINavigationController *)nav
                                        completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;

    [SVProgressHUD dismiss];
    UIViewController *c = [HGIdologyScanIdViewController instanceWithContext:context
                                                                  storyboard:navigation.storyboard
                                                                  completion:^(bool cancelled,
                                                                               HGSignupFieldsValidationCallback validationCallback) {
                                                                      
    if (cancelled) {
      if (allowBack) {
        // tap on 'Back' button
      } else {
        // unwind to 1st signup screen
        [self cancelSignupWithContext:context action:HGCancelAndUnwind navigation:navigation completion:completion];
      }
      return; // identity check cancelled, step back or rollback to start
    }

    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD show];

    [[HGApiClient shared] performIdScanActionWithPendingUserId:context.pendingUserId
                                                   documentType:context.documentType
                                                 refreshRequest:context.refreshRequest
                                                scanFrontBase64: nil //context.scanFrontBase64
                                                 scanBackBase64: nil //context.scanBackBase64
                                                        handler:^(bool success, HGPerformIdScanActionResponse *response, NSError *error) {
/*
 Response parameters:
 idscanSuccess    boolean    If true then ID scan request accepted by IDology and polling results can be started;   false if need terminate signup
 success    boolean    If true then validation passed and no errors occurred
 errorMessage    String    Error message occured on server side
 */
                                                            [SVProgressHUD dismiss];
                                                            if (error || !success || !response.idscanSuccess) {
                                                                // Registration terminated at this step as failed if server returns an error with code 505
                                                                if (505 == error.code) {
                                                                    [context clearImages];
                                                                    [self failedWithContext:context navigation:navigation completion:completion];
                                                                    return; // special code 505
                                                                }
                                                                [self showAlertWithError:error];
                                                                return; // error
                                                            }
                                                                                         
      if (success && response.idscanSuccess) {
          // user uploaded IDology - starting polling !
          [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
          [SVProgressHUD show];
          dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
          __block dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, aQueue);
          dispatch_source_set_timer(timer, DISPATCH_TIME_NOW + 2*NSEC_PER_SEC, 2 * NSEC_PER_SEC, 2 * NSEC_PER_SEC);
          dispatch_source_set_event_handler(timer, ^{
              
              /*
               Since API v2.95
               
               Sign-Up: this public method is used for polling ID Scan Identity Check results. Should be be invoked periodically - recommended interation interval is 2 sec.
               
               In request should be placed required parameter 'pendingUserId' from successfull submitAccountInfo response.
               
               If 'idscanPending' is true then it means that results are not ready - do nothing and repeat at next iteration
               
               If 'idscanObsolete' is true then it means that current request is obsolete and further invocations should be stopped.
               
               If 'idologySuccess' is true then it means user passed IDology and user account is created (with unconfirmed email if any).
               
               If identification failed (means supplied scans not passed verification) then not empty 'errorMessage' provided without throwing error 505 and performIdScanAction can be repeated with refreshRequest = true in request. Possible error messages for this case:
               
               "Your ID Scan request is expired or time out exceeded. Please try again.",
               
               "Supplied images are not readable. Please try with another.",
               
               "Supplied documents are not approved. Please try with another.".
               
               If no email confirmation required (emailConfirmNeeded = false) then user user should be sent directly into portal.
               
               If email was specified on first step then email with confirmation code was sent to provided address (field 'email' in response) - client should ask code from user and invoke submitEmailConfirmation next.
               
               Response may have loginResponse parameter (same LoginResponse as for login method) that means that authorized session already created.
               
               Note: Registration terminated at this step as failed if server returns an error with code 505
               
               Method name: performIdScanPollingResultsAction
               */
              [[HGApiClient shared] performScanIdResultPollingResultAction:context.pendingUserId
                                                                 deviceUID:nil
                                                                appVersion:nil
                                                                   handler:^(bool success,
                                                                    HGPerformIdScanPollingResultsActionResponse *response,
                                                                    NSError *error)
              {
                  [SVProgressHUD dismiss];
                  //current request is obsolete and further invocations should be stopped.
                  if (response.idscanObsolete) {
                      if (timer) {
                          dispatch_source_cancel(timer);
                          timer = nil;
                      }
                      return;
                  }
                  
                  if (error || !success || !response.idologySuccess) {
                      // Registration terminated at this step as failed if server returns an error with code 505
                      if (505 == error.code) {
                          [context clearImages];
                          [self failedWithContext:context navigation:navigation completion:completion];
                          return; // special code 505
                      }
                      
                      context.refreshRequest = @YES;
                      
                      [self showAlertWithError:error];
                      return; // error
                  }
                  
                  if (response.idologySuccess) {
                      // user passed IDology
                      if (response.emailConfirmNeeded) {
                          [self askEmailVerificationCodeWithContext:context navigation:navigation completion:completion];
                      } else {
                          // signup w/o email confirmation, ready to auto-login
                          [self finishedWithContext:context navigation:navigation completion:completion];
                      }
                  }
              }];
          });
          dispatch_resume(timer);
      }
    }];
  }];

  [nav pushViewController:c animated:YES];
}


+ (void)askContactNumberWithCompletion:(void (^)(NSString *contactNumber))completion
{
    NSString *title = @"Need help?".localized;
    NSString *message = @"If you're having difficulties with verification process, please leave us your contact number and we'll call you back to help.".localized;
    
    PSPDFAlertView *contactAlert = [HGAlertView alertViewWithTitle:title andMessage:message];
    contactAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField *contactField = [contactAlert textFieldAtIndex:0];
    contactField.keyboardType = UIKeyboardTypePhonePad;
    contactField.text = nil; // empty
    
    [contactAlert addButtonWithTitle:@"Contact me".localized block:^(NSInteger buttonIndex) {
        
        NSString *phoneNumber = contactField.text;
        if ([phoneNumber isValidPhoneWithDigitsCount:10]) {
            
            completion(phoneNumber);
            
        } else {
            
            PSPDFAlertView *retryAlert = [HGAlertView alertViewWithTitle:@"Phone number is incorrect".localized andMessage:@"Please use 10-digits format.".localized];
            [retryAlert addButtonWithTitle:@"Retry".localized block:^(NSInteger buttonIndex) {
                [self askContactNumberWithCompletion:completion];
            }];
            [retryAlert setCancelButtonWithTitle:@"Cancel".localized block:^(NSInteger buttonIndex) {
                // nop
            }];
            [retryAlert show];
        }
    }];
    [contactAlert setCancelButtonWithTitle:@"Cancel".localized block:^(NSInteger buttonIndex) {
        // nop
    }];
    [contactAlert show];
}


+ (void)askEmailVerificationCodeWithContext:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    UIViewController *c = [HGSignupVerifyEmailWorkflow controllerWithContext:context storyboard:nav.storyboard completion:^(bool cancelled) {
        
        if (cancelled) {
            [self cancelSignupWithContext:context action:HGCancelAndDismiss navigation:navigation completion:completion];
            return; // confirmation cancelled
        }
        // email confirmed (with code or URL received in email)
        [self finishedWithContext:context navigation:navigation completion:completion];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)finishedWithContext:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    NSString *password = context.password;
    NSString *username = nil;
    
    switch (context.userType) {
        case HGSignupUserClinician:
        case HGSignupUserStaff:
            username = context.username;
            break;
        case HGSignupUserPatient:
            username = context.email;
        default:
            break;
    }
    
    __weak UINavigationController *navigation = nav;
    UIViewController *c = [HGSignupDoneViewController instanceWithContext:context storyboard:nav.storyboard completion:^{
        
        // dismiss signup workflow, then auto-login
        [self dismissSignupWithUsername:username password:password navigation:navigation completion:completion];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)failedWithContext:(HGSignupContext *)context navigation:(UINavigationController *)nav completion:(HGSignupWorkflowCompletion)completion
{
    __weak UINavigationController *navigation = nav;
    UIViewController *c = [HGSignupFailedViewController instanceWithContext:context storyboard:nav.storyboard completion:^(bool showSupportContacts) {
        
        if (showSupportContacts) {
            UIViewController *vc = [HGHelpViewController instance];
            [nav pushViewController:vc animated:YES];
            return; // show HG contacts
        }
        
        // mark context as failed (to show explaining text in Sign Up screen)
        context.failedAndStartedOver = YES;
        
        // unwind to 1st signup screen
        [self cancelSignupWithContext:context action:HGCancelAndUnwind navigation:navigation completion:completion];
    }];
    
    [nav pushViewController:c animated:YES];
}


+ (void)showAlertWithError:(NSError *)error
{
    NSString *message = error.localizedDescription;
    if (!message.hasValue) {
        message = @"Something goes wrong.".localized;
    }
    PSPDFAlertView *alert = [HGAlertView alertViewWithTitle:@"Error".localized andMessage:message];
    [alert setCancelButtonWithTitle:@"OK".localized block:^(NSInteger buttonIndex) {
        // nop
    }];
    [alert show];
}


+ (void)showAlertWithMessage:(NSString *)message
{
    [self showAlertWithMessage:message title:@"Error".localized];
}


+ (void)showAlertWithMessage:(NSString *)message title:(NSString *)title
{
    PSPDFAlertView *alert = [HGAlertView alertViewWithTitle:title andMessage:message];
    [alert setCancelButtonWithTitle:@"OK".localized block:^(NSInteger buttonIndex) {
        // nop
    }];
    [alert show];
}

@end
