//
//  HGSignupContext.m
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupContext.h"
#import "NSData+Base64.h"
#import "UIImage+Resize.h"

@implementation HGSignupContext

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _userType = HGSignupUserUndefined;
    _refreshRequest = @NO;
    
    return self;
}

//- (void)setScanBackImage:(UIImage *)scanBackImage {
//    _scanBackImage = scanBackImage;
//    _scanBackBase64 = [scanBackImage base64StringFromImage];
//}
//
//- (void)setScanFrontImage:(UIImage *)scanFrontImage {
//    _scanFrontImage = scanFrontImage;
//    _scanFrontBase64 = [scanFrontImage base64StringFromImage];
//}

- (void)setScanFrontBase64:(NSString *)scanBase64 {
    [self setBase64:scanBase64 forType:HGIdologyImageTypeFront];
}

- (void)setScanBackBase64:(NSString *)scanBase64 {
    [self setBase64:scanBase64 forType:HGIdologyImageTypeBack];
}

- (void)setBase64:(NSString*)scanBase64 forType:(HGIdologyImageType)type
{
    if (type == HGIdologyImageTypeUnknown) return;
    
    if (scanBase64 == nil) {
        if (type == HGIdologyImageTypeFront) {
            _scanFrontImage = nil;
            _scanFrontBase64 = nil;
        } else if (type == HGIdologyImageTypeBack) {
            _scanBackImage = nil;
            _scanBackBase64 = nil;
        }
        
        return;
    }
    
    UIImage *img = [UIImage imageFromBase64:scanBase64];
    
    if (img) {
        BOOL isPortrait = img.size.width < img.size.height;
        
        CGFloat largerSize = isPortrait ? img.size.height : img.size.width;
        CGFloat smallerSize = isPortrait ? img.size.width : img.size.height;
        
        
        
        UIImage *photo = img;

        NSString *photoBase64 = scanBase64;
        
        if (largerSize > [HGAppConfig maxImageWidth] || smallerSize > [HGAppConfig maxImageHeight]) {
            CGSize size = isPortrait ? CGSizeMake([HGAppConfig recommendedImageHeight], [HGAppConfig recommendedImageWidth]) :
            CGSizeMake([HGAppConfig recommendedImageWidth], [HGAppConfig recommendedImageHeight]);
            
            photo = [img resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                              bounds:size
                                interpolationQuality:kCGInterpolationDefault];

//            photo = [img reduceFitToSize:reducedSize interpolationQuality:kCGInterpolationDefault];
            
            photoBase64 = [photo base64StringFromImage];
        }
        
        //done: add an option to reduce quality in case if image exceed 5MB
        //CGFloat compressionQuality = 0.8; // JPEG compression
        //NSData *imageData = UIImageJPEGRepresentation(photo, compressionQuality);
        
        NSData *imageData = UIImageJPEGRepresentation(photo, [HGAppConfig recommendedCompressionPercent]);

        CGFloat compressionQuality = [HGAppConfig recommendedCompressionPercent];
        
        while(imageData.length > [HGAppConfig maxImageDataSize]) {
            compressionQuality -= 0.05f;
            
            imageData = UIImageJPEGRepresentation(photo, compressionQuality);
        }
        
        photo = [UIImage imageWithData:imageData];
        photoBase64 = [imageData base64EncodedString];
        
        if (photo) {
            if (type == HGIdologyImageTypeFront) {
                _scanFrontImage = photo;
                _scanFrontBase64 = photoBase64;
            } else if (type == HGIdologyImageTypeBack) {
                _scanBackImage = photo;
                _scanBackBase64 = photoBase64;
            }
        }
    }
}

- (void)clearAllFields
{
    _email = nil;
    _firstName = nil;
    _middleName = nil;
    _lastName = nil;
    _practiceName = nil;
    _username = nil;
    _password = nil;
    _zip = nil;
    _dob = nil;
    _gender = nil;
    
    _documentType = nil;
    _scanFrontBase64 = nil;
    _scanBackBase64 = nil;
    
    _refreshRequest = @NO;
    
    _scanFrontImage = nil;
    _scanBackImage = nil;
    
    _scanBackExternalId = nil;
    _scanFrontExternalId = nil;
}

- (void)clearImages;
{
    _documentType = nil;
    _scanFrontBase64 = nil;
    _scanBackBase64 = nil;
    
    _scanFrontImage = nil;
    _scanBackImage = nil;
    
    _scanBackExternalId = nil;
    _scanFrontExternalId = nil;
}

@end
