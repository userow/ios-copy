//
//  HGApiClient+Signup.h
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGApiClient.h"
#import "HGSignupSearchClinicianItem.h"
#import "HGSignupInvitationItem.h"
#import "HGSignupQuestionItem.h"
#import "HGSignupSubmitAccountResponse.h"
#import "HGSignupUserTypes.h"
#import "HGPerformIdScanActionResponse.h"
#import "HGPerformIdScanPollingResultsActionResponse.h"


typedef void (^HGSignupSearchClinicianHandler)(NSArray<HGSignupSearchClinicianItem *> *items, NSError *error);
typedef void (^HGSignupCheckNPIHandler)(bool registered, NSArray<HGSignupInvitationItem *> *items, NSError *error);
typedef void (^HGSignupSubmitIdentityCheckHandler)(bool success, bool idologySuccess, bool needExtraChallenge, NSArray<HGSignupQuestionItem *> *questions, bool needEmailConfirmation, NSString *email, NSError *error);
typedef void (^HGSignupSubmitSSN4Handler)(bool success, bool idologySuccess, NSArray<HGSignupQuestionItem *> *questions, NSError *error);
typedef void (^HGSignupSubmitAutoLoginHandler)(bool success, NSError *error); 
typedef void (^HGSignupCheckInUserHandler)(bool success, NSString *message, NSError *error);
typedef void (^HGSignupSimpleHandler)(bool success, NSError *error);
typedef void (^HGSignupSubmitAccountHandler)(bool success, HGSignupSubmitAccountResponse *response, NSError *error);
typedef void (^HGPerformIdScanActionResponseHandler)(bool success, HGPerformIdScanActionResponse *response, NSError *error);
typedef void (^HGPerformIdScanPollingActionResponseHandler)(bool success, HGPerformIdScanPollingResultsActionResponse *response, NSError *error);


@interface HGApiClient (Signup)

- (void)searchClinitian:(NSString *)npiOrName
                handler:(HGSignupSearchClinicianHandler)handler;

- (void)checkInUseEmail:(NSString *)email
                handler:(HGSignupCheckInUserHandler)handler;

- (void)checkInUseUsername:(NSString *)username
                   handler:(HGSignupCheckInUserHandler)handler;

- (void)checkNPI:(NSString *)npi
         handler:(HGSignupCheckNPIHandler)handler;

- (void)submitIdentityCheckAnswers:(NSArray<NSString *> *)answers
                     pendingUserId:(NSString *)pendingUserId
                           handler:(HGSignupSubmitIdentityCheckHandler)handler;

- (void)submitSSN4Code:(NSString *)code
         pendingUserId:(NSString *)pendingUserId
               handler:(HGSignupSubmitSSN4Handler)handler;

- (void)submitContactMe:(NSString *)phone
          pendingUserId:(NSString *)pendingUserId
                handler:(HGSignupSimpleHandler)handler;

- (void)submitResendCode:(NSString *)email
                 handler:(HGSignupSimpleHandler)handler;

- (void)submitEmailConfirmation:(NSString *)code
               skipConfirmation:(bool)skipConfirmation
                        handler:(HGSignupSimpleHandler)handler;
/*
- (void)autoLoginAfterEmailConfirmation:(bool)afterEmailConfirmation
                                handler:(HGSignupSubmitAutoLoginHandler)handler;
*/

- (void)deletePendingUserWithId:(NSString *)pendingUserId
                        handler:(HGSignupSimpleHandler)handler;

- (void)submitAccountWithUserType:(HGSignupUserType)userType
                              npi:(NSString *)npi
                        firstName:(NSString *)firstName
                       middleName:(NSString *)middleName
                         lastName:(NSString *)lastName
                     practiceName:(NSString *)practiceName
                            email:(NSString *)email
                         username:(NSString *)username
                         password:(NSString *)password
                           gender:(NSString *)gender
                              zip:(NSString *)zip
                              dob:(NSString *)dob
                     invitationId:(NSString *)invitationId
                          handler:(HGSignupSubmitAccountHandler)handler;

/**
 Sign-Up: this public method is used to submit ID Scan request to IDology. If required fileds are missing of registartion can not be continue then error message returned in response. If successfull and idscanSuccess = true then it means that request was accepted and polling results can be started - see performIdScanPollingResultsAction. Note that front/back scans of document can be sent directly in request as base64 encoded strings or uploaded before (see Upload File) with additional parametrs in file upload request:  jquery-file-upload=true and jquery-file-upload-process-id = signup_idscan_front_%pendingUserId% / signup_idscan_back_%pendingUserId%
 
 Note: Registration terminated at this step if all required parameters are supplied but idscanSuccess = false in response.
 
 @param pendingUserId   String    Required - can be obtained from submittAccountInfo response
 @param documentType    String    Required. [driverLicense - Driver's License, idCard - ID Card, passport - Passport]
 @param refreshRequest    boolean    Optional - should be set to TRUE for next performIdScanAction if previous performIdScanPollingResultsAction reposnse failed Identity check but without error with code 505 (e.g. if supplied scans were invalid but next try with another scans are possible )
 @param scanFrontBase64    String    Optional - front scan image should be sent either in this request or uploaded before - see Upload File. NOTE: upload request should have these parameters: jquery-file-upload=true and jquery-file-upload-process-id = signup_idscan_front_%pendingUserId% (e.g. signup_idscan_front_1f054f5b303bdd25ca53ad2d)
 @param scanBackBase64    String    Optional - back scan image
 */
- (void)performIdScanActionWithPendingUserId:(NSString *)pendingUserId
                                 documentType:(NSString *)documentType
                               refreshRequest:(NSNumber *)refreshRequest
                              scanFrontBase64:(NSString *)scanFrontBase64
                               scanBackBase64:(NSString *)scanBackBase64
                                      handler:(HGPerformIdScanActionResponseHandler)handler;


/**
 Sign-Up: this public method is used for polling ID Scan Identity Check results. Should be be invoked periodically - recommended interation interval is 2 sec.
 
 @param pendingUserId   - String - Required - taken from submitAccountInfo response
 @param deviceUid       - String - Required if session should be created after Identity check
 @param appVersion      - String - Required if session should be created after Identity check
 */
- (void)performScanIdResultPollingResultAction:(NSString *)pendingUserId
                                     deviceUID:(NSString *)deviceUID
                                    appVersion:(NSString *)appVersion
                                       handler:(HGPerformIdScanPollingActionResponseHandler)handler;

@end
