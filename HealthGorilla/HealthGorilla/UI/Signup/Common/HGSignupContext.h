//
//  HGSignupContext.h
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGSignupSearchClinicianItem.h"
#import "HGSignupInvitationItem.h"
#import "HGSignupUserTypes.h"
#import "HGSignupStages.h"


@interface HGSignupContext : NSObject

@property (nonatomic) HGSignupUserType userType;
@property (nonatomic, strong) HGSignupSearchClinicianItem *clinician;
@property (nonatomic, strong) HGSignupInvitationItem *invitation;
@property (nonatomic, strong) NSString *pendingUserId;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *middleName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *practiceName;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password; //??? password in plaintext ?!
@property (nonatomic, strong) NSString *zip;
@property (nonatomic, strong) NSString *dob;
@property (nonatomic, strong) NSString *gender;
//
@property (nonatomic) bool failedAndStartedOver;
@property (nonatomic, strong) NSString *unwindSegue;

//idology ScanID
@property (nonatomic, strong) NSNumber *refreshRequest;

@property (nonatomic, strong) NSString *documentType; //[driverLicense - Driver's License, idCard - ID Card, passport - Passport]
@property (nonatomic, strong) NSString *scanBackBase64;
@property (nonatomic, strong) NSString *scanFrontBase64;

@property (nonatomic, readonly, strong) UIImage *scanBackImage;
@property (nonatomic, readonly, strong) UIImage *scanFrontImage;

@property (nonatomic, strong) NSString *scanBackExternalId;
@property (nonatomic, strong) NSString *scanFrontExternalId;

- (void)clearAllFields;

- (void)clearImages;

@end
