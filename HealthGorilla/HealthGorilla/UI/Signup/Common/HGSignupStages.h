//
//  HGSignupStages.h
//  HG-Project
//
//  Created by not Paul on 28/11/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

typedef NS_ENUM(NSInteger, HGSignupStage) {
    HGSignupStageUndefined,
    HGSignupStageOne, // ask for all names, email, password
    HGSignupStageTwo, // ask for zip, dob, gender
    HGSignupStageIdology, //asks for foto of Driver's Licence/ID Card/Passport - Mexican only!
};
