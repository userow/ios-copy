//
//  HGApiClient+Signup.m
//  HG-Project
//
//  Created by not Paul on 27/09/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

#import "HGApiClient+Signup.h"
#import "NSBundle+HG.h"
#import "HGGender.h"


@implementation HGApiClient (Signup)

+ (NSError *)errorFromResponse:(NSDictionary *)response
{
    NSString *errorMessage = response[@"errorMessage"];
    if (!errorMessage.hasValue) {
        errorMessage = @"Unknown error.".localized;
    }
    return [NSError errorWithDomain:HGErrorDomain code:0 userInfo:@ {
        NSLocalizedDescriptionKey : errorMessage,
    }];
}

- (void)searchClinitian:(NSString *)npiOrName
                handler:(HGSignupSearchClinicianHandler)handler
{
    id parameters = npiOrName;
    [self POST:HGMethod.searchNPI parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(nil, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(nil, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        NSArray *list = response[@"npiList"];
        NSArray *items = [HGSignupSearchClinicianItem arrayOfModelsFromDictionaries:list error:&error];
        handler(items, error);
    }];
}


- (void)checkInUseEmail:(NSString *)email
                handler:(HGSignupCheckInUserHandler)handler
{
    [self checkInUseValue:email valueType:0 handler:handler];
}


- (void)checkInUseUsername:(NSString *)username
                   handler:(HGSignupCheckInUserHandler)handler
{
    [self checkInUseValue:username valueType:1 handler:handler];
}


- (void)checkInUseValue:(NSString *)value
              valueType:(NSInteger)valueType
                handler:(HGSignupCheckInUserHandler)handler
{
    NSDictionary *parameters = @ {
        @"elemType" : @(valueType),
        @"elemValue" : value,
    };
    [self POST:HGMethod.checkInUseAndFormat parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, nil, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        handler(success.boolValue, response[@"errorMessage"], nil);
    }];
}


- (void)checkNPI:(NSString *)npi
         handler:(HGSignupCheckNPIHandler)handler
{
    NSDictionary *parameters = @ {
        @"npi" : npi,
    };
    [self POST:HGMethod.checkNPI parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, nil, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, nil, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        NSNumber *registered = response[@"alreadyRegistered"];
        if (registered.boolValue) {
            handler(YES, nil, error);
            return; // already registered
        }
        NSArray *list = response[@"invList"];
        NSArray *items = [HGSignupInvitationItem arrayOfModelsFromDictionaries:list error:&error];
        handler(NO, items, error);
    }];
}


- (void)submitIdentityCheckAnswers:(NSArray<NSString *> *)answers
                     pendingUserId:(NSString *)pendingUserId
                           handler:(HGSignupSubmitIdentityCheckHandler)handler
{
    NSDictionary *parameters = @ {
        @"pendingUserId" : pendingUserId,
        @"userAnswers" : answers,
        HGAppIdParam : HGPortalAppID,
        HGDeviceUidParam : [HGUser deviceUid],
    };
    [self POST:HGMethod.submitIdentityCheck parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, NO, NO, nil, NO, nil, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, NO, NO, nil, NO, nil, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        NSNumber *idologySuccess = response[@"idologySuccess"];
        if (idologySuccess.boolValue) {
            NSNumber *needEmailConfirmation = response[@"emailConfirmNeeded"];
            NSString *email = response[@"email"];
            handler(YES, YES, NO, nil, needEmailConfirmation.boolValue, email, error);
            return; // IDology succeeded
        }
        NSNumber *needExtraChallenge = response[@"challenge"];
        if (needExtraChallenge.boolValue) {
            NSArray *list = response[@"questions"];
            NSArray *questions = [HGSignupQuestionItem arrayOfModelsFromDictionaries:list error:&error];
            handler(YES, NO, YES, questions, NO, nil, error);
            return; // need extra challenge
        }
        // unexpected case
        handler(YES, NO, NO, nil, NO, nil, error);
    }];
}


- (void)submitSSN4Code:(NSString *)code
         pendingUserId:(NSString *)pendingUserId
               handler:(HGSignupSubmitSSN4Handler)handler
{
    NSDictionary *parameters = @ {
        @"pendingUserId" : pendingUserId,
        @"ssn4" : code,
    };
    [self POST:HGMethod.submitFindBySSN4 parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, NO, nil, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, NO, nil, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        NSNumber *idologySuccess = response[@"idologySuccess"];
        if (idologySuccess.boolValue) {
            NSArray *list = response[@"questions"];
            NSArray *questions = [HGSignupQuestionItem arrayOfModelsFromDictionaries:list error:&error];
            handler(YES, YES, questions, error);
            return; // IDology succeeded, need challenge
        }
        // IDology not succeeded
        handler(YES, NO, nil, error);
    }];
}


- (void)submitContactMe:(NSString *)phone
          pendingUserId:(NSString *)pendingUserId
                handler:(HGSignupSimpleHandler)handler
{
    NSDictionary *parameters = @ {
        @"pendingUserId" : pendingUserId,
        @"contactNumber" : phone,
    };
    [self POST:HGMethod.submitContactMe parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        handler(YES, error);
    }];
}


- (void)submitResendCode:(NSString *)email
                 handler:(HGSignupSimpleHandler)handler
{
    NSDictionary *parameters = @ {
        @"confirmEmail" : email,
    };
    [self POST:HGMethod.submitResendCode parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        NSNumber *emailConfirmNeeded = response[@"emailConfirmNeeded"];
        if (!emailConfirmNeeded.boolValue) {
            handler(NO, error);
            return; // not sent
        }
        handler(YES, error);
    }];
}


- (void)submitEmailConfirmation:(NSString *)code
               skipConfirmation:(bool)skipConfirmation
                        handler:(HGSignupSimpleHandler)handler
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:@ {
        @"skipConfirmation" : @(skipConfirmation),
    }];
    if (!skipConfirmation) {
        [parameters addEntriesFromDictionary:@ {
            @"confirmationCode" : code,
        }];
    }
    [self POST:HGMethod.submitEmailConfirmation parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        handler(YES, error);
    }];
}

/*
- (void)autoLoginAfterEmailConfirmation:(bool)afterEmailConfirmation
                                handler:(HGSignupSubmitAutoLoginHandler)handler
{
    // actually, this method is not used in signup workflow
    
    NSDictionary *parameters = @ {
        @"useRegCodeAsPassword" : @(afterEmailConfirmation),
        HGAppIdParam : HGPortalAppID,
        HGDeviceUidParam : [HGUser deviceUid],
    };
    [self POST:HGMethod.submitAutoLogin parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        
        // TODO: handle LoginResponse
        
        handler(YES, error);
    }];
}
*/

- (void)deletePendingUserWithId:(NSString *)pendingUserId
                        handler:(HGSignupSimpleHandler)handler
{
    id parameters = pendingUserId;
    [self POST:HGMethod.submitDeletePendingUser parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, error);
            return; // error
        }
        NSNumber *success = response[@"success"];
        if (!success.boolValue) {
            handler(NO, [HGApiClient errorFromResponse:response]);
            return; // not succeeded
        }
        handler(YES, error);
    }];
}


- (void)submitAccountWithUserType:(HGSignupUserType)userType
                              npi:(NSString *)npi
                        firstName:(NSString *)firstName
                       middleName:(NSString *)middleName
                         lastName:(NSString *)lastName
                     practiceName:(NSString *)practiceName
                            email:(NSString *)email
                         username:(NSString *)username
                         password:(NSString *)password
                           gender:(NSString *)gender
                              zip:(NSString *)zip
                              dob:(NSString *)dob
                     invitationId:(NSString *)invitationId
                          handler:(HGSignupSubmitAccountHandler)handler
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:@ {
        @"regUserType" : @(userType),
        @"password" : password,
        @"zip" : zip,
        @"rd" : @"ios",
        
        @"scheme" : [NSBundle URLScheme], // server will build URL using this scheme
    }];
    
    //for Angeles Clinic
    if ([HGAppConfig isAngelesClinical]) {
        parameters[@"countryCode"] = @"MEX";
    }
    parameters[@"dob"] = ({
        HGDate *date = [HGDate dobWithString:dob];
        @ {
            HGYearField : date.year,
            HGMonthField : date.month,
            HGDayField : date.day,
        };
    });
    if (npi.hasValue) { // Clinician
        parameters[@"npi"] = npi;
    }
    if (firstName.hasValue) { // Staff/Patient
        parameters[@"firstname"] = firstName;
    }
    if (lastName.hasValue) { // Staff/Patient
        parameters[@"lastname"] = lastName;
    }
    if (middleName.hasValue) { // Optional - mostly used for mexican patients for better matching
        parameters[@"middlename"] = middleName;
    }
      
    if (practiceName.hasValue) { // Staff
        parameters[@"practiceName"] = practiceName;
    }
    if (email.hasValue) { // Patient
        parameters[@"email"] = email;
    }
    if (username.hasValue) { // Clinician/Staff
        parameters[@"username"] = username;
    }
    if (gender.hasValue) { // Patient
        parameters[@"gender"] = [HGGender isMale:gender] ? @"male" : @"female" ; // API requires lowercase gender
    }
    if (invitationId.hasValue) { // Clinician, if there are several invitations
        parameters[@"selectedInvitationId"] = invitationId;
    }
    [self POST:HGMethod.submitAccountInfo parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, nil, error);
            return; // error
        }
        NSString *errorMessage = response[@"errorMessage"];
        if (errorMessage.hasValue) {
            handler(NO, nil, [HGApiClient errorFromResponse:response]);
            return; // error
        }
        HGSignupSubmitAccountResponse *model = [[HGSignupSubmitAccountResponse alloc] initWithDictionary:response error:&error];
        if (error) {
            handler(NO, nil, [HGApiClient errorFromResponse:response]);
            return; // error parsing response
        }
        NSNumber *success = response[@"success"];
        handler(success.boolValue, model, nil);
    }];
}

- (void)performIdScanActionWithPendingUserId:(NSString *)pendingUserId
                                documentType:(NSString *)documentType
                              refreshRequest:(NSNumber *)refreshRequest
                             scanFrontBase64:(NSString *)scanFrontBase64
                              scanBackBase64:(NSString *)scanBackBase64
                                     handler:(HGPerformIdScanActionResponseHandler)handler
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:@ {
        @"pendingUserId" : pendingUserId,
        @"documentType" : documentType,
    }];
    
    if (refreshRequest) {
        parameters[@"refreshRequest"] = refreshRequest;
    }
    if (scanFrontBase64.hasValue) {
        parameters[@"scanFrontBase64"] = scanFrontBase64;
    }
    if (scanBackBase64.hasValue) {
        parameters[@"scanBackBase64"] = scanBackBase64;
    }
    
    [self
     POST:HGMethod.performIdScanAction
     parameters:parameters
     resultBlock:^(NSDictionary *response, NSError *error) {
        if (error) {
            handler(NO, nil, error);
            return; // error
        }
        NSString *errorMessage = response[@"errorMessage"];
        if (errorMessage.hasValue) {
            handler(NO, nil, [HGApiClient errorFromResponse:response]);
            return; // error (recoverable)
        }
        
        
        /*
         2018-08-30 16:22:13.685559+0300 Health Gorilla[94818:8756007]
         (lldb) po response
         {
         idscanSuccess = 1;
         success = 1;
         }
         */
        
        //done: завести поле в Контексте что это переотправка реквеста на scanId
        
        HGPerformIdScanActionResponse *model = [[HGPerformIdScanActionResponse alloc] initWithDictionary:response error:&error];
        
        if (error) { // error parsing response
            handler(NO, nil, [HGApiClient errorFromResponse:response]);
            return;
        }
         
        NSNumber *success = response[@"success"];
        handler(success.boolValue, model, nil);
    }];
}

- (void)performScanIdResultPollingResultAction:(NSString *)pendingUserId
                                     deviceUID:(NSString *)deviceUID
                                    appVersion:(NSString *)appVersion
                                       handler:(HGPerformIdScanPollingActionResponseHandler)handler
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:@ {
        @"pendingUserId" : pendingUserId
        
    }];
    
    if (deviceUID) {
        parameters[@"deviceUID"] = deviceUID;
    }
    if (appVersion) {
        parameters[@"appVersion"] = appVersion;
    }
    
    [self POST:HGMethod.performIdScanPollingResultsAction parameters:parameters resultBlock:^(NSDictionary *response, NSError *error) {
         if (error) {
             handler(NO, nil, error);
             return; // error
         }
         NSString *errorMessage = response[@"errorMessage"];
         if (errorMessage.hasValue) {
             handler(NO, nil, [HGApiClient errorFromResponse:response]);
             return; // error
         }
    
    HGPerformIdScanPollingResultsActionResponse *model = [[HGPerformIdScanPollingResultsActionResponse alloc] initWithDictionary:response error:&error];
    
    if (error) {
        handler(NO, nil, [HGApiClient errorFromResponse:response]);
        return; // error parsing response
    }
    NSNumber *success = response[@"success"];
    handler(success.boolValue, model, nil);
     }];
}

@end
