//
//  HGSignupUserTypes.h
//  HG-Project
//
//  Created by not Paul on 04/10/2016.
//  Copyright © 2016 HG-Inc. All rights reserved.
//

typedef NS_ENUM(NSInteger, HGSignupUserType) {
    HGSignupUserPatient = 0,  // constants defined by API
    HGSignupUserClinician = 1,
    HGSignupUserStaff = 2,
    //
    HGSignupUserUndefined = -1,
};
