Health Gorilla Mobile
==

Required steps to use this project:

1. Make sure Cocoapods tools is installed. Check: pod -h
 (http://guides.cocoapods.org/using/getting-started.html#getting-started)

2. Update or setup project's dependencies:

a) If there're no Pods/ directory and Podfile.lock file:

> pod install

a) Pods/ directory and Podfile.lock exists:

> pod update

3. Install Twine for localization renewal from twine.txt

(https://github.com/scelis/twine)